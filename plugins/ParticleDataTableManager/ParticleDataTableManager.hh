/*
 * ParticleDataTableManager.hh
 *
 *  Created on: Oct 5, 2017
 *      Author: caiazza
 */

#ifndef PLUGINS_PARTICLEDATATABLEMANAGER_PARTICLEDATATABLEMANAGER_HH_
#define PLUGINS_PARTICLEDATATABLEMANAGER_PARTICLEDATATABLEMANAGER_HH_

#include "MXWare/Core/Plugin_Base.hh"
#include <string>
#include <vector>

#include "MXWare/Util/MXParticleData.hh"
#include "MXWare/Util/MXUtil.hh"

namespace MXWare
{
namespace Plugin
{
class ParticleDataTableManager: public Core::Plugin_SafetyChecked
{
public:
    /// Default constructor
    ParticleDataTableManager();

    /// Destructor
    ~ParticleDataTableManager() = default;

    /// Copy
    ParticleDataTableManager(const ParticleDataTableManager& other) = delete;

    /// Assignment
    ParticleDataTableManager&
    operator=(const ParticleDataTableManager& other) = delete;

    /// Move
    ParticleDataTableManager(ParticleDataTableManager&& other) = delete;

    /// Move assign
    ParticleDataTableManager&
    operator=(ParticleDataTableManager&&) = delete;

    /**
     * The execution code which can/should be called repeatedly goes here
     */
    void
    Execute ( ) override;

    /// User function to provide the activation code
    void
    doActivate ( ) override;

    /// User function to provide the deactivation code
    void
    doDeactivate ( ) override;

    /// User function to provide more version checks for other packages for example
    bool
    CheckVersionMore ( ) const override;

    /**
     * returns a string to identify the plugin
     * @return
     */
    std::string
    GetPluginName ( ) const override;

    /// Configures the plugin through a property tree
    void
    Configure (const Core::MXConfTree& ConfTree) override;

    /**
     * The concrete class should return a list of all the available
     * parameters for the concrete class
     * @return
     */
    Core::ParameterList
    GetParameterList ( ) const override;

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters
     *
     * @return
     */
    Core::MXConfTree
    PopulateTree ( ) const override;

private:
    /// Overwrite the data already existing in the table
    bool m_OverwriteData;

    /// Path from where to load additional data
    std::string m_LoadPath;

    /// Path where to save the existing data
    std::string m_SavePath;

    /// List of additional data to be loaded in the particle data table
    std::vector<Util::MXParticleData> m_AdditionalData;

    /// Prints the filled particle data table to screen
    bool m_PrintData;

    /// Configuration node names
    static const std::string s_NN_OverwriteData;
    static const std::string s_NN_LoadPath;
    static const std::string s_NN_SavePath;
    static const std::string s_NN_AdditionalData;
    static const std::string s_NN_PrintData;

};

}  // namespace Plugin


namespace Core
{
/**
 * A common template to define the registration name of a class for the factory system.
 * By default this is simply the typeid of the class as return by the compiler but all classes
 * which are registered in a factory should redefine it in a human-readable way
 * @return
 */
template<>
struct
ClassRegistrationTraits<Plugin::ParticleDataTableManager>
{
    static std::string
    Name()
    {
        return "ParticleDataTableManager";
    }
};


}  // namespace Core

/**
 * Loader function for the plugin
 * @return
 */
extern "C"
Core::IPlugin*
LoadPlugin();

}  // namespace MXWare




#endif /* PLUGINS_PARTICLEDATATABLEMANAGER_PARTICLEDATATABLEMANAGER_HH_ */
