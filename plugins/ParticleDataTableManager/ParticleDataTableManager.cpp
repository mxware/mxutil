/*
 * ParticleDataTableManager.cpp
 *
 *  Created on: Oct 5, 2017
 *      Author: caiazza
 */

#include "ParticleDataTableManager.hh"

#include "boost/filesystem.hpp"

#include "MXWare/Core/MXLogger.hh"
#include "MXWare/Util/MXParticleDataTable.hh"
#include "MXWare/MXUtilVersion.hh"
namespace bfs = boost::filesystem;

namespace MXWare
{
namespace Plugin
{
const std::string ParticleDataTableManager::s_NN_OverwriteData{"overwrite_data"};
const std::string ParticleDataTableManager::s_NN_LoadPath{"load_path"};
const std::string ParticleDataTableManager::s_NN_SavePath{"save_path"};
const std::string ParticleDataTableManager::s_NN_AdditionalData{"pdata"};
const std::string ParticleDataTableManager::s_NN_PrintData{"print"};


ParticleDataTableManager::ParticleDataTableManager ( ):
    m_OverwriteData{true},
    m_LoadPath{},
    m_SavePath{},
    m_AdditionalData{},
    m_PrintData{false}
{
}

void
ParticleDataTableManager::Execute ()
{
    BOOST_LOG_TRIVIAL(info) << "Configuration of the Particle Data Manager completed";

    auto& GTable = Util::MXParticleDataTable::GlobalPDataTable();

    if(!m_LoadPath.empty())
    {
        bfs::path P{m_LoadPath};
        if(P.extension() == ".xml")
        {
            GTable.LoadData(P, Util::MXParticleDataTable::SupportedFormats::XML, m_OverwriteData);
        }
        else
        {
            GTable.LoadData(P, Util::MXParticleDataTable::SupportedFormats::PDG2014, m_OverwriteData);
        }
    }

    for(auto& Data: m_AdditionalData)
    {
        auto res = GTable.insert(Data);
        if(!res.second && m_OverwriteData)
        {
            GTable.erase(res.first);
            GTable.insert(Data);
        }
    }

    if(m_PrintData)
    {
        auto out = GTable.PopulateTree();
        BOOST_LOG_TRIVIAL(info) << out << std::endl;
    }

    if(!m_SavePath.empty())
    {
        bfs::path P{m_SavePath};
        GTable.SaveData(P);
    }




}

void
ParticleDataTableManager::doActivate ( )
{
    auto& GTable = Util::MXParticleDataTable::GlobalPDataTable();
    Util::MXParticleDataTable::LoadDefaultData(GTable);
}

void
ParticleDataTableManager::doDeactivate ( )
{
}

bool
ParticleDataTableManager::CheckVersionMore ( ) const
{
    constexpr Core::MXPackageVersion CompileMXUtilVer = Core::MXUtilVersion;

    return(CompileMXUtilVer == Core::GetMXUtilPackageVersion());
}

std::string
ParticleDataTableManager::GetPluginName ( ) const
{
    return "ParticleDataTableManager";
}

void
ParticleDataTableManager::Configure (const Core::MXConfTree& ConfTree)
{
    Core::UpdateParameter(m_OverwriteData, ConfTree, s_NN_OverwriteData);
    Core::UpdateParameter(m_PrintData, ConfTree, s_NN_PrintData);
    auto Success = Core::UpdateParameter(m_LoadPath, ConfTree, s_NN_LoadPath);
    if(!Success)
        m_LoadPath.clear();

    Success = Core::UpdateParameter(m_SavePath, ConfTree, s_NN_SavePath);
    if(!Success)
        m_SavePath.clear();

    m_AdditionalData.clear();
    Core::UpdateParameter(m_AdditionalData, ConfTree, s_NN_AdditionalData);

}

Core::ParameterList
ParticleDataTableManager::GetParameterList ( ) const
{
    Core::ParameterList ans;
    ans.AddParameter<decltype(m_OverwriteData)>(s_NN_OverwriteData,
        "Overwrite the data already existing in the table");

    ans.AddParameter<decltype(m_PrintData)>(s_NN_PrintData,
        "Prints the filled particle data table to screen");

    ans.AddParameter<decltype(m_LoadPath)>(s_NN_LoadPath,
        "Path from where to load additional data (if empty or undefined the loading is disabled");

    ans.AddParameter<decltype(m_SavePath)>(s_NN_SavePath,
        "Path where to save additional data (if empty or undefined the saving is disabled");

    ans.AddParameter<decltype(m_AdditionalData)>(s_NN_AdditionalData,
        "List of additional data to be loaded in the particle data table");

    return ans;
}

Core::MXConfTree
ParticleDataTableManager::PopulateTree ( ) const
{
    Core::MXConfTree ans;

    Core::WriteToTree(m_OverwriteData, ans, s_NN_OverwriteData);
    Core::WriteToTree(m_PrintData, ans, s_NN_PrintData);
    Core::WriteToTree(m_LoadPath, ans,s_NN_LoadPath);
    Core::WriteToTree(m_SavePath, ans, s_NN_SavePath);
    Core::WriteToTree(m_AdditionalData, ans, s_NN_AdditionalData);

    return ans;
}
}  // namespace Plugin

Core::IPlugin*
LoadPlugin()
{
    return new Plugin::ParticleDataTableManager;
}

}  // namespace MXWare
