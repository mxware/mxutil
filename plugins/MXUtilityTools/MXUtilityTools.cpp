/*
 * MXUtilityTools.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: caiazza
 */


#include "MXUtilityTools.hh"

#include "boost/log/trivial.hpp"
#include <iostream>
#include <sstream>

#include "MXWare/Util/MXParticleDataTable.hh"
#include "MXWare/Util/MXUtil.hh"
#include "MXWare/MXUtilVersion.hh"

namespace MXWare
{
namespace Plugin
{
MXUtilityTools::MXUtilityTools():
    m_LogPhys{false},
    m_PDataPath{""},
    m_LogPData{false}
{
    RegisterParameters();
}

void
MXUtilityTools::Execute( )
{
    if(m_LogPhys)
    {
        std::stringstream msg;
        msg << "\nDefined physical constants values:\n";
        Util::PrintPhysicalConstants(msg);
        BOOST_LOG_TRIVIAL(info) << msg.str() << std::endl;
    }

    if(m_LogPData)
    {
        auto out = Util::MXParticleDataTable::GlobalPDataTable().PopulateTree();
        BOOST_LOG_TRIVIAL(info) << out << std::endl;
    }
}

std::string
MXUtilityTools::GetPluginName() const
{
    return "MXUtilityTools";
}

void
MXUtilityTools::doActivate()
{
    BOOST_LOG_TRIVIAL(debug) << GetPluginName() << " plugin ACTIVATED" << std::endl;

}

void
MXUtilityTools::doDeactivate()
{
    BOOST_LOG_TRIVIAL(debug) << GetPluginName() << " plugin DEACTIVATE" << std::endl;
}

bool
MXUtilityTools::CheckVersionMore() const
{
    constexpr Core::MXPackageVersion CompileMXUtilVer = Core::MXUtilVersion;

    return(CompileMXUtilVer == Core::GetMXUtilPackageVersion());
}

void
MXUtilityTools::RegisterMore()
{
    m_Pars.AddParameter("LogPhysConstants",
                      "Logs the physical constants defined in the system on the logging sinks",
                      m_LogPhys);
    m_Pars.AddParameter("pdatatable",
                      "Path to the particle data table",
                      m_PDataPath);
    m_Pars.AddParameter("LogPDataTable",
                      "Logs the particle data contained  in the global data table on the logging sinks",
                      m_LogPData);
}

void
MXUtilityTools::ConfigureMore(const Core::MXConfTree& ConfTree)
{

    if(m_Pars.at("pdatatable").IsSet())
    {
        BOOST_LOG_TRIVIAL(info) << "Loading the data table at " << m_PDataPath << std::endl;
        Util::MXParticleDataTable::GlobalPDataTable().LoadData(m_PDataPath);
        BOOST_LOG_TRIVIAL(info) << Util::MXParticleDataTable::GlobalPDataTable().size() <<
            " particles loaded" << std::endl;
    }

}

}  // namespace Plugin

Core::IPlugin*
LoadPlugin()
{
    return new Plugin::MXUtilityTools;
}
}  // namespace MXWare
