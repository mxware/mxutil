/**
 * \file MXUtilityTools.hh
 *
 * Header file of the MXUtilityTools plugin
 *
 *  Created on: Feb 3, 2016
 *      Author: caiazza
 */

#ifndef PLUGINS_MXUTILITYTOOLS_MXUTILITYTOOLS_HH_
#define PLUGINS_MXUTILITYTOOLS_MXUTILITYTOOLS_HH_

#include "MXWare/Core/Plugin_Base.hh"

namespace MXWare
{
namespace Plugin
{
/**
 * This plugin executes several routines to configure the components of the MXUtil
 * package or to log relevant information about the library.
 * The following routines are currently implemented:
 * * LoadParticleTable: Loads the static particle table from the required file
 * * LogPhysicalConstants: Prints to the log sinks the values of the Physical
 * Constants defined in the library.
 */
class MXUtilityTools: public Core::Plugin_Base
{
public:
    /// Default constructor
    MXUtilityTools();

    /// Destructor (non virtual NB)
    ~MXUtilityTools() = default;

    /**
     * Execute the required subroutines
     */
    void
    Execute( ) override;

    /**
     * The name of this plugin
     * @return
     */
    std::string
    GetPluginName() const override;

private:
    /**
     * Activate the static components of the plugin
     */
    void
    doActivate() override;

    /**
     * Deactivates the static components of the plugin
     */
    void
    doDeactivate() override;

    /**
     * Checks that the MXUtil version loaded corresponds to the one for which this
     * plugin was compiled
     * @return
     */
    bool
    CheckVersionMore() const override;

    /**
     * Registers additional configuration parameters in the derived class.
     * All the configuration parameters registered this way are automatically
     * configured
     */
    void
    RegisterMore() override;

    /**
     * Performs additional configurations in the derived class.
     * Notice that all the parameters registered in the parameter list are
     * already automatically updated
     */
    void
    ConfigureMore(const Core::MXConfTree& ConfTree) override;

    /// Triggers the output to the log sinks of the physical constants
    bool m_LogPhys;

    /// The path containing the particle data table to use
    std::string m_PDataPath;

    /// Triggers the output of the particle data to the log sinks
    bool m_LogPData;

};

}  // namespace Plugin

namespace Core
{
/**
 * A common template to define the registration name of a class for the factory system.
 * By default this is simply the typeid of the class as return by the compiler but all classes
 * which are registered in a factory should redefine it in a human-readable way
 * @return
 */
template<>
struct
ClassRegistrationTraits<Plugin::MXUtilityTools>
{
    static std::string
    Name()
    {
        return "MXUtilityTools";
    }
};


}  // namespace Core

extern "C"
Core::IPlugin*
LoadPlugin();
}  // namespace MXWare



#endif /* PLUGINS_MXUTILITYTOOLS_MXUTILITYTOOLS_HH_ */
