/*
 * RectangularDoubleStripLayout_Test.cpp
 *
 *  Created on: Jun 15, 2018
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE RectangularDoubleStripLayout_Test test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;


#include "MXWare/Merlin/Layout/RectangularDoubleStripLayout.hh"
namespace MXLayout = MXWare::Merlin::Layout;

#include "MXWare/Util/SystemOfUnits.hh"
using MXWare::Units::mm;

#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

BOOST_AUTO_TEST_CASE(ConstructionTest)
{
    std::cout << "Creation of a default strip layout, which is empty" << std::endl;
    auto Default = mt::GeneralClassConstructionTest<MXLayout::RectangularDoubleStripLayout>();
    BOOST_CHECK(Default.empty());
    BOOST_CHECK(Default.size() == 0);
    // An empty container is also defined by the fact that begin == end
    BOOST_CHECK(Default.begin() == Default.end());

    MXLayout::RectangularDoubleStripLayout::StripParameters StripsX{10, 0.5, 0.4};
    MXLayout::RectangularDoubleStripLayout::StripParameters StripsY{10, 0.5, 0.4};
    MXLayout::RectangularDoubleStripLayout::key_type::id_0 DetId{1};

    MXLayout::RectangularDoubleStripLayout TestLayout{StripsX, StripsY, DetId};
    for(auto& Surf: TestLayout)
    {
        std::cout << "Strip ID: " << Surf.GetId() << "\nStrip Area: " <<
            MXGeo::Area(Surf.GetPolygon()) << "\nVertexes:\n";
        for(auto&Vertex: Surf.GetPolygon().outer())
        {
            std::cout << Vertex << '\t';
        }
        std::cout << std::endl;

    }

    auto BBox = TestLayout.GetBoundingBox();
    std::cout << "\nLayout Bounding Box: " << BBox.min_corner() <<
        "\t" << BBox.max_corner()<< std::endl;
}

BOOST_AUTO_TEST_CASE(IterationTest)
{
    std::cout << "Testing the iteration through the layout" << std::endl;
    MXLayout::RectangularDoubleStripLayout::StripParameters StripsX{100, 0.5, 0.4};
    MXLayout::RectangularDoubleStripLayout::StripParameters StripsY{100, 0.5, 0.4};
    MXLayout::RectangularDoubleStripLayout::key_type::id_0 DetId{1};

    MXLayout::RectangularDoubleStripLayout TestLayout{StripsX, StripsY, DetId};

    auto it = TestLayout.begin();
    std::cout << "Type dereferenced by begin: " << boost::core::demangle(typeid(*it).name()) << std::endl;
    std::cout << "Strip ID: " << it->GetId() << std::endl;

    size_t counter = 0;
    for(; it != TestLayout.end(); ++it, ++counter)
    {
    }

    BOOST_CHECK(counter == 200);

}



