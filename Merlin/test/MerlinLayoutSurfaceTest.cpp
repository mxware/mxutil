/*
 * MerlinLayoutSurfaceTest.cpp
 *
 *  Created on: Jun 13, 2018
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE MerlinLayoutSurfaceTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

#include "MXWare/Merlin/Layout/SensitiveSurface.hh"
namespace MXLayout = MXWare::Merlin::Layout;
typedef MXLayout::SensitiveSurface::Polygon::point_type              Point_2;


BOOST_AUTO_TEST_CASE(ConstructionTest)
{
    std::cout << "Testing the sensitive surface default construction" << std::endl;
    auto DefaultSurface =mt::GeneralClassConstructionTest<MXLayout::SensitiveSurface>();
    BOOST_CHECK(DefaultSurface.GetId().IsUndefined());
    BOOST_CHECK(!MXGeo::Is_Valid(DefaultSurface.GetPolygon()));

    MXLayout::SensitiveSurface::id_t Id{1, 29};

    Point_2 p1{0,0};
    Point_2 p2{0,2.5};
    Point_2 p3{2.5, 2.5};
    Point_2 p4{2.5, 0};
    MXLayout::SensitiveSurface::Polygon Poly{{p1, p2, p3, p4}};
    auto FullSurface = mt::GeneralClassConstructionTest<MXLayout::SensitiveSurface>(Id, Poly);

}
