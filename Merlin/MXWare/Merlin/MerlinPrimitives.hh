/*
 * MerlinPrimitives.hh
 *
 *  Created on: Jun 13, 2018
 *      Author: caiazza
 */

#ifndef MERLIN_MERLIN_MERLINPRIMITIVES_HH_
#define MERLIN_MERLIN_MERLINPRIMITIVES_HH_

#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/CommonIdentifier.hh"

namespace MXWare
{

namespace Merlin
{
///
/// The structure collects all the type definitions which are used by the Merlin package
/// It provides a single point of reference for data structure and algorithms to obtain
/// the types used in the package.
struct Primitives
{
public:
    /// The type defining all the geometry primitives used in the Merlin package
    /// by default those are the cartesian primitives of the Geometry package
    /// This means that the underlying representation of the detector description
    /// is cartesian
    typedef Geometry::DPCartesian                           geometry_t;

    /// The type of numerical type used to express the geometrical coordinates
    typedef typename geometry_t::coordinate_t               coordinate_t;

    /// The type of object used to represent a 2D polygon
    typedef typename geometry_t::Polygon_2                  Polygon_2;

    /// The object representing the 2D bounding box of an element or of a group of them
    typedef typename geometry_t::Bbox_2                     Bbox_2;

    /// The type used to identify a layout element
    typedef Util::CommonIdentifier                          LayoutId;




};

}  // namespace Merlin

}  // namespace MXWare



#endif /* MERLIN_MERLIN_MERLINPRIMITIVES_HH_ */
