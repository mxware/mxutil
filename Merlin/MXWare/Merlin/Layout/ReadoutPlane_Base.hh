/*
 * ReadoutPlane_Base.hh
 *
 *  Created on: Jun 14, 2018
 *      Author: caiazza
 */

#ifndef MERLIN_MERLIN_LAYOUT_READOUTPLANE_BASE_HH_
#define MERLIN_MERLIN_LAYOUT_READOUTPLANE_BASE_HH_

#include "MXWare/Merlin/Layout/IReadoutPlane.hh"
#include "MXWare/Core/SmartParameterList.hh"
#include <map>
#include <boost/iterator/transform_iterator.hpp>
#include "MXWare/Core/MXLogger.hh"

namespace MXWare
{

namespace Merlin
{

namespace Layout
{
/// Base class for readout plane description, implementing the basic architecture of
/// the base class interfaces
class ReadoutPlane_Base: public IReadoutPlane
{
protected:
    ///{@ The gang of five
    /// Constructor, destructor, assignment, copy and move
    ReadoutPlane_Base():
        m_Pars{},
        m_Name{""}
    {

    }

    virtual
    ~ReadoutPlane_Base() = default;

    ReadoutPlane_Base(const ReadoutPlane_Base& other):
        m_Pars{},
        m_Name{other.m_Name}
    {

    }

    ReadoutPlane_Base&
    operator=(const ReadoutPlane_Base& other)
    {
        if(this!= &other)
        {
            m_Name = other.m_Name;
        }
        return *this;
    }

    ReadoutPlane_Base(ReadoutPlane_Base&& other):
        m_Pars{},
        m_Name{std::move(other.m_Name)}
    {

    }

    ReadoutPlane_Base&
    operator=(ReadoutPlane_Base&& other)
    {
        if(this != &other)
        {
            m_Name = std::move(other.m_Name);
        }
        return *this;
    }
    ///@}

public:

    /* ***************************************************************
     * NamedObject interface
     *****************************************************************
     ***************************************************************** */
    /**
     * Retrieves the object name
     * @return
     */
    inline std::string
    GetName ( ) const override
    {
        return m_Name;
    }

    /**
     * Sets the object name
     * @param Name
     */
    inline void
    SetName (const std::string& Name) override
    {
        m_Name = Name;
    }

    /* ***************************************************************
     * Configurable interface
     *****************************************************************
     ***************************************************************** */
    /**
     * Configures the concrete object through a boost property tree
     * @param ConfTree
     */
    void
    Configure (const Core::MXConfTree& ConfTree) override
    {
        /*
         * The parameter list is filled the first time when it is to be configured
         * This is to avoid putting the registration function in the constructors
         * which in turn allows that function to call a virtual function
         */
        if(!m_Pars.IsInitialized())
        {
            BOOST_LOG_TRIVIAL(warning) << "The smart parameter list was not initialized in the "
                "constructor of the class " << IsA() << ". Forcing the list initialization.";
            RegisterParameters();
        }

        m_Pars.UpdateAll(ConfTree);

        ConfigureMore(ConfTree);
    }

    /**
     * The concrete class should return a list of all the available
     * parameters for the concrete class.
     * If additional operation needs to be defined the user should override this
     * virtual function
     * @return
     */
    virtual Core::ParameterList
    GetParameterList ( ) const override
    {
        return m_Pars.CreateParameterList();
    }

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters.
     * If additional operation needs to be defined the user should override this
     * virtual function
     * @return
     */
    virtual Core::MXConfTree
    PopulateTree ( ) const override
    {
        return m_Pars.CreateTree();
    }

protected:

    /// Container for the configuration parameters
    Core::SmartParameterList m_Pars;

    /// The name of the object
    std::string m_Name;

    /**
     * Registers the configuration parameters of the concrete classes in the internal
     * container.
     * This operation is normally performed automatically at configuration time but
     * in case an application requires to obtain the parameter list or populate the tree
     * before the executing the configuration, it will need to execute this command first
     */
    virtual void
    RegisterParameters ( )
    {
        m_Pars.clear();
        m_Pars.AddParameter(NamedObject::ObjectNameNodeName(),
                            "The name of the generator instance",
                            m_Name);
    }

    /**
     * Performs additional configurations in the derived class.
     * Notice that all the parameters registered in the parameter list are
     * already automatically updated
     */
    virtual void
    ConfigureMore (const Core::MXConfTree& ConfTree) = 0;

    /**
     * Returns the identification name of the derived class
     * @return
     */
    virtual std::string
    IsA ( ) const override = 0;


    /**
     * Polymorphic comparison operator.
     * Called by the normal operator== which checks the equality of the typeid
     * so the user can safely assume that rhs is of the same derived class of this
     * @param rhs
     * @return
     */
    virtual bool
    IsEqual(const IMXData& rhs) const override = 0;

};

/// Implementation of a generic readout plane class based on a map that stores all
/// the sensitive element.
/// The class should be further specialzied to describe specific layout models therefore
/// all the constructors are protected
class ReadoutPlane_MapBased: public ReadoutPlane_Base
{
    /// The type containing the sensitive elements of the layout
    typedef std::map<key_type, mapped_type>             container_t;

    typedef std::function<const mapped_type&(const container_t::value_type&)> AccessFunction;
    struct AccessMapped
    {
        typedef const container_t::mapped_type result_type;

        const mapped_type& operator()(const container_t::value_type& p) { return p.second; }
    };
    typedef boost::transform_iterator<AccessFunction,container_t::const_iterator>  adapted_const_iterator;


protected:
    ///@{ The gang of five
    /// Constructor, destructor, assignment, copy and move
    ReadoutPlane_MapBased() = default;

    virtual
    ~ReadoutPlane_MapBased() = default;

    ReadoutPlane_MapBased(const ReadoutPlane_MapBased& other) = default;

    ReadoutPlane_MapBased&
    operator=(const ReadoutPlane_MapBased& other) = default;

    ReadoutPlane_MapBased(ReadoutPlane_MapBased&& other) = default;

    ReadoutPlane_MapBased&
    operator=(ReadoutPlane_MapBased&& other) = default;
    ///@}

public:
    /* ******************************************************
     * Capacity
     ****************************************************** */
    /**
     * Returns the total number of sensitive elements managed by the layout
     * @return
     */
    size_type
    size() const noexcept override
    {
        return m_SurfMap.size();
    }

    /// Returns true if no sensitive element is contained in the readout
    bool
    empty() const noexcept override
    {
        return m_SurfMap.empty();
    }

    /* ******************************************************
     * Lookup
     ****************************************************** */
    /**
     * Counts how many sensitive elements matching the key are managed by the layout
     * @param Key
     * @return
     */
    size_type
    count(const key_type& Key) const override
    {
        return m_SurfMap.count(Key);
    }

    /**
     * Returns an iterator to the first element mapped to the argument key
     * @param Key
     * @return
     */
//    surface_iterator
//    find(const key_type& Key) const override
//    {
//        auto it = m_SurfMap.find(Key);
//        if(it == m_SurfMap.end())
//        {
//            return cend();
//        }
//        else
//        {
//            return surface_iterator{adapted_const_iterator{it, AccessMapped{}}};
//        }
//    }

    /* ******************************************************
     * Access
     ****************************************************** */
    ///@{
    /// Direct access at one of the elements managed by the readout.
    /// Throws an OutOfRange exception if the key is not found
//    const mapped_type&
//    at(const key_type& Key) const override
//    {
//        return m_SurfMap.at(Key);
//    }
    ///@}

    ///@{
    /// Standard iterator interface
    /// Only the constant iterators are defined because the contained sensitive
    /// surfaces are not to be modified directly by the user
//    surface_iterator
//    begin() const override
//    {
//        return surface_iterator{adapted_const_iterator{m_SurfMap.cbegin(), AccessMapped{}}};
//    }
//
//    surface_iterator
//    cbegin() const override
//    {
//        return surface_iterator{adapted_const_iterator{m_SurfMap.cbegin(), AccessMapped{}}};
//    }
//
//    surface_iterator
//    end() const override
//    {
//        return surface_iterator{adapted_const_iterator{m_SurfMap.cend(), AccessMapped{}}};
//    }
//
//    surface_iterator
//    cend() const override
//    {
//        return surface_iterator{adapted_const_iterator{m_SurfMap.cend(), AccessMapped{}}};
//    }
    ///@}

    /* *****************************************************************
     * Geometry functions
     ***************************************************************** */
    /// Calculates the bounding box of all the stored polygons based on the iteration
    /// on all the element.
    /// The implementation classes could provide a more efficient implementation
    /// od this function
    virtual boundingbox_t
    GetBoundingBox() const override;

protected:
    /// The container of the sensitive elements described by the derived class
    container_t m_SurfMap;

    /* ******************************************************
     * Virtual iteration
     * Standard set of function used to navigate through the layout
     * They are used by the iterator to allow to do it in a standard way
     ****************************************************** */
    /// Returns the key of the first element stored in the layout
//    key_type
//    FirstKey() const override;

    /// Returns the key of the element which comes after that of the argument key
//    virtual key_type
//    Next(const key_type& here) const override;

    /**
     * Performs additional configurations in the derived class.
     * Notice that all the parameters registered in the parameter list are
     * already automatically updated
     */
    virtual void
    ConfigureMore (const Core::MXConfTree& ConfTree) override = 0;

    /**
     * Returns the identification name of the derived class
     * @return
     */
    virtual std::string
    IsA ( ) const override = 0;

    /**
     * Polymorphic comparison operator.
     * Called by the normal operator== which checks the equality of the typeid
     * so the user can safely assume that rhs is of the same derived class of this
     * @param rhs
     * @return
     */
    virtual bool
    IsEqual(const IMXData& rhs) const override = 0;

};

}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare



#endif /* MERLIN_MERLIN_LAYOUT_READOUTPLANE_BASE_HH_ */
