/*
 * IReadoutPlane.hh
 *
 *  Created on: Jun 13, 2018
 *      Author: caiazza
 */

#ifndef MERLIN_MERLIN_LAYOUT_IREADOUTPLANE_HH_
#define MERLIN_MERLIN_LAYOUT_IREADOUTPLANE_HH_

#include "MXWare/Core/Configurable.hh"
#include "MXWare/IO/IMXData.hh"

#include "MXWare/Merlin/MerlinPrimitives.hh"
#include "MXWare/Merlin/Layout/SensitiveSurface.hh"

#include "boost/range/detail/any_iterator.hpp"

namespace MXWare
{

namespace Merlin
{

namespace Layout
{
namespace detail
{
/// Forward declaration of the iterator which will be defined to navigate the readout
/// layout object
template<bool is_const_iterator>
class ReadoutPlane_iterator;


}  // namespace detail




/// Interface class for a readout plane described by the Merlin package.
/// This set of classes should manage the geometry and relationship
/// between the sensitive elements of a single readout structure and, in particular:
/// * allow to navigate through all sensitive elements in a readout
/// * define the position of each sensitive element in the local coordinate system of the plane
/// * Help finding those elements which are geometrically close to one another
///
/// All the elements positions are always defined in terms of a local reference system.
/// The layout must then be positioned in some geometrical volume to calculate a global
/// geometric positioning of the element
///
/// The readout classes must be default constructible and configurable
/// They also need to be clonable because it is a common situation to have
/// the same layout for several detectors in parallel
/// For this reason each layout object can have his own identification name
/// Finally two layouts should be comparable and a polymorphic equality operator is
/// therefore defined.
///
/// The readout classes should be configured through the common MXWare configuration system.
/// To be used in a multithreaded system, the interface also includes the access to an internal
/// lock which allows to take exclusive control of the object
///
///
///
class IReadoutPlane: public IO::IMXData,
                     public Core::Configurable,
                     public Core::NamedObject
{
public:
    /// The identifier used to index the surfaces in the readout
    typedef Primitives::LayoutId                  key_type;

    /// The object representing a surface mapped in the readout
    typedef SensitiveSurface                            mapped_type;

    /// This class is a polymorphic, generic iterator to navigate a set of sensitive surfaces
    /// stored in a container like, for example a readout plane
    /// The boost base class allows to create a common base class hiding potential different
    /// implementations of the container itself
    /// NOTE that the iterator will dereference to a const reference of the iterated object which
    /// means that we need to use containers storing the required objects and not use a functional
    /// implementation where the referred object is recalculated each time. Potentially we could
    /// use a lazy implementation where the object is calculated and stored only at the first request
    /// and then simply accessed
    using surface_iterator = boost::range_detail::any_iterator<const Layout::SensitiveSurface,
                                                               boost::forward_traversal_tag,
                                                               const Layout::SensitiveSurface&,
                                                               std::ptrdiff_t>;
    ///@}

    /// The type representing the size of the container
    typedef size_t                                      size_type;

    /// The type representing the bounding box of the layout or of any of its elements
    typedef Primitives::Bbox_2                          boundingbox_t;

public:
    /// Polymorhic destructor
    virtual
    ~IReadoutPlane() = default;

    /// Configures the concrete class through a property tree
    virtual void
    Configure (const Core::MXConfTree& ConfTree) override = 0;

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters
     * @return
     */
    virtual Core::MXConfTree
    PopulateTree ( ) const override = 0;

    /**
     * Returns a list of the configuration parameter metadata.
     * This allows the configuration structure to be queried by the configuration
     * system.
     * @return
     */
    virtual Core::ParameterList
    GetParameterList ( ) const override= 0;

    /**
     * Retrieves the object name
     * @return
     */
    virtual std::string
    GetName ( ) const override = 0;

    /**
     * Sets the object name
     * @param Name
     */
    virtual void
    SetName (const std::string& Name) override = 0;

    /**
     * Returns the identification name of the derived class
     * @return
     */
    virtual std::string
    IsA ( ) const override = 0;

    /* ******************************************************
     * Capacity
     ****************************************************** */
    /**
     * Returns the total number of sensitive elements managed by the layout
     * @return
     */
    virtual size_type
    size() const noexcept = 0;

    /// Returns true if no sensitive element is contained in the readout
    virtual bool
    empty() const noexcept = 0;

    /* ******************************************************
     * Lookup
     ****************************************************** */
    /**
     * Counts how many sensitive elements matching the key are managed by the layout
     * @param Key
     * @return
     */
    virtual size_type
    count(const key_type& Key) const = 0;

    /**
     * Returns an iterator to the first element mapped to the argument key
     * @param Key
     * @return
     */
//    virtual surface_iterator
//    find(const key_type& Key) const = 0;

    /* ******************************************************
     * Access
     ****************************************************** */
    ///@{
    /// Direct access at one of the elements managed by the readout.
    /// Throws an OutOfRange exception if the key is not found
    /// PROBLEM Doesn't allow lazy evaluation in this way. Better performance or better flexibility?
//    virtual const mapped_type&
//    at(const key_type& Key) const = 0;
    ///@}

    ///@{
    /// Standard iterator interface
    /// Only the constant iterators are defined because the contained sensitive
    /// surfaces are not to be modified directly by the user
//    virtual surface_iterator
//    begin() const = 0;
//
//    virtual surface_iterator
//    cbegin() const = 0;
//
//    virtual surface_iterator
//    end() const = 0;
//
//    virtual surface_iterator
//    cend() const = 0;
    ///@}


    /* ******************************************************
     * Geometry
     ****************************************************** */
    /// Retrieves the bounding box of the implemented layout
    virtual boundingbox_t
    GetBoundingBox() const = 0;

    static std::string
    GetDefaultNodeName()
    {
        return "readout_layout";
    }

protected:
    /**
     * Polymorphic comparison operator.
     * Called by the normal operator== which checks the equality of the typeid
     * so the user can safely assume that rhs is of the same derived class of this
     * @param rhs
     * @return
     */
    virtual bool
    IsEqual(const IMXData& rhs) const override = 0;

    /* ******************************************************
     * Virtual iteration
     * Standard set of function used to navigate through the layout
     * They are used by the iterator to allow to do it in a standard way
     * It is protected because the users have to use the normal iteration system
     ****************************************************** */

    /// Returns the key of the first element stored in the layout
//    virtual key_type
//    FirstKey() const = 0;

    /// Returns the key of the past-the-end element stored in the layout
    /// This key foesn't match any stored element and should only be used to
    /// set the limits of loop iterations in a standard way
//    static constexpr key_type
//    PastLast()
//    {
//        return key_type{};
//    }

    /// Returns the key of the element which comes after that of the argument key
    /// NOTE
    /// The key after to the one identifying the PastLast element is the PastLast key itself
    /// If the argument doesn't match a stored element the function should throw an
    /// ObjectNotFound exception
//    virtual key_type
//    Next(const key_type& here) const = 0;

    /**
     * Copies the object polymorphically and returns a raw pointer.
     * Using the return type covariance one can return directly the pointer to the concrete
     * class if the call is done on a pointer to the concrete class
     * @return
     */
    virtual IReadoutPlane*
    RawClone ( ) const override = 0;

    virtual IReadoutPlane*
    RawCreate ( ) const override = 0;
};

namespace detail
{
/// The iterator which can be used to iterate through the Readout plane objects
template<bool is_const_iterator>
class ReadoutPlane_iterator
{
public:
    /// Declaration of the type of container connected to the iterator
    typedef typename std::conditional<is_const_iterator,
        const IReadoutPlane, IReadoutPlane>::type       readout_t;

    /// Declaring friend the other iterator type
    friend ReadoutPlane_iterator<!is_const_iterator>;

    /// Iterator category (sequential access, not random by index)
    typedef std::forward_iterator_tag                   iterator_category;

    /// The type of the object which is dereferenced by the iterator
    typedef typename readout_t::mapped_type             value_type;

    /// The reference the iterator will dereference to
    typedef typename std::conditional<is_const_iterator,
        const value_type&,
        value_type&>::type                              reference;

    /// The pointer type of the dereferenced object
    typedef typename std::conditional<is_const_iterator,
        const value_type*,
        value_type*>::type                              pointer;

    /// The type that can represent the distance between objects
    typedef std::ptrdiff_t                              difference_type;

    typedef ReadoutPlane_iterator<is_const_iterator>    self_type;

    typedef typename readout_t::key_type                key_type;

    /// Default constructor
    ReadoutPlane_iterator():
        m_Readout{nullptr},
        m_LastKey{}
    {

    }

    /// Normal constructor
    ReadoutPlane_iterator(readout_t& Readout, key_type Last):
        m_Readout{&Readout},
        m_LastKey{Last}
    {

    }

    /// Destructor
    ~ReadoutPlane_iterator() = default;

    /// Copy for the non-const version and conversion from non-const to const for the other
    ReadoutPlane_iterator(const ReadoutPlane_iterator<false>& other):
        m_Readout{other.m_Readout},
        m_LastKey{other.m_LastKey}
    {

    }

    /*
     * I didn't include the usual default declaration of copy and move because
     * otherwise a move operator without an explicitly defined copy operator will delete
     * the latter and in our case because of the way the conversion/copy operator
     * is defined, a non-const iterator would not have the copy constructor and so
     * it will be automatically deleted
     */

    ///@{ Comparison operators
    friend bool
    operator==(const ReadoutPlane_iterator& lhs, const ReadoutPlane_iterator& rhs)
    {
        return (lhs.m_Readout == rhs.m_Readout) &&
            (lhs.m_LastKey == rhs.m_LastKey);
    }

    friend bool
    operator!=(const ReadoutPlane_iterator& lhs, const ReadoutPlane_iterator& rhs)
    {
        return !(lhs == rhs);
    }
    ///@}

    /// Dereferencing
    reference
    operator*()
    {
        return m_Readout->at(m_LastKey);
    }

    /// Indirection
    pointer
    operator->()
    {
        return &(operator*());
    }

    /// Prefix increment
    self_type&
    operator++()
    {
        if(m_LastKey != m_Readout->PastLast())
            m_LastKey = m_Readout->Next(m_LastKey);
        return *this;
    }

    /// Postfix increment
    self_type
    operator++(int)
    {
        auto tmp = *this;
        if(m_LastKey != m_Readout->PastLast())
            m_LastKey = m_Readout->Next(m_LastKey);
        return tmp;
    }


private:
    /// A pointer to the readout object we are iterating on
    readout_t* m_Readout;

    /// The last key that was used in the navigation
    key_type m_LastKey;
};


}  // namespace detail

/*
inline IReadoutPlane::const_iterator
IReadoutPlane::begin() const
{
    return const_iterator(*this, FirstKey());
}

inline IReadoutPlane::const_iterator
IReadoutPlane::cbegin() const
{
    return const_iterator(*this, FirstKey());
}

inline IReadoutPlane::const_iterator
IReadoutPlane::end() const
{
    return const_iterator(*this, PastLast());
}

inline IReadoutPlane::const_iterator
IReadoutPlane::cend() const
{
    return const_iterator(*this, PastLast());
}

inline IReadoutPlane::const_reverse_iterator
IReadoutPlane::rbegin() const
{
    return const_reverse_iterator(end());
}

inline IReadoutPlane::const_reverse_iterator
IReadoutPlane::crbegin() const
{
    return const_reverse_iterator(end());
}

inline IReadoutPlane::const_reverse_iterator
IReadoutPlane::rend() const
{
    return const_reverse_iterator(begin());
}

inline IReadoutPlane::const_reverse_iterator
IReadoutPlane::crend() const
{
    return const_reverse_iterator(begin());
}*/

}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare



#endif /* MERLIN_MERLIN_LAYOUT_IREADOUTPLANE_HH_ */
