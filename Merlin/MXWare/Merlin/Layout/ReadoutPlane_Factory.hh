/*
 * ReadoutPlane_Factory.hh
 *
 *  Created on: Jul 6, 2018
 *      Author: caiazza
 */

#ifndef MERLIN_MXWARE_MERLIN_LAYOUT_READOUTPLANE_FACTORY_HH_
#define MERLIN_MXWARE_MERLIN_LAYOUT_READOUTPLANE_FACTORY_HH_

#include "MXWare/Core/GeneralizedFactory.hh"
#include "MXWare/Merlin/Layout/IReadoutPlane.hh"
#include "MXWare/Core/Traits.hh"
#include "MXWare/DSOSymbols.hh"

namespace MXWare
{
/// Creating a visible symbol for the specialization
extern template
class MXAPI Core::GeneralizedFactory<Merlin::Layout::IReadoutPlane>;

namespace Merlin
{

namespace Layout
{
/// Alias for the readout plane factory class
typedef Core::GeneralizedFactory<IReadoutPlane> ReadoutPlane_Factory;

/// Registration shortcut for implemented type R
template<class R>
void
RegisterReadoutPlane(ReadoutPlane_Factory& Fac = ReadoutPlane_Factory::GetGlobalFactory())
{
    Fac.RegisterClass(std::make_unique<R>,
                      Core::ClassRegistrationTraits<R>::Name(),
                      typeid(R));
}

/**
 * Overload of the function to registers the default available functions
 * @param Fac
 */
void
RegisterDefaultClasses(ReadoutPlane_Factory& Fac);

///@{
/// Definition of the specific configuration functions for this interface
/// NOTE That this interface implements the configurable interface so therefore it
/// is not necessary to define the Configure and Populate for the reference type
MXAPI void
Configure(std::unique_ptr<IReadoutPlane>& Obj, const Core::MXConfTree& Tree);

MXAPI void
Configure(std::shared_ptr<IReadoutPlane>& Obj, const Core::MXConfTree& Tree);

MXAPI Core::MXConfTree
PopulateTree(const std::unique_ptr<IReadoutPlane>& Obj);
///@}



}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare




#endif /* MERLIN_MXWARE_MERLIN_LAYOUT_READOUTPLANE_FACTORY_HH_ */
