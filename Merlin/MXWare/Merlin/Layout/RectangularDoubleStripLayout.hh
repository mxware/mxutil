/*
 * RectangularDoubleStripLayout.hh
 *
 *  Created on: Jun 15, 2018
 *      Author: caiazza
 */

#ifndef MERLIN_MERLIN_LAYOUT_RECTANGULARDOUBLESTRIPLAYOUT_HH_
#define MERLIN_MERLIN_LAYOUT_RECTANGULARDOUBLESTRIPLAYOUT_HH_

#include "MXWare/Merlin/Layout/ReadoutPlane_Base.hh"
#include "MXWare/Core/MXCore.hh"

/*
 * TODO
 * Bounding box (is a whole geometry concept)
 * Clustering view
 */

namespace MXWare
{

namespace Merlin
{

namespace Layout
{

class RectangularDoubleStripLayout : public ReadoutPlane_MapBased
{
public:
    /// Labels of the two clustering groups of the layout, corresponding
    /// to the two sets of strips where one should independently look for
    /// clusters
    enum ClusteringIndex:
        uint32_t
        {
            X = 0u,//!< X
            Y = 1u //!< Y
        };

    /// A data structure to collect the configuration parameters of a strip layout
    struct StripParameters
    {
        uint32_t N;
        double Pitch;
        double Width;

        StripParameters():
            N{0},
            Pitch{0.0},
            Width{0.0}
        {

        }

        StripParameters(decltype(N) NStrips,
                        decltype(Pitch) P,
                        decltype(Width) W):
                            N{NStrips},
                            Pitch{P},
                            Width{W}
        {

        }

        friend bool
        operator==(const StripParameters& lhs, const StripParameters& rhs)
        {
            return lhs.N == rhs.N &&
                lhs.Pitch == rhs.Pitch &&
                lhs.Width == rhs.Width;
        }

        friend bool
        operator!=(const StripParameters& lhs, const StripParameters& rhs)
        {
            return !(lhs==rhs);
        }
    };

public:
    ///@{ the gang of five
    /// Constructor, destructor, assignment, copy and move
    RectangularDoubleStripLayout();

    RectangularDoubleStripLayout(const StripParameters& Strips_X,
                                 const StripParameters& Strips_Y,
                                 const key_type::id_0& Detector_ID);

    virtual
    ~RectangularDoubleStripLayout() = default;

    RectangularDoubleStripLayout(const RectangularDoubleStripLayout& other);

    RectangularDoubleStripLayout&
    operator=(const RectangularDoubleStripLayout& other) =default;

    RectangularDoubleStripLayout(RectangularDoubleStripLayout&& other);

    RectangularDoubleStripLayout&
    operator=(RectangularDoubleStripLayout&& other) = default;
    ///@}

    /**
     * Returns the identification name of the derived class
     * @return
     */
    virtual std::string
    IsA ( ) const override;

    /// Calculates the bounding box of all the stored polygons based on the iteration
    /// on all the element.
    virtual boundingbox_t
    GetBoundingBox() const override;


private:
    /* ****************************************
     * ****************************************
     * Configurable members
     *
     **************************************** */
    /// Strips configuration for the X direction
    StripParameters m_XStrips;

    /// Strips configuration for the Y direction
    StripParameters m_YStrips;

    /// Index identifying the specific detector in the setup
    key_type::id_0 m_Detector_ID;

    /**
     * Registers additional configuration parameters in the derived class.
     * All the configuration parameters registered this way are automatically
     * configured
     */
    virtual void
    RegisterParameters ( ) override;

    /**
     * Performs additional configurations in the derived class.
     * Notice that all the parameters registered in the parameter list are
     * already automatically updated
     */
    virtual void
    ConfigureMore (const Core::MXConfTree& ConfTree) override;

    /**
     * Polymorphic comparison operator.
     * Called by the normal operator== which checks the equality of the typeid
     * so the user can safely assume that rhs is of the same derived class of this
     * @param rhs
     * @return
     */
    virtual bool
    IsEqual(const IMXData& rhs) const override;

    /**
     * Updates the content of the surface container with a new set of strips
     * calculated from the configuration parameters
     */
    void
    UpdateLayout();

    friend bool
    operator==(const RectangularDoubleStripLayout& lhs, const RectangularDoubleStripLayout& rhs)
    {
        return lhs.m_XStrips == rhs.m_XStrips &&
            lhs.m_YStrips == rhs.m_YStrips &&
            lhs.m_Detector_ID == rhs.m_Detector_ID &&
            lhs.m_Name == rhs.m_Name;
    }


    /**
     * Copies the object polymorphically and returns a raw pointer.
     * Using the return type covariance one can return directly the pointer to the concrete
     * class if the call is done on a pointer to the concrete class
     * @return
     */
    virtual RectangularDoubleStripLayout*
    RawClone ( ) const override;

    virtual RectangularDoubleStripLayout*
    RawCreate ( ) const override;
};

}  // namespace Layout

}  // namespace Merlin

namespace Core
{
/**
 * Template specialization to define the class registration name
 */
template<>
struct
ClassRegistrationTraits<Merlin::Layout::RectangularDoubleStripLayout>
{
    /// @return The class registration name for the factory system
    static std::string
    Name()
    {
        return "RectangularDoubleStripLayout";
    }
};


}  // namespace Core

}  // namespace MXWare



#endif /* MERLIN_MERLIN_LAYOUT_RECTANGULARDOUBLESTRIPLAYOUT_HH_ */
