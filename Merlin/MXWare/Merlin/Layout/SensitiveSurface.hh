/*
 * SensitiveSurface.hh
 *
 *  Created on: Jun 13, 2018
 *      Author: caiazza
 */

#ifndef MERLIN_MERLIN_LAYOUT_SENSITIVESURFACE_HH_
#define MERLIN_MERLIN_LAYOUT_SENSITIVESURFACE_HH_

#include "MXWare/Merlin/MerlinPrimitives.hh"

namespace MXWare
{

namespace Merlin
{

namespace Layout
{

class SensitiveSurface
{
public:
    /// The type representing the surface identifier in the layout where it is stored
    typedef Primitives::LayoutId          id_t;

    /// The type of object which represents the surface shape
    typedef Primitives::Polygon_2         Polygon;

    /// Default constructor
    /// Point at the origin, 0 area and unknown identifier
    SensitiveSurface();

    /// Full constructor
    /// Defines the identifier and the shape
    SensitiveSurface(const id_t& Id, const Polygon& Poly);

    /// Destructor
    /// Polymorphic behavior allowed if necessary
    virtual
    ~SensitiveSurface() = default;

    ///@{
    /// Copy and move
    SensitiveSurface(const SensitiveSurface& other) = default;

    SensitiveSurface&
    operator=(const SensitiveSurface& other) = default;

    SensitiveSurface(SensitiveSurface&& other) = default;

    SensitiveSurface&
    operator=(SensitiveSurface&& other) = default;
    ///@}

    ///@{
    /// Getters and setters
    inline const id_t&
    GetId() const
    {
        return m_Id;
    }

    inline void
    SetId(const id_t& Id)
    {
        m_Id = Id;
    }

    inline const Polygon&
    GetPolygon() const
    {
        return m_Poly;
    }

    inline void
    SetPolygon(const Polygon& Poly)
    {
        m_Poly = Poly;
    }
    ///@}


private:
    id_t m_Id;

    Polygon m_Poly;

    friend bool
    operator==(const SensitiveSurface& lhs, const SensitiveSurface& rhs)
    {
        return lhs.m_Id == rhs.m_Id &&
            lhs.m_Poly == rhs.m_Poly;
    }

    friend bool
    operator!=(const SensitiveSurface& lhs, const SensitiveSurface& rhs)
    {
        return !(lhs == rhs);
    }

};


}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare



#endif /* MERLIN_MERLIN_LAYOUT_SENSITIVESURFACE_HH_ */
