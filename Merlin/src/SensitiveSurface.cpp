/*
 * SensitiveSurface.cpp
 *
 *  Created on: Jun 13, 2018
 *      Author: caiazza
 */

#include "MXWare/Merlin/Layout/SensitiveSurface.hh"

namespace MXWare
{

namespace Merlin
{

namespace Layout
{
SensitiveSurface::SensitiveSurface ( ):
    m_Id{},
    m_Poly{}
{
}

SensitiveSurface::SensitiveSurface (const id_t& Id, const Polygon& Poly):
    m_Id{Id},
    m_Poly{Poly}
{
}



}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare
