/*
 * ReadoutPlane_Base.cpp
 *
 *  Created on: Jun 14, 2018
 *      Author: caiazza
 */

#include "MXWare/Merlin/Layout/ReadoutPlane_Base.hh"
#include "MXWare/Core/MXExceptions.hh"
#include "MXWare/Core/MXLogger.hh"

#include "MXWare/Util/Geometry/Algorithm.hh"
#include "boost/geometry/algorithms/envelope.hpp"
#include "boost/geometry/algorithms/expand.hpp"

#include <sstream>

namespace MXWare
{

namespace Merlin
{

namespace Layout
{

//ReadoutPlane_MapBased::key_type
//ReadoutPlane_MapBased::FirstKey() const
//{
//    if(m_SurfMap.empty())
//        return PastLast();
//    else
//        return m_SurfMap.begin()->first;
//}
//
//ReadoutPlane_MapBased::key_type
//ReadoutPlane_MapBased::Next(const key_type& here) const
//{
//    if(here == PastLast())
//    {
//        return PastLast();
//    }
//
//    auto it =  m_SurfMap.find(here);
//    if(it == m_SurfMap.end())
//    {
//        std::stringstream msg;
//        msg << "No element stored in the readout layout description named " << GetName() <<
//            " with the key: " << here << ".\nUnable to define a Next element";
//        MAGIX_LOG_TRIVIAL(error) << msg.str();
//        Core::ObjectNotFound e{IsA(), BOOST_CURRENT_FUNCTION, msg.str()};
//        e.raise();
//    }
//    ++it;
//    if(it == m_SurfMap.end())
//    {
//        return PastLast();
//    }
//    else
//    {
//        return it->first;
//    }
//}


IReadoutPlane::boundingbox_t
ReadoutPlane_MapBased::GetBoundingBox() const
{
    boundingbox_t ans{};
    if(m_SurfMap.empty())
    {
        return ans;
    }

    boost::geometry::envelope(m_SurfMap.begin()->second.GetPolygon(), ans);

    for(auto& Surf: m_SurfMap)
    {
        // The direct expansion of the bounding box with a polygon is not yet implemented in boost
        boundingbox_t tmp;
        boost::geometry::envelope(Surf.second.GetPolygon(), tmp);
        boost::geometry::expand(ans, tmp);
    }

    return ans;


}



}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare



