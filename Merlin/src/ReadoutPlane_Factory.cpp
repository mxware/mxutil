/*
 * ReadoutPlane_Factory.cpp
 *
 *  Created on: Jul 6, 2018
 *      Author: caiazza
 */

#include "MXWare/Merlin/Layout/ReadoutPlane_Factory.hh"
#include "MXWare/Merlin/Layout/RectangularDoubleStripLayout.hh"

namespace MXWare
{

template
class Core::GeneralizedFactory<Merlin::Layout::IReadoutPlane>;

namespace Merlin
{

namespace Layout
{
void
RegisterDefaultClasses(ReadoutPlane_Factory& Fac)
{
    RegisterReadoutPlane<RectangularDoubleStripLayout>(Fac);
}

void
Configure(std::unique_ptr<IReadoutPlane>& Obj, const Core::MXConfTree& Tree)
{
    // NOTE The pointer is always reset because the use case we want to implement is that
    // if the user gives us a configure request through an interface which is managed by a factory
    // he wants it to set it to a specific implementation in a specific configuration. We don't
    // want to have the user configure class A with the data of class B just because the pointer
    // already has an object configured
    // If we just want to configure an existing object we can pass it to the configure function
    // by reference which should trigger the use of the Configure function
    Obj = ReadoutPlane_Factory::GetGlobalFactory().CreateConfigured(Tree);
}

void
Configure(std::shared_ptr<IReadoutPlane>& Obj, const Core::MXConfTree& Tree)
{
    Obj = ReadoutPlane_Factory::GetGlobalFactory().CreateConfigured(Tree);
}

Core::MXConfTree
PopulateTree(const std::unique_ptr<IReadoutPlane>& Obj)
{
    return ReadoutPlane_Factory::GetGlobalFactory().GenerateConfigurationTree(*Obj);
}


}  // namespace Layout


}  // namespace Merlin

}  // namespace MXWare


