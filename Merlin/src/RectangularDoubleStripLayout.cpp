/*
 * RectangularDoubleStripLayout.cpp
 *
 *  Created on: Jun 15, 2018
 *      Author: caiazza
 */

#include "MXWare/Merlin/Layout/RectangularDoubleStripLayout.hh"
#include "MXWare/Core/MXLogger.hh"
#include "MXWare/Core/MXExceptions.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"
#include "MXWare/Util/SystemOfUnits.hh"
using MXWare::Units::mm;

namespace MXWare
{

namespace Merlin
{

namespace Layout
{
RectangularDoubleStripLayout::RectangularDoubleStripLayout():
    RectangularDoubleStripLayout(StripParameters{}, StripParameters{}, 0)
{

}

RectangularDoubleStripLayout::RectangularDoubleStripLayout(const StripParameters& Strips_X,
                                                           const StripParameters& Strips_Y,
                                                           const key_type::id_0& Detector_ID):
                                                               m_XStrips{Strips_X},
                                                               m_YStrips{Strips_Y},
                                                               m_Detector_ID{Detector_ID}
{
    UpdateLayout();
    RegisterParameters();
}

RectangularDoubleStripLayout::RectangularDoubleStripLayout(const RectangularDoubleStripLayout& other):
    m_XStrips{other.m_XStrips},
    m_YStrips{other.m_YStrips},
    m_Detector_ID{other.m_Detector_ID}
{
    RegisterParameters();
}

RectangularDoubleStripLayout::RectangularDoubleStripLayout(RectangularDoubleStripLayout&& other):
    m_XStrips{std::move(other.m_XStrips)},
    m_YStrips{std::move(other.m_YStrips)},
    m_Detector_ID{std::move(other.m_Detector_ID)}
{
    RegisterParameters();
}

RectangularDoubleStripLayout*
RectangularDoubleStripLayout::RawClone ( ) const
{
    return new RectangularDoubleStripLayout{*this};
}

RectangularDoubleStripLayout*
RectangularDoubleStripLayout::RawCreate ( ) const
{
    return new RectangularDoubleStripLayout{};
}

std::string
RectangularDoubleStripLayout::IsA ( ) const
{
    return Core::ClassRegistrationTraits<RectangularDoubleStripLayout>::Name();
}

void
RectangularDoubleStripLayout::RegisterParameters( )
{
    ReadoutPlane_MapBased::RegisterParameters();

    m_Pars.AddParameter("nstrip_x",
                        "Number of strips on the first axis (X)",
                        m_XStrips.N);

    m_Pars.AddPhysicalParameter("pitch_x",
                                "The pitch of the strips in the x direction (in mm)",
                                m_XStrips.Pitch, mm);

    m_Pars.AddPhysicalParameter("width_x",
                                "The width of the strips in the x direction (in mm)",
                                m_XStrips.Width, mm);

    m_Pars.AddParameter("nstrip_y",
                        "Number of strips on the second axis (Y)",
                        m_YStrips.N);

    m_Pars.AddPhysicalParameter("pitch_y",
                                "The pitch of the strips in the y direction (in mm)",
                                m_YStrips.Pitch, mm);

    m_Pars.AddPhysicalParameter("width_y",
                                "The width of the strips in the y direction (in mm)",
                                m_YStrips.Width, mm);

    m_Pars.AddParameter("detector_id",
                        "Index identifying the specific detector in the setup",
                        m_Detector_ID);
}

void
RectangularDoubleStripLayout::ConfigureMore (const Core::MXConfTree& ConfTree)
{
    UpdateLayout();
}

bool
RectangularDoubleStripLayout::IsEqual (const IMXData& rhs) const
{
    return *this == static_cast<const RectangularDoubleStripLayout&>(rhs);
}


void
RectangularDoubleStripLayout::UpdateLayout()
{
    if(m_XStrips.Pitch < m_XStrips.Width ||
        m_YStrips.Pitch < m_YStrips.Width)
    {
        std::stringstream err;
        err << " The strip pitch should always be larger than its width to avoid overlaps. "
            "The currently set values for the X strips are:\nPitch: " << m_XStrips.Pitch <<
            "\tWidth: " << m_XStrips.Width << "\nThe currently set values for the Y strips are:"
                "\nPitch: " << m_YStrips.Pitch << "\tWidth: " << m_YStrips.Width;
        MAGIX_LOG_TRIVIAL(error) << err.str();
        Core::RequirementFailure e{IsA(), BOOST_CURRENT_FUNCTION, err.str()};
        e.raise();
    }

    double TotalWX = m_XStrips.Pitch * m_XStrips.N;
    double GapX = m_XStrips.Pitch - m_XStrips.Width;
    double TotalWY = m_YStrips.Pitch * m_YStrips.N;
    double GapY = m_YStrips.Pitch - m_YStrips.Width;

    Primitives::geometry_t::Vector_2 XStripBase{m_XStrips.Width, 0.0};
    Primitives::geometry_t::Vector_2 XStripSide{0.0, TotalWY};
    Primitives::geometry_t::Point_2 XStripAnchor{GapX / 2.0, 0.0};
    Primitives::Polygon_2 XStrip_Poly{{XStripAnchor,
                                             XStripAnchor + XStripSide,
                                             XStripAnchor + XStripSide + XStripBase,
                                             XStripAnchor + XStripBase,
                                             XStripAnchor}};

    m_SurfMap.clear();
    auto n = 0U;

    for(; n < m_XStrips.N; ++n)
    {
        key_type Key(m_Detector_ID, n);
        Geometry::Translation<Primitives::geometry_t, 2> TranslateX{m_XStrips.Pitch * n,
                                                                          0.0};

        Primitives::Polygon_2 Poly{};
        Geometry::Transform(XStrip_Poly, Poly, TranslateX);
        m_SurfMap.insert(std::make_pair(Key, SensitiveSurface{Key,Poly}));
    }

    Primitives::geometry_t::Vector_2 YStripBase{0.0, m_YStrips.Width};
    Primitives::geometry_t::Vector_2 YStripSide{TotalWX, 0.0};
    Primitives::geometry_t::Point_2 YStripAnchor{0.0, GapY /2.0};
    Primitives::geometry_t::Polygon_2 YStripPoly{{YStripAnchor,
                                                        YStripAnchor + YStripBase,
                                                        YStripAnchor + YStripBase + YStripSide,
                                                        YStripAnchor + YStripSide,
                                                        YStripAnchor}};

    for(auto YN=0; n < m_XStrips.N + m_YStrips.N; ++n, ++YN)
    {
        key_type Key{m_Detector_ID, n};
        Geometry::Translation<Primitives::geometry_t, 2> TranslateY{0.0,
                                                                          m_YStrips.Pitch * YN};

        Primitives::Polygon_2 Poly{};
        Geometry::Transform(YStripPoly, Poly, TranslateY);
        m_SurfMap.insert(std::make_pair(Key, SensitiveSurface{Key,Poly}));
    }

}


IReadoutPlane::boundingbox_t
RectangularDoubleStripLayout::GetBoundingBox() const
{
    typedef Primitives::geometry_t::Point_2       point_t;
    return boundingbox_t{ point_t{0,0},
        point_t{m_XStrips.Pitch * m_XStrips.N, m_YStrips.Pitch * m_YStrips.N} };
}



}  // namespace Layout

}  // namespace Merlin

}  // namespace MXWare
