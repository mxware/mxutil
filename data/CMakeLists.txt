 ########################################################################################
# Installing new shared data
#    The data folder contains the additional predefined data files the package provides
#    To add a new data file to the installation, add the relative path to the DATA
#    variable.
#    If a data file must be pre-configured this should be done manually using the
#    configure_file command and adding the destination file to the list of DATA 
########################################################################################

# Pre-configure data files if necessary   
configure_file( mass_width_2014.mcd
                ${CMAKE_BINARY_DIR}/data/defaultPartData.mcd
                COPYONLY)
 
# Add the data to install
set( DATA ${CMAKE_BINARY_DIR}/data/defaultPartData.mcd)               
                
if( DEFINED DATA)
    mxware_add_data_file(DATAFILES "${DATA}")
endif( DEFINED DATA)
               
#install( FILES ${CMAKE_BINARY_DIR}/data/defaultPartData.mcd DESTINATION ${DATA_INSTALL_DIR} )