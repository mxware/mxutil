/*
 * AffineTransformation.hh
 *
 *  Created on: Feb 1, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_AFFINETRANSFORMATION_HH_
#define SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_AFFINETRANSFORMATION_HH_


#include "Eigen/Geometry"

namespace MXWare
{

namespace Geometry
{

template <typename Primitives, size_t DimensionCount>
using Translation = Eigen::Translation<typename Primitives::coordinate_t, DimensionCount>;

template <typename Primitives, size_t DimensionCount>
using Rotation = Eigen::Matrix<typename Primitives::coordinate_t, DimensionCount,DimensionCount>;

template <typename Primitives, size_t DimensionCount>
using AffineTransformation = Eigen::Transform<typename Primitives::coordinate_t, DimensionCount, Eigen::Affine>;

template <typename Primitives>
using AxialRotation_3D = Eigen::AngleAxis<typename Primitives::coordinate_t>;

template <typename Primitives>
using AxialRotation_2D = Eigen::Rotation2D<typename Primitives::coordinate_t>;

//template<typename Primitives, size_t DimensionCount>
//class Translation: public Eigen::Translation<typename Primitives::coordinate_t, DimensionCount>
//{
//    typedef Eigen::Translation<typename Primitives::coordinate_t, DimensionCount>   Base;
//public:
//    template <typename... Args>
//    explicit EIGEN_DEVICE_FUNC Translation(Args&&... args):
//        Base(std::forward<Args>(args)...)
//    {
//
//    }
//
//    using Base::operator*;
//
//    EIGEN_DEVICE_FUNC inline Translation
//    operator*(const Base& other) const
//    {
//        return Translation{Base::operator *(other)};
//    }
//
//    EIGEN_DEVICE_FUNC inline Translation
//    operator*(const Translation& other) const
//    {
//        return Translation{Base::operator *(other)};
//    }
//
//    template <typename P1, typename P2>
//    bool
//    apply(const P1& rhs, P2& lhs) const
//    {
//
//        lhs = P2{Base::operator *(rhs.EigenData())};
//        return true;
//    }
//};

//template<class Primitives, size_t DimensionCount>
//class AffineTransformation :
//    public Eigen::Transform<typename Primitives::coordinate_t, DimensionCount, Eigen::Affine>
//{
//    typedef Eigen::Transform<typename Primitives::coordinate_t, DimensionCount, Eigen::Affine>  Base;
//
//public:
//    template <typename... Args>
//    explicit EIGEN_DEVICE_FUNC AffineTransformation(Args&&... args):
//        Base(std::forward<Args>(args)...)
//    {
//
//    }
//
//    template <typename P1, typename P2>
//    bool
//    apply(const P1& rhs, P2& lhs) const
//    {
//
//        lhs = P2{Base::operator *(rhs.EigenData())};
//        return true;
//    }
//
//};

template<typename T>
class EigenBoostStrategyWrapper
{
    T m_Transformation;

public:
    EigenBoostStrategyWrapper(T Transform):
        m_Transformation{Transform}
    {

    }

    template<typename P1, typename P2>
    bool
    apply(const P1& rhs, P2& lhs) const
    {
        lhs = P2{m_Transformation * rhs};
        return true;
    }

};


}  // namespace Geometry

}  // namespace MXWare


#endif /* SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_AFFINETRANSFORMATION_HH_ */
