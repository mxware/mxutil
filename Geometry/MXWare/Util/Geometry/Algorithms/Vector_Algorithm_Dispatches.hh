/*
 * Vector_Algorithm_Dispatches.hh
 *
 *  Created on: Feb 23, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_VECTOR_ALGORITHM_DISPATCHES_HH_
#define SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_VECTOR_ALGORITHM_DISPATCHES_HH_

#include <boost/geometry/algorithms/transform.hpp>
#include <boost/geometry/algorithms/is_empty.hpp>
#include <boost/geometry/algorithms/is_simple.hpp>
#include <boost/geometry/algorithms/is_valid.hpp>
#include <boost/version.hpp>

namespace MXWare
{
namespace Geometry
{
struct Vector_Tag;

}  // namespace Geometry

}


namespace boost
{
namespace geometry
{

namespace dispatch
{

template<typename Vector1, typename Vector2>
struct transform<Vector1, Vector2, MXWare::Geometry::Vector_Tag, MXWare::Geometry::Vector_Tag>
    : detail::transform::transform_point
{

};

/// Implementing the dispatch of the boost is_empty for vectors
/// A vector is never empty
template <typename Geometry>
struct is_empty<Geometry, MXWare::Geometry::Vector_Tag>
    : detail::is_empty::always_not_empty
{};

/// A vector is always simple
template <typename Vec>
struct is_simple<Vec, MXWare::Geometry::Vector_Tag>
    : detail::is_simple::always_simple<Vec>
{};



/// The vector validity is mutuated from that of the points
template <typename Vec>
struct is_valid<Vec, MXWare::Geometry::Vector_Tag>
{
    template <typename VisitPolicy>
    static inline bool apply(Vec const& V, VisitPolicy& visitor)
    {
        boost::ignore_unused(visitor);
        return ! detail::is_valid::point_has_invalid_coordinate::apply(V, visitor);
    }

#if BOOST_VERSION >= 106400
    template <typename VisitPolicy, typename Strategy>
    static inline bool apply(Vec const& V, VisitPolicy& visitor, Strategy const&)
    {
        boost::ignore_unused(visitor);
        return ! detail::is_valid::point_has_invalid_coordinate::apply(V, visitor);
    }
#endif
};


}  // namespace dispatch

}  // namespace geometry

}  // namespace boost


#endif /* SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_VECTOR_ALGORITHM_DISPATCHES_HH_ */
