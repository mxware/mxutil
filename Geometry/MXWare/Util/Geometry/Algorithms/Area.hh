/*
 * Area.hh
 *
 *  Created on: 27 Jan 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_AREA_HH_
#define SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_AREA_HH_

#include "boost/geometry/algorithms/area.hpp"
#include <boost/geometry/strategies/tags.hpp>

#include <boost/geometry/strategies/area.hpp>
#include <boost/geometry/strategies/cartesian/area_surveyor.hpp>
#include <utility>

namespace MXWare
{

namespace Geometry
{

///@{
/// Calculates the area of a primitive, forwarding the call to the boost dispatching system.
/// Any primitive of the boost geometry category is supported
/// See http://www.boost.org/doc/libs/1_66_0/libs/geometry/doc/html/geometry/reference/algorithms/area/area_1.html
/// for more details
/// It takes one or two arguments, which typically automatically deduce the related template parameters
/// The first is the object of which we want the calculate the area, the second is a class
/// representing the calculation method, which in the boost jargon is called the "strategy"
/// This second argument can be used to define specific algorithms to apply to the calculation of
/// area in specific cases
/// @param args
/// @return
//template <typename... Args>
//auto Area(Args&&... args) ->decltype(boost::geometry::area(std::forward<Args>(args)...))
//{
//    return boost::geometry::area(std::forward<Args>(args)...);
//}

template <typename Geometry>
auto Area(Geometry&& G) ->decltype(boost::geometry::area(std::forward<Geometry>(G)))
{
    return boost::geometry::area(std::forward<Geometry>(G));
}

template <typename Geometry, typename Strategy>
auto Area(Geometry&& G, Strategy&& S) ->decltype(boost::geometry::area(std::forward(G), std::forward(S)))
{
    return boost::geometry::area(std::forward(G), std::forward(S));
}


///@}

//template<typename Primitive>
//auto
//Area(Primitive&& Obj) ->typename std::remove_reference<Primitive>::type::coordinate_t
//{
//    return boost::geometry::area(std::forward<Primitive>(Obj));
//}


}  // namespace Geometry


}  // namespace MXWare



#endif /* SOURCE_MXUTIL_GEOMETRY_ALGORITHMS_AREA_HH_ */
