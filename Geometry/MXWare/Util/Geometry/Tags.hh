/**
 * \file Tags.hh
 * The file contains the tag class to characterize the geometry primitive and algorithms
 *
 *  Created on: Jan 25, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_TAGS_HH_
#define SOURCE_MXUTIL_GEOMETRY_TAGS_HH_

#include <boost/geometry/core/tags.hpp>
#include "boost/geometry/core/cs.hpp"

namespace MXWare
{

namespace Geometry
{
/* *****************************************************************
 *              Primitive type tags
 ***************************************************************** */
/// Point identifying tag
typedef boost::geometry::point_tag                  Point_Tag;

typedef boost::geometry::segment_tag                Segment_Tag;

/// Vector identifying tag
struct Vector_Tag :
    boost::geometry::single_tag,
    boost::geometry::linear_tag
{

};







/// Tag identifying a primitive represented in cartesian geometry
/// It is aliassed from the boost equivalent tag system to be able to use
/// those primitives in the boost algorithms directly
typedef boost::geometry::cs::cartesian              CartesianSystem;


struct DegreeAngle_Tag{};

struct RadianAngle_Tag{};

}  // namespace Geometry

}  // namespace MXWare



#endif /* SOURCE_MXUTIL_GEOMETRY_TAGS_HH_ */
