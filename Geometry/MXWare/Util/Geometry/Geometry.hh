/*
 * Geometry.hh
 *
 *  Created on: Jan 18, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_GEOMETRY_HH_
#define SOURCE_MXUTIL_GEOMETRY_GEOMETRY_HH_

#include "MXWare/Util/Geometry/CartesianPrimitives.hh"

namespace MXWare
{
namespace Geometry
{

typedef CartesianPrimitives<double>                     DPCartesian;

#if defined(USING_CGAL)

// If I decide to use CGAL I can create here the CGAL compatible kernel which should
// use the same primitives defined in CartesianPrimitives<double>

#endif


}  // namespace Geometry



}  // namespace MXWare




#endif /* SOURCE_MXUTIL_GEOMETRY_GEOMETRY_HH_ */
