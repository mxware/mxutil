/*
 * Operators.hh
 *
 *  Created on: Feb 21, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_OPERATORS_HH_
#define SOURCE_MXUTIL_GEOMETRY_OPERATORS_HH_


#include "MXWare/Util/Geometry/Primitives/Point.hh"
#include "MXWare/Util/Geometry/Primitives/Vector.hh"


namespace MXWare
{
namespace Geometry
{


template<typename Primitives, std::size_t DimensionCount>
inline Vector<Primitives, DimensionCount>
operator-(const Point<Primitives, DimensionCount>& P1,
          const Point<Primitives, DimensionCount>& P2)
{
    return Vector<Primitives, DimensionCount>{
        static_cast<const typename std::remove_reference<decltype(P1)>::type::Base&>(P1) -
            static_cast<const typename std::remove_reference<decltype(P2)>::type::Base&>(P2)};
}

template<typename Primitives, std::size_t DimensionCount>
inline Point<Primitives, DimensionCount>
operator+(const Point<Primitives, DimensionCount>& P1,
          const Vector<Primitives, DimensionCount>& V1)
{
    return Point<Primitives, DimensionCount>{
        static_cast<const typename std::remove_reference<decltype(P1)>::type::Base&>(P1) +
            static_cast<const typename std::remove_reference<decltype(V1)>::type::Base&>(V1)};
}

template<typename Primitives, std::size_t DimensionCount>
inline Point<Primitives, DimensionCount>
operator-(const Point<Primitives, DimensionCount>& P1,
          const Vector<Primitives, DimensionCount>& V1)
{
    return Point<Primitives, DimensionCount>{
        static_cast<const typename std::remove_reference<decltype(P1)>::type::Base&>(P1) -
            static_cast<const typename std::remove_reference<decltype(V1)>::type::Base&>(V1)};
}

}  // namespace Geometry

}  // namespace MXWare
#endif /* SOURCE_MXUTIL_GEOMETRY_OPERATORS_HH_ */
