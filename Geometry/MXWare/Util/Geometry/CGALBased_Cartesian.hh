/*
 * CGALBased_Cartesian.hh
 *
 *  Created on: 8 Jul 2018
 *      Author: caiazza
 */

#ifndef GEOMETRY_MXWARE_UTIL_GEOMETRY_CGALBASED_CARTESIAN_HH_
#define GEOMETRY_MXWARE_UTIL_GEOMETRY_CGALBASED_CARTESIAN_HH_

#include "CGAL/Cartesian.h"

#include "MXWare/Util/Geometry/Primitives/Point.hh"
#include "MXWare/Util/Geometry/Tags.hh"

namespace MXWare
{

namespace Geometry
{
/*template<class ConstructBase, class Point2>
struct ConstructCGALBBox_2:
    public ConstructBase
{
    using ConstructBase::operator();

    CGAL::Bbox_2 operator()(const Point2& p) const
    {
        return CGAL::Bbox_2{p.get<0>(), p.get<1>(), p.get<0>(), p.get<1>()};
    }
};

template<class ConstructBase, class Point3>
struct ConstructCGALBBox_3:
    public ConstructBase
{
    using ConstructBase::operator();

    CGAL::Bbox_3 operator()(const Point2& p) const
    {
        return CGAL::Bbox_3{p.get<0>(), p.get<1>(), p.get<2>(), p.get<0>(), p.get<1>(), p.get<2>()};
    }
};

template<class Primitives, std::size_t Dim>
struct MXPoint_coord_iterator
{
    typedef typename Primitives::Point_N<Primitives, Dim> Point_t;

    const double* operator()(const Point_t& p)
    {
        return &p.data();
    }

    const double* operator()(const Point_t& p, int)
    {
        return &p.data()[Dim];
    }
};*/


///
/// This class defines an extensible kernel based on the BOOST compatible and Eigen based
/// geometry classes which are defined in the library
/// The system of the CGAL extensible kernels needs a base kernel that we intend to extern,
/// which will define all the types which are not already defined in the extension class
/// The first template parameter is the type of the kernel that will be finally composed
/// by the combination of the class with its base kernel through the type equality wrapper
/// which is used later
/// The second template parameter is the type of CGAL kernel to use as a base type container
/// for this extension.
/// NOTE that the extension is actually derived from a nested class template inside the base
/// kernel which defines the actual container of the components type. This is done because
/// the CGAL kernels are actually passed through an intermediate inheritance layer with the
/// type equality wrapper which we should skip to directly access the type in the base kernel
template<typename FinalK, typename CGAL_BaseK>
class CGALCartesian_Extension:
    public CGAL_BaseK::template Base<FinalK>::Type
{
    /// Alias to the base kernel containing all those aliases to the types which
    /// are not redefined in this extension
    typedef typename CGAL_BaseK::template Base<FinalK>::Type    BaseK;

public:
    /// The type of the final kernel we are going to produce after the type equality wrapper
    typedef FinalK                                              Kernel;
    /// The type of this class
    typedef CGALCartesian_Extension<FinalK, CGAL_BaseK>        Self;

    ///@{
    /// Alias of the concrete type representing the coordinates of the primitives
    typedef typename BaseK::FT                                  FT;
    typedef FT                                                  coordinate_t;
    static_assert(std::is_arithmetic<coordinate_t>::value,
                  "The coordinate representation type of the Cartesian Primitives is not "
                  "an arithmetic type");
    ///@}

    enum CoordinateIndex:
        size_t
    {
        X = 0,
        Y = 1,
        Z = 2
    };

    /// Tag for the type of coordinate system used in this primitive set (Cartesian)
    typedef CartesianSystem                                     coordinatesystem_t;

    /// This declaration imports the template definition inside the struct so that it
    /// can be referred to by other primitives
    template<typename Primitives, std::size_t DimensionCount>
    using Point_N = Point<Primitives, DimensionCount>;

    ///@{ 2-dimensional geometric primitives
    typedef Point_N<Self, 2>                                    Point_2;
    ///@}



    ///@{ 3-dimensional geometric primitives
    typedef Point_N<Self, 3>                                    Point_3;
    ///@}





    /// This structure, when used in combination with the type equality wrapper allows
    /// to derive from the actual base class of the wrapper
    template < typename DerivedK >
    struct Base { typedef CGALCartesian_Extension<DerivedK, CGAL_BaseK>  Type; };
};

/// The type equality wrapper creates aliases of all the base kernel typedefs so that
/// they can be accessed as CGAL::Point_2<Kernel> rather than Kernel::Point_2 and so on
/// It basically acts as an adaptor between a generic kernel and the CGAL algorithms
template<typename FT>
struct CGALBased_Cartesian:
    public CGAL::Type_equality_wrapper<
        CGALCartesian_Extension<CGALBased_Cartesian<FT>, CGAL::Cartesian<FT>>,
        CGALBased_Cartesian<FT>>
{

};


}  // namespace Geometry


}  // namespace MXWare




#endif /* GEOMETRY_MXWARE_UTIL_GEOMETRY_CGALBASED_CARTESIAN_HH_ */
