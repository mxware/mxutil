/**
 * \file Algorithm.hh
 * This file collects all the generalized geometric algorithms based on a
 * certain primitive set
 *
 *  Created on: Jan 26, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_ALGORITHM_HH_
#define SOURCE_MXUTIL_GEOMETRY_ALGORITHM_HH_

#include <utility>
#include <boost/geometry/algorithms/transform.hpp>
#include <boost/geometry/algorithms/is_empty.hpp>
#include <boost/geometry/algorithms/is_simple.hpp>
#include <boost/geometry/algorithms/is_valid.hpp>

#include <boost/geometry/strategies/strategies.hpp>

//#include "boost/geometry/geometry.hpp"


// Core algorithms




#include "MXWare/Util/Geometry/Algorithms/AffineTransformation.hh"
#include "MXWare/Util/Geometry/Algorithms/Area.hh"
#include "MXWare/Util/Geometry/Algorithms/Vector_Algorithm_Dispatches.hh"
#include "MXWare/Util/Geometry/Tags.hh"


namespace MXWare
{

namespace Geometry
{
///@{
/// Generic function to apply the boost geometry transformations.
/// Just forward the arguments to the matching boost function.
/// There are two variants of this template imported from the boost package
/// The first takes only two arguments and it is used to transform one type into another
/// The second has a third parameter which is the transformation strategy which is the one actually used
/// to apply transformations like rotation, translations and so on
/// That third argument should respect the interface of the boost geometry package for the strategy
/// class. That means they need to have an function named "apply" which takes the input and
/// output object as arguments.
/// Having a transformation function comply with the boost strategy system means it will be automatically
/// forwarded to specifically derived functions in case of objects composed of multiple points
/// like segments or polygons
/// @param args
/// @return
template <typename Geometry1, typename Geometry2, typename Strategy>
inline bool Transform(Geometry1 const&& g1,
                      Geometry2&& g2,
                      Strategy const&& strategy)
{
    return boost::geometry::transform(std::forward(g1),
                                      std::forward(g2),
                                      std::forward(strategy));
}

template <typename Geometry1, typename Geometry2>
inline bool Transform(Geometry1 const&& g1,
                      Geometry2&& g2)
{
    return boost::geometry::transform(std::forward(g1),
                                      std::forward(g2));
}
///@}

template <typename Geometry1, typename Geometry2, typename Scalar, int D>
inline bool Transform(const Geometry1& g1,
                      Geometry2& g2,
                      const Eigen::Translation<Scalar, D>& T)
{
    EigenBoostStrategyWrapper<decltype(T)> Wrap{T};
    return boost::geometry::transform(g1,
                                      g2,
                                      Wrap);
}

template <typename Geometry1, typename Geometry2, typename Scalar, int D>
inline bool Transform(const Geometry1& g1,
                      Geometry2& g2,
                      const Eigen::Matrix<Scalar, D, D>& T)
{
    EigenBoostStrategyWrapper<decltype(T)> Wrap{T};
    return boost::geometry::transform(g1,
                                      g2,
                                      Wrap);
}

template <typename Geometry1, typename Geometry2, typename Scalar, int D>
inline bool Transform(const Geometry1& g1,
                      Geometry2& g2,
                      const Eigen::Transform<Scalar, D, Eigen::Affine>& T)
{
    EigenBoostStrategyWrapper<decltype(T)> Wrap{T};
    return boost::geometry::transform(g1,
                                      g2,
                                      Wrap);
}

template <typename Geometry1, typename Geometry2, typename Scalar>
inline bool Transform(const Geometry1& g1,
                      Geometry2& g2,
                      const Eigen::Rotation2D<Scalar>& T)
{
    EigenBoostStrategyWrapper<decltype(T)> Wrap{T};
    return boost::geometry::transform(g1,
                                      g2,
                                      Wrap);
}

///@{ Proxy of the boost functions in the MXGeo namespace
template<typename Geometry>
inline bool
Is_Empty(const Geometry& G)
{
    return boost::geometry::is_empty(G);
}

template <typename Geometry>
inline bool
Is_Simple(const Geometry& G)
{
    return boost::geometry::is_simple(G);
}

template <typename Geometry>
inline bool
Is_Valid(const Geometry& G)
{
    return boost::geometry::is_valid(G);
}
///@}



/*

template <typename Primitive1, typename Primitive2, typename EigenScalar, size_t DimensionCount>
auto Transform(const Primitive1& p1, Primitive2& p2, const Eigen::Translation<EigenScalar, DimensionCount>& Strategy)
{
    p2 = Primitive2{p1.EigenData() * Strategy};
    return true;
}

template <typename Primitive1, typename Primitive2, typename EigenScalar, int DimensionCount, int Mode>
auto Transform(const Primitive1& p1, Primitive2& p2, const Eigen::Transform<EigenScalar, DimensionCount, Mode>& Strategy)
{
    p2 = Primitive2{p1.EigenData() * Strategy};
    return true;
}
*/



//template<typename Primitives, size_t DimensionCount>
//using Translation = Eigen::Translation<typename Primitives::coordinate_t, DimensionCount>;




}  // namespace Geometry

}  // namespace MXWare




#endif /* SOURCE_MXUTIL_GEOMETRY_ALGORITHM_HH_ */
