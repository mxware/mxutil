/*
 * CartesianPrimitives.hh
 *
 *  Created on: Jan 25, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_CARTESIANPRIMITIVES_HH_
#define SOURCE_MXUTIL_GEOMETRY_CARTESIANPRIMITIVES_HH_

#include "MXWare/Util/Geometry/Operators.hh"
#include "MXWare/Util/Geometry/Primitives/GeneralPolygon.hh"
#include "MXWare/Util/Geometry/Primitives/Point.hh"
#include "MXWare/Util/Geometry/Primitives/Segment.hh"
#include "MXWare/Util/Geometry/Primitives/Vector.hh"
#include "MXWare/Util/Geometry/Primitives/BoundingBox.hh"
#include "MXWare/Util/Geometry/Tags.hh"


namespace MXWare
{
namespace Geometry
{
template<typename CoordinateType>
struct CartesianPrimitives
{
    static_assert(std::is_arithmetic<CoordinateType>::value,
                  "The coordinate representation type of the Cartesian Primitives is not "
                  "an arithmetic type");

    /// Typedef to the type's own type
    typedef CartesianPrimitives<CoordinateType>         Self;

    ///@{ Alias of the concrete type representing the coordinates of the primitives
    /// The first is also a type compatible with the CGAL representation
    typedef CoordinateType                              FT;
    typedef CoordinateType                              coordinate_t;
    ///@}

    enum CoordinateIndex:
        size_t
    {
        X = 0,
        Y = 1,
        Z = 2
    };

    /// Tag for the type of coordinate system used in this primitive set (Cartesian)
    typedef CartesianSystem                             coordinatesystem_t;

    /// This declaration imports the template definition inside the struct so that it
    /// can be referred to by other primitives
    template<typename Primitives, std::size_t DimensionCount>
    using Point_N = Point<Primitives, DimensionCount>;

    template<typename Primitives, std::size_t DimensionCount>
    using Vector_N = Vector<Primitives, DimensionCount>;

    template<typename Primitives, std::size_t DimensionCount>
    using Segment_N = Segment<Primitives, DimensionCount>;

    ///@{ 2-dimensional geometric primitives
    typedef Point_N<Self, 2>                    Point_2;
    typedef Vector_N<Self, 2>                   Vector_2;
    typedef Segment_N<Self, 2>                  Segment_2;
    typedef GeneralPolygon<Self>                Polygon_2;
    typedef BoundingBox<Point_2>                Bbox_2;
    ///@}

    ///@{ 3-dimensional geometric primitives
    typedef Point_N<Self, 3>                    Point_3;
    typedef Vector_N<Self, 3>                   Vector_3;
    typedef Segment_N<Self, 3>                  Segment_3;
    typedef BoundingBox<Point_3>                Bbox_3;
    ///@}


};


}  // namespace Geometry

}  // namespace MXWare


#endif /* SOURCE_MXUTIL_GEOMETRY_CARTESIANPRIMITIVES_HH_ */
