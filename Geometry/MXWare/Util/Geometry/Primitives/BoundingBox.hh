/*
 * BoundingBox.hh
 *
 *  Created on: 8 Jul 2018
 *      Author: caiazza
 */

#ifndef GEOMETRY_MXWARE_UTIL_GEOMETRY_PRIMITIVES_BOUNDINGBOX_HH_
#define GEOMETRY_MXWARE_UTIL_GEOMETRY_PRIMITIVES_BOUNDINGBOX_HH_

#include <boost/geometry/geometries/box.hpp>

namespace MXWare
{

namespace Geometry
{
/// A class representing an axis aligned bounding box
template<typename Point>
using BoundingBox = boost::geometry::model::box<Point>;


}  // namespace Geometry


}  // namespace MXWare




#endif /* GEOMETRY_MXWARE_UTIL_GEOMETRY_PRIMITIVES_BOUNDINGBOX_HH_ */
