/*
 * GeneralPolygon.hh
 *
 *  Created on: Feb 6, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_GENERALPOLYGON_HH_
#define SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_GENERALPOLYGON_HH_

#include <boost/geometry/geometries/ring.hpp>
#include <boost/geometry/geometries/concepts/point_concept.hpp>
#include <boost/geometry/core/exterior_ring.hpp>
#include <boost/geometry/core/interior_rings.hpp>
#include <vector>

namespace MXWare
{

namespace Geometry
{
/// Definition of a polygon class, representing a general polygon with holes which
/// is compatible with the boost geometry system and with the CGAL
/// algorithms system.
/// The polygon is represented by a set of rings, using the boost terminology.
/// A ring is a oriented list of points which allow to circulate the polygon and
/// its internal holes
/// Each polygon has a single outer ring and an indefinite number of inner rings
/// representing its holes
/// IMPORTANT The outer ring ordering direction is specified by the second template
/// parameter but the inner rings must be ordered in the opposite direction, otherwise
/// the area calculations end up being wrong. Read the reference of the boost geometry
/// polygon concept for more information
template<typename Primitives, bool OrderClockwise = true>
class GeneralPolygon
{
    /// Alias of the point type defined in the primitivs template parameter
    typedef typename Primitives::Point_2                    Point;

    /// Alias of the type representing the polygon edges
    typedef typename Primitives::Segment_2                  Edge;

    /// An object representing a numerical field (any floating point will do)
    typedef typename Primitives::FT                         FT;

    ///@{
    /// The type of container on which to base the rings and his allocator
    /// Defined in case I want to specify any of those parameters in the template
    /// pack like in the boost geometry polygon model
    template<typename T, typename A = std::allocator<T>>
    using PointList = std::vector<T, A>;

    template<typename T>
    using PointAlloc = std::allocator<T>;
    ///@}

    ///@{
    /// The type of container for the inner rings
    template<typename T, typename A>
    using RingList = std::vector<T, A>;

    template<typename T>
    using RingAlloc = std::allocator<T>;
    ///@}

public:

    /// Ordering  option for the outer ring.
    /// The point sequence inside the boundary vectors are to be interpreted as
    /// clockwise
    static constexpr bool Clockwise{OrderClockwise};

    /// Closure option for the rings and the polygon.
    /// Specifies whether the first and last point of the rings are to be always the
    /// same or not. This is an option in the boost geometry package that seems ignored
    static constexpr bool Closed{false};

    /// The representation kernel used to define this object.
    /// Follows the naming convention of CGAL
    typedef Primitives                                                  R;

    /// The type describing the polygon points
    typedef Point                                                       point_type;

    /// The type describing the rings
    typedef typename boost::geometry::model::ring<
        Point, Clockwise, Closed, PointList, PointAlloc>                ring_type;

    /// the container of the rings of the inner holes
    typedef RingList<ring_type , RingAlloc<ring_type > >                inner_container_type;

    ///@{ Iterate over the polygon vertexes
    typedef typename ring_type::iterator                                Vertex_iterator;
    typedef typename ring_type::const_iterator                          Vertex_const_iterator;
    ///@}

    inline GeneralPolygon()
        : m_outer()
        , m_inners()
    {}

    /// Construct the polygon from a list of boundaries.
    /// The first of the boundaries is the outer, the other are the holes
    inline GeneralPolygon(std::initializer_list<ring_type> l)
        : m_outer(l.size() > 0 ? *l.begin() : ring_type())
        , m_inners(l.size() > 0 ? l.begin() + 1 : l.begin(), l.end())
    {}

    /// Construct a polygon without holes
    inline GeneralPolygon(const ring_type& Outer):
        m_outer{Outer},
        m_inners{}
    {
    }

    /// Construct a polygon without holes using a simple vector of points
    inline GeneralPolygon(const PointList<Point>& Outer):
        m_outer{Outer.begin(), Outer.end()}
    {

    }

    /// Utility method, clears outer and inner rings
    inline void clear()
    {
        m_outer.clear();
        m_inners.clear();
    }

    ///@{ Access the outer boundary of the polygon
    inline ring_type&
    outer()
    {
        return m_outer;
    }

    inline const ring_type&
    outer() const
    {
        return m_outer;
    }
    ///@}

    ///@{ Access to the polygon holes
    inline inner_container_type &
    inners()
    {
        return m_inners;
    }

    inline const inner_container_type&
    inners() const
    {
        return m_inners;
    }
    ///@}


    ///@{
    /// Creates an iterator to the beginning of the outer boundary.
    /// That is the first of the points defined in the outer boundary
    inline Vertex_iterator
    vertices_begin()
    {
        return m_outer.begin();
    }

    inline Vertex_const_iterator
    vertices_begin() const
    {
        return m_outer.cbegin();
    }
    ///@}

    ///@{
    /// Access the past-the-end position in the boundary
    inline Vertex_iterator
    vertices_end()
    {
        return m_outer.end();
    }

    inline Vertex_const_iterator
    vertices_end() const
    {
        return m_outer.cend();
    }
    ///@}





private:
    /// Container of the vertexes of the outer boundary
    ring_type m_outer;

    /// Container of the rings defining the holes in the polygon
    inner_container_type m_inners;

    /// Checks that the template parameter respects the boost point concept
    BOOST_CONCEPT_ASSERT((boost::geometry::concepts::Point<point_type>));
    static_assert(boost::geometry::dimension<point_type>::value == 2,
                  "A sensitive strip is a 2-dimensional polygon and need a 2-dimensional "
                  "point primitive");

    friend bool
    operator==(const GeneralPolygon<Primitives, OrderClockwise>& lhs,
               const GeneralPolygon<Primitives, OrderClockwise>& rhs)
               {
        return lhs.m_outer == rhs.m_outer &&
            lhs.m_inners == rhs.m_inners;
               }

    friend bool
    operator!=(const GeneralPolygon<Primitives, OrderClockwise>& lhs,
               const GeneralPolygon<Primitives, OrderClockwise>& rhs)
               {
        return !(lhs == rhs);
               }

};

}  // namespace Geometry

}  // namespace MXWare




namespace boost {

namespace geometry {

namespace traits {

/// Tag describing the geometry type
template<typename Primitives,bool ClockWise>
struct tag<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef polygon_tag type;
};

///@{
/// Traits specifying the types of the ring and ring containers
template<typename Primitives,bool ClockWise>
struct ring_const_type<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef typename MXWare::Geometry::
        GeneralPolygon<Primitives, ClockWise>::ring_type const&              type;
};

template<typename Primitives,bool ClockWise>
struct ring_mutable_type<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef typename MXWare::Geometry::
        GeneralPolygon<Primitives, ClockWise>::ring_type&                    type;
};

template<typename Primitives,bool ClockWise>
struct interior_const_type<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef typename MXWare::Geometry::
        GeneralPolygon<Primitives, ClockWise>::inner_container_type const&              type;
};

template<typename Primitives,bool ClockWise>
struct interior_mutable_type<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef typename MXWare::Geometry::
        GeneralPolygon<Primitives, ClockWise>::inner_container_type&              type;
};
///@}

///@{
/// Template traits to specify the accessor to the ring functions
template<typename Primitives,bool ClockWise>
struct exterior_ring<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef MXWare::Geometry::GeneralPolygon<Primitives, ClockWise> polygon_type;

    static inline typename polygon_type::ring_type& get(polygon_type& p)
    {
        return p.outer();
    }

    static inline typename polygon_type::ring_type const& get(
                    polygon_type const& p)
    {
        return p.outer();
    }
};

template<typename Primitives,bool ClockWise>
struct interior_rings<MXWare::Geometry::GeneralPolygon<Primitives, ClockWise>>
{
    typedef MXWare::Geometry::GeneralPolygon<Primitives, ClockWise> polygon_type;

    static inline typename polygon_type::inner_container_type& get(
                    polygon_type& p)
    {
        return p.inners();
    }

    static inline typename polygon_type::inner_container_type const& get(
                    polygon_type const& p)
    {
        return p.inners();
    }
};
///@}




}  // namespace traits

}  // namespace geometry

}  // namespace boost


#endif /* SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_GENERALPOLYGON_HH_ */
