/*
 * Vector.hh
 *
 *  Created on: Jan 29, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_VECTOR_HH_
#define SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_VECTOR_HH_

#include "MXWare/Util/Geometry/Concepts/Vector.hh"

#include <cstddef>
#include <type_traits>

#include <boost/mpl/assert.hpp>
#include <boost/geometry/core/coordinate_type.hpp>
#include <boost/geometry/core/coordinate_dimension.hpp>
#include <boost/geometry/core/coordinate_system.hpp>
#include <boost/geometry/core/access.hpp>

#include <boost/geometry/geometries/concepts/check.hpp>

#include "Eigen/Core"
#include "MXWare/Util/Geometry/Tags.hh"

namespace MXWare
{

namespace Geometry
{
/// Definition of a vector class based on the Eigen vectors
/// A geometrical vector is always intended to have the vertex fixed in the origin.
/// A vector
template<typename Primitives,
         std::size_t DimensionCount>
class Vector: public Eigen::Matrix<typename Primitives::coordinate_t, DimensionCount, 1>
{
    BOOST_MPL_ASSERT_MSG((DimensionCount >= 1),
                         DIMENSION_GREATER_THAN_ZERO_EXPECTED,
                         (boost::mpl::int_<DimensionCount>));

public:
    /// Eigen base class of this class
    typedef Eigen::Matrix<typename Primitives::coordinate_t, DimensionCount, 1> Base;

    /// Defines the set of primitives of which this is part of
    /// Allows to define the coordinate type and system and provides extensibility
    typedef Primitives                                  primitives_t;

    /// Alias to the type representing the coordinates
    typedef typename Primitives::coordinate_t           coordinate_t;

    /// Alias to the coordinate system
    typedef typename Primitives::coordinatesystem_t     coordinatesystem_t;

    static auto constexpr D     =   DimensionCount;

private:
    /// Verifies that this object complies with the Vector concept
    BOOST_CONCEPT_ASSERT((Concepts::Vector<Vector<Primitives, DimensionCount>>));

public:

    ///Default constructor
    Vector() = default;

    /// Constructor to set a single value
    /// NOTE
    /// It requires a template parametrization to enable the enable_if template
    /// It requires the template parameter to have a different name to avoid shadowing the
    /// main class template parameter
    /// The constructor cannot be called with a separate template parameter so this will
    /// effectively use the same value used in the main class template parameter
    template<std::size_t D = DimensionCount>
    explicit Vector(const coordinate_t& v0,
                   typename std::enable_if<D == 1>::type* = nullptr):
                   Base(v0)
    {

    }

    /// Constructor to set two values
    /// See the note to the 1D constructor
    template<std::size_t D = DimensionCount>
    explicit Vector(const coordinate_t& v0,
                   const coordinate_t& v1,
                   typename std::enable_if<D == 2>::type* = nullptr):
                   Base(v0, v1)
    {

    }

    /// Constructor to set three values
    /// See the note to the 1 D constructor
    template<std::size_t D = DimensionCount>
    explicit Vector(const coordinate_t& v0,
                   const coordinate_t& v1,
                   const coordinate_t& v2,
                   typename std::enable_if<D == 3>::type* = nullptr):
                   Base(v0, v1, v2)
    {

    }

    /// Generic constructor for N dimensions
    explicit Vector(const std::array<coordinate_t, DimensionCount>& value):
        Base(value.data())
    {
#ifndef __clang__
        static_assert(value.size() >= DimensionCount,
                      "The size of the input array is smaller than the vector dimensionality."
                      " This would introduce an out_of_range error");
#endif
    }

    /// Direct transfer of the eigen data in the vector
    Vector(const Base& Matrix):
        Base{Matrix}
    {

    }

    ///@{
    /// Accessor functions to the vector coordinates
    template <std::size_t K>
    const coordinate_t&
    get() const
    {
        static_assert(K < DimensionCount,
                      "Attempting to access a vector index larger than the vector size");
        return (*this)[K];
    }

    template <std::size_t K>
    void set(coordinate_t const & value)
    {
        static_assert(K < DimensionCount,
                      "Attempting to assign a value to a vector index larger than the vector size");
        (*this)[K] = value;
    }
    ///@}

public:
    /// Note that the container of the coordinates is a fixed size Eigen vector which
    /// requires specific care so that it can be properly vectorized. This is taken care by the
    /// follwoing macro which defines a a placement new and delete so that the matrix can be
    /// defined with the correct memory alignment to be used with the vectorialization instructions
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    ///@{
    /// Vector sum
    inline Vector<Primitives, DimensionCount>&
    operator+=(const Vector<Primitives, DimensionCount>& V2)
    {
        Base::operator+=(static_cast<const Base&>(V2));
        return *this;
    }

    friend inline Vector<Primitives, DimensionCount>
    operator+(const Vector<Primitives, DimensionCount>& V1,
              const Vector<Primitives, DimensionCount>& V2)
    {
        Vector<Primitives, DimensionCount> ans{V1};
        return ans += V2;
    }
    ///@}

    ///@{
    /// Vector difference
    inline Vector<Primitives, DimensionCount>&
    operator-=(const Vector<Primitives, DimensionCount>& V2)
    {
        Base::operator-=(static_cast<const Base&>(V2));
        return *this;
    }

    friend inline Vector<Primitives, DimensionCount>
    operator-(const Vector<Primitives, DimensionCount>& V1,
              const Vector<Primitives, DimensionCount>& V2)
    {
        Vector<Primitives, DimensionCount> ans{V1};
        return ans -= V2;
    }
    ///@}

    ///@{
    /// Scalar product
    template<typename Scalar, typename = typename std::enable_if<std::is_arithmetic<Scalar>::value>::type>
    inline Vector<Primitives, DimensionCount>&
    operator*=(const Scalar& S)
    {
        Base::operator*=(S);
        return *this;
    }

    template<typename Scalar, typename = typename std::enable_if<std::is_arithmetic<Scalar>::value>::type>
    friend inline Vector<Primitives, DimensionCount>
    operator*(const Vector<Primitives, DimensionCount>& V1, const Scalar& S)
    {
        Vector<Primitives, DimensionCount> ans{V1};
        return ans*=S;
    }

    template<typename Scalar, typename = typename std::enable_if<std::is_arithmetic<Scalar>::value>::type>
    friend inline Vector<Primitives, DimensionCount>
    operator*(const Scalar& S, const Vector<Primitives, DimensionCount>& V1)
    {
        Vector<Primitives, DimensionCount> ans{V1};
        return ans*=S;
    }
    ///@}

    inline Vector<Primitives, DimensionCount>
    operator-() const
    {
        return Vector<Primitives, DimensionCount>{Base::operator-()};
    }

};

}  // namespace Geometry

}  // namespace MXWare


/* *****************************************************************************
 *                              BOOST adaptation
 * The next section contains all the boost traits adapter to configure the
 * class within the boost geometry system so that one can directly use the
 * algorithm system and the default implementation that are defined there
 * *****************************************************************************/

namespace boost
{

namespace geometry
{
// This namespace contains the general traits which, when connected to recognized
// tags would enable all the correct dispatching.
// In the case of the vector, as we added a new tag, we need also to add the correct dispatching
// where necessary
namespace traits
{

/// Defines the primitive type tag for the vector class
/// As from the syntactical point of you vector and points have the same
/// interface they have both the same tag.
/// NOTE I tried to add a specific vector tag but the boost system of tagging is very obscure
/// and I still didn't manage to find a way to dispatch the correct algorithms for new tag types
template<typename Primitives,
         std::size_t DimensionCount>
struct tag<MXWare::Geometry::Vector<Primitives, DimensionCount>>
{
    typedef MXWare::Geometry::Vector_Tag   type;
};

/// Defines the correlated point type.
/// NOTE That it is not necessary if the vector as the same tag as the point but it
/// would be necessary when the vector will receive his own specific tag
template<typename Primitives,
         std::size_t DimensionCount>
struct point_type<MXWare::Geometry::Vector<Primitives, DimensionCount>>
{
    typedef typename Primitives::template Point_N<Primitives, DimensionCount>   type;
};

/// Defines the coordinate type, that is the type representing the coordinate of the point
/// for the boost geometry package
template<typename Primitives,
         std::size_t DimensionCount>
struct coordinate_type<MXWare::Geometry::Vector<Primitives, DimensionCount>>
{
    typedef typename MXWare::Geometry::Vector<Primitives, DimensionCount>::coordinate_t         type;
};

/// Defines the coordinate system for the boost geometry package so that it can dispatch
/// the correct calculation functions for the different coordinate systems
template<typename Primitives,
         std::size_t DimensionCount>
struct coordinate_system<MXWare::Geometry::Vector<Primitives, DimensionCount>>
{
    typedef typename MXWare::Geometry::Vector<Primitives, DimensionCount>::coordinatesystem_t    type;
};

/// Defines the number of dimensions of the space in which the point is defined
template<typename Primitives,
         std::size_t DimensionCount>
struct dimension<MXWare::Geometry::Vector<Primitives, DimensionCount>>:
    boost::mpl::int_<DimensionCount>
{

};

/// Defines the accessor functions to the coordinates of the point
template<typename Primitives,
         std::size_t DimensionCount,
         std::size_t Dimension>
struct access<MXWare::Geometry::Vector<Primitives, DimensionCount>, Dimension>
{
    /// Gets the value of the coordinate indexed by dimension
    static inline typename MXWare::Geometry::Vector<Primitives, DimensionCount>::coordinate_t
    get(const MXWare::Geometry::Vector<Primitives, DimensionCount>& p)
    {
        return p.template get<Dimension>();
    }

    /// Sets the value of the coordinate indexed by dimension
    static inline void
    set(MXWare::Geometry::Vector<Primitives, DimensionCount>& p,
        const typename MXWare::Geometry::Vector<Primitives, DimensionCount>::coordinate_t& value)
    {
        p.template set<Dimension>(value);
    }
};


}  // namespace traits

// This namespace contains some dispatch class to define correctly the access
// algorithm to the vector coordinates
namespace core_dispatch
{
///@{
/// Getters and setters for the coordinates of the vector tagged classes for both
/// concrete and pointer type classes
template <typename Vector, typename CoordinateType, std::size_t Dimension>
struct access<MXWare::Geometry::Vector_Tag, Vector, CoordinateType, Dimension, boost::false_type>
{
    static inline CoordinateType get(Vector const& v)
    {
        return traits::access<Vector, Dimension>::get(v);
    }
    static inline void set(Vector& v, CoordinateType const& value)
    {
        traits::access<Vector, Dimension>::set(v, value);
    }
};

template <typename Vector, typename CoordinateType, std::size_t Dimension>
struct access<MXWare::Geometry::Vector_Tag, Vector, CoordinateType, Dimension, boost::true_type>
{
    static inline CoordinateType get(Vector const* v)
    {
        return traits::access<typename boost::remove_pointer<Vector>::type, Dimension>::get(*v);
    }
    static inline void set(Vector* v, CoordinateType const& value)
    {
        traits::access<typename boost::remove_pointer<Vector>::type, Dimension>::set(*v, value);
    }
};
///@}

}  // namespace core_dispatch

// General dispatch namespace containing the function specializations to check the vector concept
namespace dispatch
{

template <typename Geometry>
struct check<Geometry, MXWare::Geometry::Vector_Tag, true>
    : detail::concept_check::check<MXWare::Geometry::Concepts::ConstVector<Geometry> >
{};

template <typename Geometry>
struct check<Geometry, MXWare::Geometry::Vector_Tag, false>
    : detail::concept_check::check<MXWare::Geometry::Concepts::Vector<Geometry> >
{};


}  // namespace dispatch

}  // namespace geometry

}  // namespace boost


#endif /* SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_VECTOR_HH_ */
