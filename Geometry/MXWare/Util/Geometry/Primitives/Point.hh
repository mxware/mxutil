/*
 * Point.hh
 *
 *  Created on: Jan 25, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_POINT_HH_
#define SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_POINT_HH_



#include "MXWare/Util/Geometry/Concepts/Point.hh"

#include "Eigen/Core"
#include <boost/mpl/assert.hpp>
#include <boost/geometry/core/coordinate_type.hpp>
#include <boost/geometry/core/coordinate_dimension.hpp>
#include <boost/geometry/core/coordinate_system.hpp>
#include <boost/geometry/core/access.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>

#include <cstddef>
#include <type_traits>
#include <array>
#include <iostream>

#include "MXWare/Util/Geometry/Tags.hh"

namespace MXWare
{

namespace Geometry
{

/// Definition of a point class based on the Eigen vector
/// A point is defined by its indexde coordinates. What those coordinates
/// represent is typically defined by the coordinate system. For example, in the case of
/// a cartesian coordinate system, for example, the coordinates are those typically defined
/// as x, y and z. In a spherical system they may be rho, theta and phi.
/// Internally the point is represented by an Eigen vector so that the spatial transformation
/// system of Eigen can be used directly
template<typename Primitives,
         std::size_t DimensionCount>
class Point: public Eigen::Matrix<typename Primitives::coordinate_t, DimensionCount, 1>
{
    BOOST_MPL_ASSERT_MSG((DimensionCount >= 1),
                         DIMENSION_GREATER_THAN_ZERO_EXPECTED,
                         (boost::mpl::int_<DimensionCount>));

public:
    /// Eigen base class of this class
    typedef Eigen::Matrix<typename Primitives::coordinate_t, DimensionCount, 1> Base;

    /// Defines the set of primitives of which this is part of
    /// Allows to define the coordinate type and system and provides extensibility
    typedef Primitives                                  primitives_t;

    /// Alias to the type representing the coordinates
    typedef typename Primitives::coordinate_t           coordinate_t;

    /// Alias to the coordinate system
    typedef typename Primitives::coordinatesystem_t     coordinatesystem_t;

    static auto constexpr D     =   DimensionCount;

    typedef Point<Primitives, DimensionCount>           Self;

private:

public:

    ///Default constructor
    Point() = default;

    /// Constructor to set a single value
    /// NOTE
    /// It requires a template parametrization to enable the enable_if template
    /// It requires the template parameter to have a different name to avoid shadowing the
    /// main class template parameter
    /// The constructor cannot be called with a separate template parameter so this will
    /// effectively use the same value used in the main class template parameter
    template<std::size_t D = DimensionCount>
    explicit Point(const coordinate_t& v0,
                   typename std::enable_if<D == 1>::type* = nullptr):
                   Base{v0}
    {

    }

    /// Constructor to set two values
    /// See the note to the 1D constructor
    template<std::size_t D = DimensionCount>
    explicit Point(const coordinate_t& v0,
                   const coordinate_t& v1,
                   typename std::enable_if<D == 2>::type* = nullptr):
                   Base{v0, v1}
    {

    }

    /// Constructor to set three values
    /// See the note to the 1 D constructor
    template<std::size_t D = DimensionCount>
    explicit Point(const coordinate_t& v0,
                   const coordinate_t& v1,
                   const coordinate_t& v2,
                   typename std::enable_if<D == 3>::type* = nullptr):
                   Base{v0, v1, v2}
    {

    }

    /// Generic constructor for N dimensions
    explicit Point(const std::array<coordinate_t, DimensionCount>& value):
        Base{value.data()}
    {
#ifndef __clang__
        static_assert(value.size() >= DimensionCount,
                      "The size of the input array is smaller than the point dimensionality."
                      " This would introduce an out_of_range error");
#endif
    }

    /// Direct transfer of the eigen data in the vector
    Point(const Base& Matrix):
        Base{Matrix}
    {

    }

    ///@{
    /// Accessor functions to the point coordinates
    template <std::size_t K>
    const coordinate_t&
    get() const
    {
        static_assert(K < DimensionCount,
                      "Attempting to access a vector index larger than the vector size");
        return (*this)[K];
    }

    template <std::size_t K>
    void set(coordinate_t const & value)
    {
        static_assert(K < DimensionCount,
                      "Attempting to assign a value to a vector index larger than the vector size");
        (*this)[K] = value;
    }
    ///@}

    ///@{
    /// Accessor to the point coordinates via the typical x, y and z function.
    /// The second template argument enables the corresponding coordinates only
    /// if the point dimensionality is large enough
    /// NOTE Removed because this would be misleading for non-cartesian points and to
    /// enable it only for cartesian points I would need to include the tag and formally
    /// depend on a specific tag which is not necessary
//    const coordinate_t&
//    x() const
//    {
//        return this->template get<0>();
//    }
//
//    template<std::size_t D = DimensionCount,
//        typename std::enable_if< D >= 2 >::type* = nullptr>
//    const coordinate_t&
//    y() const
//    {
//        return this->template get<1>();
//    }
//
//    template<std::size_t D = DimensionCount,
//        typename std::enable_if< D >= 3 >::type* = nullptr>
//    const coordinate_t&
//    z() const
//    {
//        return this->template get<2>();
//    }
    ///@}

    /// Direct access to the internal Eigen data structure
    /// Note that typically this should not be done and the coordinates should be accessed
    /// with the get and set template function while the transformations of a point should
    /// be performed with the appropriate transformations free functions which are the one
    /// that should use this function if necessary
//    const decltype(m_Vec)&
//    EigenData() const
//    {
//        return m_Vec;
//    }

public:
    /// Note that the container of the coordinates is a fixed size Eigen vector which
    /// requires specific care so that it can be properly vectorized. This is taken care by the
    /// follwoing macro which defines a a placement new and delete so that the matrix can be
    /// defined with the correct memory alignment to be used with the vectorialization instructions
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    friend std::ostream&
    operator<<(std::ostream& out, const Point<Primitives, DimensionCount> & P)
    {
        out << boost::geometry::wkt(P);
//        out << '(' << P(0);
//
//        for(auto i =1u; i< DimensionCount; ++i)
//        {
//            out << ',' << P(i);
//        }
//        out << ')';

        return out;
    }

    ///@{ Comparison operators
//    friend bool
//    operator==(const Point<Primitives, DimensionCount>& lhs, const Point<Primitives, DimensionCount>& rhs)
//    {
//        return lhs.m_Vec == rhs.m_Vec;
//    }
//
//    friend bool
//    operator!=(const Point<Primitives, DimensionCount>& lhs, const Point<Primitives, DimensionCount>& rhs)
//    {
//        return !(lhs == rhs);
//    }
    ///@}
};

}  // namespace Geometry

}  // namespace MXWare


namespace boost {

namespace geometry {

namespace traits {

/// Defines the primitive type tag for the point class identifying the class as a
/// point primitive to the boost geometry package
template<typename Primitives,
         std::size_t DimensionCount>
struct tag<MXWare::Geometry::Point<Primitives, DimensionCount>>
{
    typedef MXWare::Geometry::Point_Tag   type;
};

/// Defines the coordinate type, that is the type representing the coordinate of the point
/// for the boost geometry package
template<typename Primitives,
         std::size_t DimensionCount>
struct coordinate_type<MXWare::Geometry::Point<Primitives, DimensionCount>>
{
    typedef typename MXWare::Geometry::Point<Primitives, DimensionCount>::coordinate_t         type;
};

/// Defines the coordinate system for the boost geometry package so that it can dispatch
/// the correct calculation functions for the different coordinate systems
template<typename Primitives,
         std::size_t DimensionCount>
struct coordinate_system<MXWare::Geometry::Point<Primitives, DimensionCount>>
{
    typedef typename MXWare::Geometry::Point<Primitives, DimensionCount>::coordinatesystem_t    type;
};

/// Defines the number of dimensions of the space in which the point is defined
template<typename Primitives,
         std::size_t DimensionCount>
struct dimension<MXWare::Geometry::Point<Primitives, DimensionCount>>:
    boost::mpl::int_<DimensionCount>
{

};

/// Defines the accessor functions to the coordinates of the point
template<typename Primitives,
         std::size_t DimensionCount,
         std::size_t Dimension>
struct access<MXWare::Geometry::Point<Primitives, DimensionCount>, Dimension>
{
    /// Gets the value of the coordinate indexed by dimension
    static inline typename MXWare::Geometry::Point<Primitives, DimensionCount>::coordinate_t
    get(const MXWare::Geometry::Point<Primitives, DimensionCount>& p)
    {
        return p.template get<Dimension>();
    }

    /// Sets the value of the coordinate indexed by dimension
    static inline void
    set(MXWare::Geometry::Point<Primitives, DimensionCount>& p,
        const typename MXWare::Geometry::Point<Primitives, DimensionCount>::coordinate_t& value)
    {
        p.template set<Dimension>(value);
    }
};


}  // namespace traits

}  // namespace geometry

}  // namespace boost


#endif /* SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_POINT_HH_ */
