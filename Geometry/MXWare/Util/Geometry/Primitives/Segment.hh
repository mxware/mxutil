/*
 * Segment.hh
 *
 *  Created on: Jan 26, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_SEGMENT_HH_
#define SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_SEGMENT_HH_

#include <cstddef>
#include <boost/concept/assert.hpp>
#include <boost/geometry/geometries/concepts/point_concept.hpp>
#include <boost/geometry/geometries/concepts/segment_concept.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/geometry/core/tags.hpp>
#include <boost/geometry/core/access.hpp>

namespace MXWare
{

namespace Geometry
{
/// Template class defining a segment as a function of the geometry primitives
/// A segment requires the Point_N primitive template to be defined in the Primitives template
/// parameter
/// An object of Segment class is a directed straight line segment in the N dimensional Euclidean space
/// The segment is topologically closed, i.e. the end points belong to it.
/// Point p is called the source and q is called the target of s.
/// The length of s is the Euclidean distance between p and q.
/// To comply with the CGAL interface the first point is accessed through the source function
/// and the second through the target function
template<typename Primitives, std::size_t DimensionCount>
class Segment
{
    BOOST_MPL_ASSERT_MSG((DimensionCount >= 1),
                         DIMENSION_GREATER_THAN_ZERO_EXPECTED,
                         (boost::mpl::int_<DimensionCount>));

public:
    /// Defines the set of primitives of which this is part of
    /// Allows to define the coordinate type and system and provides extensibility
    typedef Primitives                                  primitives_t;

    /// Definition of the point primitive forming the segment
    typedef typename Primitives::template Point_N<Primitives, DimensionCount>        Point;

    /// Definition of the self type
    typedef Segment<Primitives, DimensionCount>                             Self;

    /// Dimensionality of the segment
    static auto constexpr D     =   DimensionCount;

private:

    BOOST_CONCEPT_ASSERT( (boost::geometry::concepts::Point<Point>) );
    BOOST_CONCEPT_ASSERT( (boost::geometry::concepts::Segment<Self>) );

    /// The starting point of the segment
    Point       m_Source;

    /// The ending point of the segment
    Point       m_Target;


public:
    /// Default constructor
    Segment() = default;

    /// Full construction of the segment with 2 point objects
    Segment(const Point& p1, const Point& p2):
        m_Source{p1},
        m_Target{p2}
    {

    }

    /// Direct access (copy) to the source of the segment
    Point
    source() const
    {
        return m_Source;
    }

    /// Direct access (copy) to the target of the segment
    Point
    target() const
    {
        return m_Target;
    }


private:
    /// Provides the direct access templates direct access to the internal points
    /// NOTE I cannot find a way to give the access only to the correct partial
    /// specializations of that template. So theoretically also a indexed_access trait
    /// class for a box should be able to access the internals but that is extremely
    /// unlikely because one should also specifically redesign the get and set classes
    /// of the trait object
    template <typename Geometry, std::size_t Index, std::size_t Dimension>
    friend struct boost::geometry::traits::indexed_access;

    friend std::ostream&
    operator<<(std::ostream& out, const Segment<Primitives, DimensionCount>& S)
    {
        out << S.m_Source << '\t' << S.m_Target;
        return out;
    }

    friend bool
    operator==(const Segment<Primitives, DimensionCount>&lhs, const Segment<Primitives, DimensionCount>& rhs)
    {
        return (lhs.m_Source == rhs.m_Source) &&
            (lhs.m_Target == rhs.m_Target);
    }

    friend bool
    operator!=(const Segment<Primitives, DimensionCount>&lhs, const Segment<Primitives, DimensionCount>& rhs)
    {
        return !(lhs==rhs);
    }
};

}  // namespace Geometry

}  // namespace MXWare


namespace boost
{

namespace geometry
{

namespace traits
{
/// Tag identifying the Segment class as a segment
template<typename Primitives, std::size_t DimensionCount>
struct tag<MXWare::Geometry::Segment<Primitives, DimensionCount>>
{
    typedef segment_tag                             type;
};

/// Tag identifying the underlying point type
template<typename Primitives, std::size_t DimensionCount>
struct point_type<MXWare::Geometry::Segment<Primitives, DimensionCount>>
{
    typedef typename MXWare::Geometry::Segment<Primitives, DimensionCount>::Point       type;
};

/// Trait class to access the first of the two points of the segment
template<typename Primitives, std::size_t DimensionCount, std::size_t Dimension>
struct indexed_access<MXWare::Geometry::Segment<Primitives, DimensionCount>, 0, Dimension>
{
    typedef MXWare::Geometry::Segment<Primitives, DimensionCount> segment_type;
    typedef typename geometry::coordinate_type<segment_type>::type coordinate_type;

    static inline coordinate_type get(segment_type const& s)
    {
        return geometry::get<Dimension>(s.source());
    }

    static inline void set(segment_type& s, coordinate_type const& value)
    {
        geometry::set<Dimension>(s.m_Source, value);
    }
};

/// Trait class to access the second of the two point of the segment
template<typename Primitives, std::size_t DimensionCount, std::size_t Dimension>
struct indexed_access<MXWare::Geometry::Segment<Primitives, DimensionCount>, 1, Dimension>
{
    typedef MXWare::Geometry::Segment<Primitives, DimensionCount> segment_type;
    typedef typename geometry::coordinate_type<segment_type>::type coordinate_type;

    static inline coordinate_type get(segment_type const& s)
    {
        return geometry::get<Dimension>(s.target());
    }

    static inline void set(segment_type& s, coordinate_type const& value)
    {
        geometry::set<Dimension>(s.m_Target, value);
    }
};

}  // namespace traits

}  // namespace geometry

}  // namespace boost


#endif /* SOURCE_MXUTIL_GEOMETRY_PRIMITIVES_SEGMENT_HH_ */
