/*
 * Point.hh
 *
 *  Created on: Feb 21, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_CONCEPTS_POINT_HH_
#define SOURCE_MXUTIL_GEOMETRY_CONCEPTS_POINT_HH_

#include "boost/geometry/geometries/concepts/point_concept.hpp"

namespace MXWare
{

namespace Geometry
{

namespace Concepts
{

template <typename T>
using Point = boost::geometry::concepts::Point<T>;

template <typename T>
using ConstPoint = boost::geometry::concepts::ConstPoint<T>;


}  // namespace Concepts

}  // namespace Geometry

}  // namespace MXWare



#endif /* SOURCE_MXUTIL_GEOMETRY_CONCEPTS_POINT_HH_ */
