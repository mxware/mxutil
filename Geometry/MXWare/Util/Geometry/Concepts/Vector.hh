/*
 * Vector_Concept.hh
 *
 *  Created on: Jan 30, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_GEOMETRY_CONCEPTS_VECTOR_HH_
#define SOURCE_MXUTIL_GEOMETRY_CONCEPTS_VECTOR_HH_

#include <boost/concept_check.hpp>

#include <boost/geometry/core/coordinate_type.hpp>
#include <boost/geometry/core/coordinate_system.hpp>
#include <boost/geometry/core/coordinate_dimension.hpp>
#include <boost/geometry/core/access.hpp>

namespace MXWare
{

namespace Geometry
{

namespace Concepts
{

template <typename Geometry>
class Vector
{
    typedef typename boost::geometry::coordinate_type<Geometry>::type ctype;
    typedef typename boost::geometry::coordinate_system<Geometry>::type csystem;

    enum { ccount = boost::geometry::dimension<Geometry>::value };


    template <typename V, std::size_t Dimension, std::size_t DimensionCount>
    struct dimension_checker
    {
        static void apply()
        {
            V* v = 0;
            boost::geometry::set<Dimension>(*v, boost::geometry::get<Dimension>(*v));
            dimension_checker<V, Dimension+1, DimensionCount>::apply();
        }
    };


    template <typename V, std::size_t DimensionCount>
    struct dimension_checker<V, DimensionCount, DimensionCount>
    {
        static void apply() {}
    };

public:

    /// BCCL macro to apply the Vector concept
    BOOST_CONCEPT_USAGE(Vector)
    {
        static const bool cs_check = ::boost::is_same<csystem, CartesianSystem>::value;
        BOOST_MPL_ASSERT_MSG(cs_check, NOT_IMPLEMENTED_FOR_THIS_CS, (csystem));

        dimension_checker<Geometry, 0, ccount>::apply();
    }
};


template <typename Geometry>
class ConstVector
{
    typedef typename boost::geometry::coordinate_type<Geometry>::type ctype;
    typedef typename boost::geometry::coordinate_system<Geometry>::type csystem;

    enum { ccount = boost::geometry::dimension<Geometry>::value };

    template <typename V, std::size_t Dimension, std::size_t DimensionCount>
    struct dimension_checker
    {
        static void apply()
        {
            const V* v = 0;
            ctype coord(boost::geometry::get<Dimension>(*v));
            boost::ignore_unused_variable_warning(coord);
            dimension_checker<V, Dimension+1, DimensionCount>::apply();
        }
    };


    template <typename V, std::size_t DimensionCount>
    struct dimension_checker<V, DimensionCount, DimensionCount>
    {
        static void apply() {}
    };

public:

    /// BCCL macro to apply the ConstVector concept
    BOOST_CONCEPT_USAGE(ConstVector)
    {
        static const bool cs_check = ::boost::is_same<csystem, CartesianSystem>::value;
        BOOST_MPL_ASSERT_MSG(cs_check, NOT_IMPLEMENTED_FOR_THIS_CS, (csystem));

        dimension_checker<Geometry, 0, ccount>::apply();
    }
};


}  // namespace Concepts

}  // namespace Geometry

}  // namespace MXWare



#endif /* SOURCE_MXUTIL_GEOMETRY_CONCEPTS_VECTOR_HH_ */
