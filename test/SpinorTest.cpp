/*
 * SpinorTest.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE SpinorTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Util/Spinor.hh"
using MXWare::Util::Spinor;
using MXWare::Util::LorentzVector;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
using namespace MXWare::Test;

BOOST_AUTO_TEST_CASE(Construction)
{
    Spinor S;
    BOOST_CHECK(S[0] == Complex{0.0});
    BOOST_CHECK(S[1] == Complex{0.0});
    BOOST_CHECK(S[2] == Complex{0.0});
    BOOST_CHECK(S[3] == Complex{0.0});
    cout << "Default Spinor: " << S << std::endl;

    Spinor S2{Complex{1,2}, Complex{2,1}, Complex{2, 2}, Complex{1, 1} };
    cout << "Component constructed: " << S2 << std::endl;

    Spinor Part{LorentzVector{1, 1, 1, 4}, true};
    cout << "Particle from 4-Momentum: " << Part << std::endl;

    Spinor Anti{LorentzVector{1, 1, 1, 4}, true, false};
    cout << "Anti-Particle from 4-Momentum: " << Anti << std::endl;
}

BOOST_AUTO_TEST_CASE(CopyAndMove)
{
    Spinor S{Complex{1,2}, Complex{2,1}, Complex{2, 2}, Complex{1, 1} };
    CopyAndMoveTest(std::move(S));
}

BOOST_AUTO_TEST_CASE(Arithmetics)
{
    Spinor S0;
    BOOST_CHECK(S0[0] == Complex{0.0});
    BOOST_CHECK(S0[1] == Complex{0.0});
    BOOST_CHECK(S0[2] == Complex{0.0});
    BOOST_CHECK(S0[3] == Complex{0.0});

    Spinor S1{Complex{1,2}, Complex{2,1}, Complex{2, 2}, Complex{1, 1} };
    cout << "Component constructed S1: " << S1 << std::endl;
    cout << "S1 Adjoint: " << !S1 << std::endl;

    Complex C0{0.0, 0.0};
    Complex C{1.0, 0.0};

    BOOST_CHECK( (S0 + S1) == S1);
    BOOST_CHECK( (S0 += S1) == S1);
    BOOST_CHECK( (S0 -= S1) == Spinor{});
    BOOST_CHECK( (S1 - S0) == S1);
    BOOST_CHECK( (S0 * S1) == C0);
    BOOST_CHECK( (S1 * C) == S1);
    BOOST_CHECK( (S1 / C) == S1);



}
