/**
 * \file RangeTest.cpp
 *
 *  Created on: Apr 29, 2015
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE RangeTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Util/Range.hh"
using namespace MXWare::Util;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
using namespace MXWare::Test;

BOOST_AUTO_TEST_CASE(Construction)
{
    cout << "Testing the construction of the range template" << endl;

    Range<double> R;
    BOOST_CHECK(R.Min() == R.Max());
    BOOST_CHECK(R.Min() == 0.0);

    std::pair<double, double> Pair{2.1, 0.1};
    Range<double> R2{Pair};
    BOOST_CHECK(R2.Min() == Pair.second);
    BOOST_CHECK(R2.Max() == Pair.first);

    Range<double> R3{2.1, 0.1};
    BOOST_CHECK(R3.Min() == Pair.second);
    BOOST_CHECK(R3.Max() == Pair.first);
}

BOOST_AUTO_TEST_CASE(CopyAndMove)
{
    Range<double> R{2.1, 0.1};
    CopyAndMoveTest(std::move(R));
}


BOOST_AUTO_TEST_CASE(StreamingTest)
{
    RangeD MyRange{1.11, 85.05};
    std::cout << "Testing the input and output streaming" << std::endl;

    std::cout << "Initial value: " << MyRange<< std::endl;
    std::stringstream tmp;
    tmp << MyRange;

    RangeD CopyRange{};
    tmp >> CopyRange;

    BOOST_CHECK(MyRange == CopyRange);
}

BOOST_AUTO_TEST_CASE(ConftreeStreamingTest)
{
    std::cout << "Testing the streaming to a configuration tree"<< std::endl;
    RangeD Input{1.11, 85.05};
    MXWare::Core::MXConfTree TestTree;

    BOOST_CHECK(TestTree.empty());

    TestTree.put("range", Input);
    BOOST_CHECK(!TestTree.empty());

    auto Output = TestTree.get<RangeD>("range");
    BOOST_CHECK(Input == Output);
}

BOOST_AUTO_TEST_CASE(OverlapAndLogicTest)
{
    std::cout << "Testing the overlap and logical operations on ranges" << std::endl;
    RangeD FirstRange{0, 3};
    RangeD OverlappingRange{2, 5};
    RangeD DisjointRange{8,11};

    BOOST_CHECK(FirstRange.Overlap(FirstRange));
    BOOST_CHECK(FirstRange.Overlap(OverlappingRange));
    BOOST_CHECK(OverlappingRange.Overlap(FirstRange));
    BOOST_CHECK(!FirstRange.Overlap(DisjointRange));
    BOOST_CHECK(!DisjointRange.Overlap(FirstRange));

    auto JoinedRanges = FirstRange || OverlappingRange;
    std::cout << "Joined range: " << JoinedRanges << std::endl;
    BOOST_CHECK(JoinedRanges.Length() == 5);

    auto IntersectionRanges = JoinedRanges && OverlappingRange;
    std::cout << "Intersected ranges: " << IntersectionRanges << std::endl;
    BOOST_CHECK((JoinedRanges && OverlappingRange) == OverlappingRange);
}

BOOST_AUTO_TEST_CASE(ArithmeticTest)
{
    std::cout << "Testing the arithmentic operation on double precision ranges" << std::endl;

    RangeD FirstRange{0, 3};
    RangeD SummedRange = FirstRange + 2.0;
    BOOST_CHECK(SummedRange == RangeD(2, 5));
    BOOST_CHECK(FirstRange == SummedRange - 2.0);

    RangeD MultipliedRange = SummedRange * 3.0;
    BOOST_CHECK(MultipliedRange == RangeD(6, 15));
    BOOST_CHECK(SummedRange == MultipliedRange / 3.0);

    std:: cout << "Value at 25% of the range " << MultipliedRange << ": " <<
        ProportionalExtract(MultipliedRange, 0.25) << std::endl;
}
