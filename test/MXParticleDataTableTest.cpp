/*
 * MXParticleDataTableTest.cpp
 *
 *  Created on: May 21, 2015
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE MXParticleDataTableTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include "boost/filesystem.hpp"
namespace bfs = boost::filesystem;

#include <iostream>
using std::cout;
using std::endl;

#include <thread>
#include <vector>
#include <mutex>

#include "MXWare/Util/MXParticleDataTable.hh"
#include "MXWare/Core/MXExceptions.hh"

using namespace MXWare::Util;
using namespace MXWare;

BOOST_AUTO_TEST_CASE(Construction)
{
    cout << "Testing the construction of an empty ParticleData object" << endl;
    MXParticleData Data1{};
    cout << Data1 << endl;

    cout <<"Testing the construction of an empty table" << endl;
    MXParticleDataTable Table;
    cout << Table.size() << endl;
    BOOST_CHECK(Table.size() == 0);

}

BOOST_AUTO_TEST_CASE(ParticleDataManip)
{
    MXParticleData Data1{};
    cout << "Empty Data: " << Data1 << endl;

    Data1.SetPID(MXParticleID{11});
    Data1.SetMass(ExperimentalValue{0.511, 0.0});

    cout << "Basic data: " << Data1 << endl;

    MXParticleData Data2{};
    cout << "Empty Data: " << Data2 << endl;


}

BOOST_AUTO_TEST_CASE(LoadData)
{
    cout << "Testing the loading of the data in the table" << endl;

    MXParticleDataTable Table;

    try
    {
        cout << "First I load a wrong file" << endl;
        bfs::path FName{"./wrong.dat"};
        Table.LoadData(FName);

        cout <<"The file name was wrong, we should never get here" << endl;
        BOOST_CHECK(false);

    }
    catch(Core::FileNotFound& e)
    {
        cout << e.what() << endl;
        cout << "This is ok as the filename was wrong on purpose" << endl;
        BOOST_CHECK(true);
    }

    try
    {
        cout << "Now I use the correct one" << endl;
        bfs::path FName{"./../data/defaultPartData.mcd"};
        Table.LoadData(FName);

        BOOST_CHECK(Table.size() > 0);

        //BOOST_CHECK(g_ParticleData.size() > 0);

    }
    catch(Core::FileNotFound& e)
    {
        cout << e.what() << endl;
        cout << "This is not ok as we should have loaded the right file" << endl;
        BOOST_CHECK(false);
    }

    for(auto Data : Table)
    {
        cout << Data << endl;
    }
    //MXParticleData Positron = Table.m_Data.find(-11)->second;

    cout << *Table.find(1000922350) << endl;
    cout << *Table.find(11) << endl;
    cout << *Table.find(-11) << endl;
    cout << *Table.find(113) << endl;
    cout << "Uranium: " << Table.find(1000922350)->Name() << endl;
    //auto& Positron = Table.GetData(-11);
    //cout << "Positron: " << Positron.Name() << " charge " << Positron.Charge()
    //    << " mass: " << Positron.Mass().Value() << endl;
    //auto& Electron = Table.GetData(11);
    //cout << "Electron: " << Electron.Name() << " charge " << Electron.Charge()
    //    << " mass: " << Electron.Mass().Value() << endl;
    //auto& Down = Table.GetData(113);
    //cout << "Pion: " << Down.Name() << " charge " << Down.Charge()
    //    << " mass: " << Down.Mass().Value() << endl;
    //cout << Down << std::endl;

    //BOOST_CHECK(Table.GetData(-11).Charge() > 0);
    //BOOST_CHECK(Table.GetData(11).Charge() < 0);

    Table.clear();
    BOOST_CHECK(Table.size() == 0);

    //cout << g_ParticleData.GetData(1000922350).Name() << endl;


}

BOOST_AUTO_TEST_CASE(LoadGlobalData)
{
    cout << "Loading particle data in the global particle table." << endl;

    auto& g_ParticleData = MXParticleDataTable::GlobalPDataTable();
    cout << "Global table current size: " << g_ParticleData.size() << endl;

    bfs::path FName{"./../data/defaultPartData.mcd"};
    BOOST_CHECK(g_ParticleData.size() == 0);
    g_ParticleData.LoadData(FName);
    cout << g_ParticleData.PopulateTree() << endl;
    BOOST_CHECK(g_ParticleData.size() > 0);

    BOOST_CHECK(MXParticleDataTable::GlobalPDataTable().size() > 0);
}

BOOST_AUTO_TEST_CASE(SaveData)
{
    auto& g_ParticleData = MXParticleDataTable::GlobalPDataTable();
    cout << "Global table current size: " << g_ParticleData.size() << endl;
    BOOST_CHECK(g_ParticleData.size() > 0);

    bfs::path FName{"./ParticleData.xml"};
    g_ParticleData.SaveData(FName);

}

