/*
 * GeometryPolygonTest.cpp
 *
 *  Created on: 2 Feb 2018
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE GeometryPolygonTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/SystemOfUnits.hh"
#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

#include "boost/geometry/geometries/polygon.hpp"
#include "boost/geometry/geometries/point.hpp"

typedef MXGeo::CartesianPrimitives<double>              Primitives;
typedef Primitives::Point_2                             Point_2;
typedef Primitives::Polygon_2                           Polygon;

BOOST_AUTO_TEST_CASE(PolygonConstruction)
{
    std::cout << "Testing the construction and access of the polygon primitive" << std::endl;

    auto DefaultPoly = mt::GeneralClassConstructionTest<Polygon>();

    BOOST_CHECK(MXGeo::Is_Empty(DefaultPoly));
    BOOST_CHECK(!MXGeo::Is_Simple(DefaultPoly));
    BOOST_CHECK(!MXGeo::Is_Valid(DefaultPoly));
    Point_2 p1{0,0};
    Point_2 p2{0,2.5};
    Point_2 p3{2.5, 2.5};
    Point_2 p4{2.5, 0};

    Polygon::ring_type Outer{p1, p2, p3, p4};
    BOOST_CHECK(MXGeo::Is_Valid(Outer));
    std::vector<Point_2> OuterV{p1, p2, p3, p4};

    Point_2 pi_1{1, 1};
    Point_2 pi_2{1, 2};
    Point_2 pi_3{2, 2};
    Point_2 pi_4{2, 1};
    Polygon::ring_type Inner{pi_1, pi_2, pi_3, pi_4};
    Polygon::ring_type InnerReverse{pi_4, pi_3, pi_2, pi_1};
    auto PolyWrong = mt::GeneralClassConstructionTest<Polygon>(std::initializer_list<Polygon::ring_type>{Outer, Inner});
    BOOST_CHECK(!MXGeo::Is_Valid(PolyWrong));
    BOOST_CHECK(!MXGeo::Is_Empty(PolyWrong));
    Polygon PolyRight{Outer, InnerReverse};
    BOOST_CHECK(MXGeo::Is_Valid(PolyRight));
    BOOST_CHECK(MXGeo::Is_Simple(PolyRight));



    auto Poly_O1 = mt::GeneralClassConstructionTest<Polygon>(Outer);
    auto Poly_O2 = mt::GeneralClassConstructionTest<Polygon>(OuterV);


}


BOOST_AUTO_TEST_CASE(BaseAlgorithmsTest)
{
    std::cout << "Testing the area algorithm on the polygon primitive" << std::endl;

    Point_2 p1{0,0};
    Point_2 p2{0,2.5};
    Point_2 p3{2.5, 2.5};
    Point_2 p4{2.5, 0};

    Polygon::ring_type Outer{p1, p2, p3, p4, p1};

    Point_2 pi_1{1, 1};
    Point_2 pi_2{1, 2};
    Point_2 pi_3{2, 2};
    Point_2 pi_4{2, 1};
    Polygon::ring_type Inner{pi_4, pi_3, pi_2, pi_1};

    Polygon Poly{std::initializer_list<Polygon::ring_type>{Outer, Inner}};
    auto area = MXGeo::Area(Poly);
    BOOST_CHECK(area == 5.25);
    std::cout << "Polygon Area:\t" << area << std::endl;
}

BOOST_AUTO_TEST_CASE(SpatialTransformationTest)
{
    std::cout << "Applying the spatial transformations"<< std::endl;

    Point_2 p1{0,0};
    Point_2 p2{0,2.5};
    Point_2 p3{2.5, 2.5};
    Point_2 p4{2.5, 0};

    Polygon::ring_type Outer{p1, p2, p3, p4};
    Polygon Poly{std::initializer_list<Polygon::ring_type>{Outer}};
    Polygon Poly_T;
    Polygon Poly_I;

    MXGeo::Translation<Primitives, 2> Translate{4, 2};
    MXGeo::Translation<Primitives, 2> InverseTranslate{Translate.inverse()};
    std::cout << "Translation vector:\n" << Translate.vector() << std::endl;
    std::cout << "Original polygon:\n";
    for(auto& Vertex: Poly.outer())
    {
        std::cout << Vertex << '\t';
    }
    std::cout << std::endl;
    MXGeo::Transform(Poly, Poly_T, Translate);
    std::cout << "Translated polygon:\n";
    for(auto& Vertex: Poly_T.outer())
    {
        std::cout << Vertex << '\t';
    }
    std::cout << std::endl;
    std::cout << "Inverse Translation:\n" << InverseTranslate.vector() << std::endl;
    MXGeo::Transform(Poly_T, Poly_I, InverseTranslate);
    std::cout << "Translation invertend polygon:\n";
    for(auto& Vertex: Poly_I.outer())
    {
        std::cout << Vertex << '\t';
    }
    std::cout << std::endl;

    std::cout << "Polygon rotation" << std::endl;
    MXGeo::AxialRotation_2D<Primitives> R1{MXWare::Units::halfpi};
    MXGeo::Transform(Poly, Poly_T, R1);
    std::cout << "Original polygon:\n";
    for(auto& Vertex: Poly.outer())
    {
        std::cout << Vertex << '\t';
    }
    std::cout << std::endl;
    std::cout << "Rotated polygon:\n";
    for(auto& Vertex: Poly_T.outer())
    {
        std::cout << Vertex << '\t';
    }
    std::cout << std::endl;



}
