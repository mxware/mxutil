/*
 * MXParticleDataTest.cpp
 *
 *  Created on: Mar 3, 2017
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE MXParticleDataTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <MXWare/Core/MXConfTree.hh>

#include <iostream>

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
#include "MXWare/Util/MXParticleData.hh"
#include "MXWare/Util/MXUtil.hh"

BOOST_AUTO_TEST_CASE(ConstructionTest)
{
    std::cout << "Testing the constructors" << std::endl;
    MXWare::Util::MXParticleData EmptyParticle{};
    BOOST_CHECK(EmptyParticle.PID() == 0);
    BOOST_CHECK(EmptyParticle.Mass().GetValue() == 0.0);
    BOOST_CHECK(EmptyParticle.Width().GetValue() == 0.0);

    MXWare::Util::MXParticleData FakeElectron{11, "Felectron",
        MXWare::Util::ExperimentalValue{0.889, 0.02},
        MXWare::Util::ExperimentalValue{0.0025, 0.001, 0.00001} };
    BOOST_CHECK(FakeElectron.PID() == 11);
    BOOST_CHECK(FakeElectron.Mass().GetValue() == 0.889);
    BOOST_CHECK(FakeElectron.Width().GetValue() == 0.0025);
}

BOOST_AUTO_TEST_CASE(CopyAndMove)
{
    std::cout << "Testing the copy an move" << std::endl;
    MXWare::Test::CopyAndMoveTest(
        MXWare::Util::MXParticleData{11, "Felectron",
        MXWare::Util::ExperimentalValue{0.889, 0.02},
        MXWare::Util::ExperimentalValue{0.0025, 0.001, 0.00001} });
}

BOOST_AUTO_TEST_CASE(SimpleStreaming)
{
    std::cout << "Testing the streaming of the Particle Data" << std::endl;
    MXWare::Util::MXParticleData FakeElectron{11, "Felectron",
        MXWare::Util::ExperimentalValue{0.889, 0.02},
        MXWare::Util::ExperimentalValue{0.0025, 0.001, 0.00001} };

    std::cout << FakeElectron << std::endl;
}
