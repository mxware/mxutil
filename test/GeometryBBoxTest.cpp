/*
 * GeometryBBoxTest.cpp
 *
 *  Created on: 8 Jul 2018
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE GeometryBBoxTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

#include <boost/geometry/algorithms/envelope.hpp>
#include <boost/geometry/algorithms/expand.hpp>

typedef MXGeo::CartesianPrimitives<double>                  Primitives;
typedef Primitives::Point_2                                 Point_2;
typedef Primitives::Point_3                                 Point_3;
typedef Primitives::Bbox_2                                  Bbox_2;
typedef Primitives::Bbox_3                                  Bbox_3;
typedef Primitives::Segment_2                               Segment_2;
typedef Primitives::Segment_3                               Segment_3;

BOOST_AUTO_TEST_CASE(BoundingBoxConstruction)
{
    std::cout << "Testing the construction of a bounding box from the points" << std::endl;

    Point_2 a2{1,3};
    Point_2 b2{3,2.2};
    auto Box2D = mt::GeneralClassConstructionTest<Bbox_2>(a2, b2);

    std::cout << "2D bounding box: " << Box2D.min_corner() <<
        "\t" << Box2D.max_corner()<< std::endl;

    Point_3 a3{-1.3, 2.4, 0.0};
    Point_3 b3{0.2, -1.5, 1.2};
    auto Box3D = mt::GeneralClassConstructionTest<Bbox_3>(a3, b3);

    std::cout << "3D bounding box: " << Box3D.min_corner() <<
        "\t" << Box3D.max_corner() << std::endl;
}

BOOST_AUTO_TEST_CASE(EnvelopeTest)
{
    std::cout << "Testing the envelope on points" << std::endl;
    Point_2 a2{1,3};
    Bbox_2 Box2D{};
    boost::geometry::envelope(a2, Box2D);

    std::cout << "Point 2D bounding box: " << Box2D.min_corner() <<
        "\t" << Box2D.max_corner()<< std::endl;

    Point_2 b2{3,2.2};
    Segment_2 s2{a2, b2};
    boost::geometry::envelope(s2, Box2D);

    std::cout << "Segment 2D bounding box: " << Box2D.min_corner() <<
        "\t" << Box2D.max_corner()<< std::endl;


    Point_3 a3{-1.3, 2.4, 0.0};
    Bbox_3 Box3D{};
    boost::geometry::envelope(a3, Box3D);

    std::cout << "3D bounding box: " << Box3D.min_corner() <<
        "\t" << Box3D.max_corner() << std::endl;


}

BOOST_AUTO_TEST_CASE(ExpandTest)
{
    std::cout << "Testing the expansion of the bounding box" << std::endl;
    Point_2 a2{1,3};
    Bbox_2 Box2D{};
    boost::geometry::envelope(a2, Box2D);

    Point_2 b2{3,2.2};
    boost::geometry::expand(Box2D, b2);
    std::cout << "Expanded 2D bounding box: " << Box2D.min_corner() <<
        "\t" << Box2D.max_corner()<< std::endl;


}

