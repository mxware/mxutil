/*
 * GeometryPointTest.cpp
 *
 *  Created on: Jan 22, 2018
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE GeometryPointTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <type_traits>
#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

#include "boost/geometry/algorithms/is_valid.hpp"

#include "Eigen/Geometry"

typedef MXGeo::CartesianPrimitives<double>                  Primitives;
typedef Primitives::Point_2                                 CartesianPoint_R2D;
typedef Primitives::Point_3                                 CartesianPoint_R3D;
typedef Primitives::Vector_3                                CartesianVector_R3D;

/// Checks that the class respects the boost point concept
BOOST_CONCEPT_ASSERT((MXGeo::Concepts::Point<CartesianPoint_R2D>));
BOOST_CONCEPT_ASSERT((MXGeo::Concepts::Point<CartesianPoint_R3D>));

BOOST_AUTO_TEST_CASE(PointConstruction)
{
    typedef MXGeo::CartesianPrimitives<double>::CoordinateIndex CI;

    std::cout << "testing the construction and access of the point primitive" << std::endl;
    auto x = 12.2;
    auto y = 156.1;
    auto z = 44.3;
    auto Point1D = mt::GeneralClassConstructionTest<
        MXGeo::Point<MXGeo::CartesianPrimitives<double>, 1>>(x);
    std::cout << "Point 1D:\tx(" << Point1D.get<0>() << ")" << std::endl;
    BOOST_CHECK(Point1D.get<0>() == x);

    auto Point2D = mt::GeneralClassConstructionTest<CartesianPoint_R2D>(x, y);
    std::cout << "Point2D:\tx(" << Point2D.get<0>() << ")\ty(" << Point2D.get<1>() << ")" << std::endl;
    BOOST_CHECK(Point2D.get<0>() == x);
    BOOST_CHECK(Point2D.get<1>() == y);
    BOOST_CHECK(!MXGeo::Is_Empty(Point2D));
    BOOST_CHECK(MXGeo::Is_Simple(Point2D));
    BOOST_CHECK(MXGeo::Is_Valid(Point2D));

    auto Point3D = mt::GeneralClassConstructionTest<CartesianPoint_R3D>(x, y, z);
    std::cout << "Point3D:\tx(" << Point3D.get<0>() <<
        ")\ty(" << Point3D.get<1>() <<
        ")\tz(" << Point3D.get<2>() << ")" << std::endl;
    BOOST_CHECK(Point3D.get<0>() == x);
    BOOST_CHECK(Point3D.get<1>() == y);
    BOOST_CHECK(Point3D.get<2>() == z);
    BOOST_CHECK(Point3D.get<CI::X>() == x);
    BOOST_CHECK(Point3D.get<CI::Y>() == y);
    BOOST_CHECK(Point3D.get<CI::Z>() == z);

    std::array<double, 3> Coord{{x, y, z}};
    auto Point3D_A = mt::GeneralClassConstructionTest<CartesianPoint_R3D>(Coord);
    std::cout << "Point3D_A:\tx(" << Point3D_A.get<0>() <<
        ")\ty(" << Point3D_A.get<1>() <<
        ")\tz(" << Point3D_A.get<2>() << ")" << std::endl;
    BOOST_CHECK(Point3D_A.get<0>() == x);
    BOOST_CHECK(Point3D_A.get<1>() == y);
    BOOST_CHECK(Point3D_A.get<2>() == z);

    BOOST_CHECK(Point3D == Point3D_A);
}

BOOST_AUTO_TEST_CASE(BaseAlgorithmsTest)
{
    std::cout << "Testing the area algorithm on the point primitive" << std::endl;
    CartesianPoint_R3D Point1{1,1,1};
    auto area = MXGeo::Area(Point1);
    BOOST_CHECK(area == 0);
    std::cout << "Point 3D area: " << area << std::endl;
    std::cout << "Return type: " << typeid(area).name() << std::endl;

    CartesianPoint_R2D Point2{1, 2};
    area = MXGeo::Area(Point2);
    std::cout << "Point 2D area: " << area << std::endl;

}

BOOST_AUTO_TEST_CASE(PointTransformationTest)
{
    std::cout << "Testing the point transformation functions" << std::endl;
    CartesianPoint_R3D Point1{1,1,1};
    CartesianPoint_R3D Point2{};
    CartesianPoint_R3D Point3{};

//    bgt::translate_transformer<double, 3, 3> PointTranslate{2, -1, 0};
//    PointTranslate.apply(Point1, Point2);
//    std::cout << Point1 << "\t->\t" << Point2 << std::endl;
//
//    MXGeo::Transform(Point1, Point3, PointTranslate);
//    std::cout << Point1 << "\t->\t" << Point3 << std::endl;



    MXGeo::Translation<Primitives, 3> Translate{4, 2, -1};
    MXGeo::Translation<Primitives, 3> InverseTranslate{Translate.inverse()};
    std::cout << "Eigen translation type:\t" << typeid(Translate).name() << '\n' << Translate.vector() << std::endl;
    std::cout << "Eigen translated:\t" << typeid(decltype(Translate * Point1)).name() <<
        '\n' << Translate * Point1 << std::endl;

    MXGeo::Transform(Point1, Point3, Translate);
    std::cout << Point1 << "\t->\t" << Point3 << std::endl;

    std::cout << "Eigen translation type:\t" << typeid(InverseTranslate).name() << '\n' << InverseTranslate.vector() << std::endl;
    std::cout << "Eigen translated and inverted:\t"
        << typeid(decltype(Translate * InverseTranslate * Point1)).name()
        << '\n' << Translate * InverseTranslate * Point1 << std::endl;

    MXGeo::Transform(Point1, Point3, Translate * InverseTranslate);
    std::cout << Point1 << "\t->\t" << Point3 << std::endl;

    MXGeo::Rotation<Primitives, 3> R1{Eigen::AngleAxis<double>{0.15, MXGeo::Vector<Primitives, 3>{0,0,1}}};
    MXGeo::Transform(Point1, Point2, R1);
    std::cout << Point1 << "\t->\t" << Point2 << std::endl;
}

BOOST_AUTO_TEST_CASE(PointAlgebraTest)
{
    CartesianPoint_R3D Point1{1, 1, 1};
    CartesianPoint_R3D Point2{3, 3, 3};
    CartesianVector_R3D DiffVec{2, 2, 2};

    auto V1 = Point2 - Point1;
    BOOST_CHECK((std::is_same<decltype(V1), CartesianVector_R3D>::value));
    std::cout << "Difference Vector: " << V1 << std::endl;
    BOOST_CHECK(V1 == DiffVec);

    auto Point3 = Point1 + V1;
    BOOST_CHECK((std::is_same<decltype(Point3), CartesianPoint_R3D>::value));
    std::cout << "Translated point: " << Point3 << std::endl;
    BOOST_CHECK(Point3 == Point2);

    auto Point4 = Point2 - V1;
    BOOST_CHECK((std::is_same<decltype(Point4), CartesianPoint_R3D>::value));
    std::cout << "Reverse translated point: " << Point4 << std::endl;
    BOOST_CHECK(Point4 == Point1);

    auto Point5 = Point2 + (-V1 );
    BOOST_CHECK((std::is_same<decltype(Point5), CartesianPoint_R3D>::value));
    std::cout << "Reverse translated point: " << Point5 << std::endl;
    BOOST_CHECK(Point4 == Point5);
}

