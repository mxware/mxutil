/*
 * TensorTest.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE TensorTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;


#include "MXWare/Util/Tensor.hh"
using MXWare::Util::Tensor;

#include "MXWare/Util/Spinor.hh"
using MXWare::Util::Spinor;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
using namespace MXWare::Test;
using namespace MXWare::Util;


BOOST_AUTO_TEST_CASE(Construction)
{
    Tensor T;
    BOOST_CHECK( T[0][0] == Complex{0.0} );
    cout << "Default Tensor:\n" << T << endl;

    typedef Tensor::Row TR;
    TR Row0{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 0.0) } };
    TR Row1{ {Complex(1.0, 0.0), Complex(0.0, 1.0), Complex(0.0, 0.0), Complex(0.0, 0.0) } };
    TR Row2{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(2.0, 0.0), Complex(0.0, 1.0) } };
    TR Row3{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 1.0), Complex(1.0, 0.0) } };

    Tensor T1{Row0, Row1, Row2, Row3};
    cout << "\nConstructed from rows (T1):\n" << T1 << endl;

    Tensor T2{ TR(Row0), TR(Row1), TR(Row2), TR(Row3)};
    cout << "\nConstructed from r-value rows (T2):\n" << T2 << endl;

    BOOST_CHECK(T1 == T2);

    LorentzVector P{10.0, 100.0, 20.0, 100.0};
    Tensor T3{P};
    cout << "\nConstructed from 4-Momentum:\n" << T3 << endl;


}

BOOST_AUTO_TEST_CASE(CopyAndMove)
{
    typedef Tensor::Row TR;
    TR Row0{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 0.0) } };
    TR Row1{ {Complex(1.0, 0.0), Complex(0.0, 1.0), Complex(0.0, 0.0), Complex(0.0, 0.0) } };
    TR Row2{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(2.0, 0.0), Complex(0.0, 1.0) } };
    TR Row3{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 1.0), Complex(1.0, 0.0) } };

    Tensor T{Row0, Row1, Row2, Row3};
    CopyAndMoveTest(std::move(T));
}

BOOST_AUTO_TEST_CASE(Arithmetics)
{
    cout << "Arithmetics check" << endl;
    Tensor T0;
    cout << "Default Tensor:\n" << T0 << endl;

    typedef Tensor::Row TR;
    TR Row0{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 0.0) } };
    TR Row1{ {Complex(1.0, 0.0), Complex(0.0, 1.0), Complex(0.0, 0.0), Complex(0.0, 0.0) } };
    TR Row2{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(2.0, 0.0), Complex(0.0, 1.0) } };
    TR Row3{ {Complex(1.0, 0.0), Complex(0.0, 0.0), Complex(0.0, 1.0), Complex(1.0, 0.0) } };

    Tensor T1{Row0, Row1, Row2, Row3};
    cout << "Second operand:\n" << T1 << endl;

    cout << "Tensor sum:\n" << T0 + T1 << endl;

    Complex K0{ 0, 0 };
    Complex K1{ 1, 0 };

    BOOST_CHECK( (T0 + T1) == T1);
    BOOST_CHECK( (T0 += T1) == T1);
    BOOST_CHECK( (T0 -= T1) == Tensor{});
    BOOST_CHECK( (T1 - T0) == T1);
    BOOST_CHECK( (T1 * K0) == T0);
    BOOST_CHECK( (T1 * K1) == T1);
    BOOST_CHECK( (T1 / K1) == T1);

    auto Diag = T1.Diagonal();
    cout << "Diagonal: (" << Diag[0] << "," << Diag[1] << "," << Diag[2] << "," << Diag[3] << ")" << std::endl;

    auto RDiag = T1.RDiagonal();
    cout << "Real Diagonal: (" << RDiag[0] << "," << RDiag[1] << "," << RDiag[2] << "," << RDiag[3] << ")" << std::endl;

    auto Identity = Tensor::Id();
    cout << "Identity Tensor:\n" << Identity << endl;

    BOOST_CHECK( (T1 * Identity) == T1 );

    cout << "Null Tensor:\n" << Tensor::Null() << std::endl;
    cout << "Metric Tensor:\n" << Tensor::G_mn() << std::endl;

    for(auto i = 0; i<5; ++i)
    {
        cout << "Gamma" << i << " Tensor:\n" << Tensor::Gamma(i) << endl;
    }

    Spinor S{Complex{1,2}, Complex{2,1}, Complex{2, 2}, Complex{1, 1} };
    cout << "Tensor-Spinor Multiplication:\n" << Identity*S << std::endl;
    BOOST_CHECK( (Identity*S) == S);
/*
    auto Sigma = Tensor::SigmaSpin();
    size_t RowId = 0;
    cout << "Spin tensor:\n";
    for(auto& Row: Sigma)
    {
        size_t ColumnId = 0;
        for(auto& T: Row)
        {
            cout << "[" << RowId << "] [" << ColumnId++<< "]:\n" << T<< std::endl;
        }
        RowId++;
    }*/

}








