/*
 * VectorsTest.cpp
 *
 *  Created on: Mar 6, 2017
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE VectorsTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <sstream>

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
using namespace MXWare::Test;

#include "MXWare/Util/MXUtil.hh"
#include "MXWare/Core/MXConfTree.hh"
namespace MXUtil = MXWare::Util;


BOOST_AUTO_TEST_CASE(Construction)
{
    std::cout << "Testing the construction of a Lorentz vector" << std::endl;
    auto E = GeneralClassConstructionTest<MXUtil::LorentzVector>(11.0, 221.1, 221.1, 0.0);

    std::cout << "Testing the construction of a three vector" << std::endl;
    auto P = GeneralClassConstructionTest<MXUtil::ThreeVec>(221.1, 221.1, 0.0);

    std::cout << "Testing the construction of a two vector" << std::endl;
    auto S = GeneralClassConstructionTest<MXUtil::TwoVec>(221.1, 0.0);
}

BOOST_AUTO_TEST_CASE(CopyAndMove)
{
    std::cout << "Testing the copy and move operations" << std::endl;
    MXUtil::LorentzVector E{11.0, 221.1, 221.1, 0.0};
    CopyAndMoveTest(std::move(E));

    MXUtil::ThreeVec P{221.1, 221.1, 0.0};
    CopyAndMoveTest(std::move(P));

    MXUtil::TwoVec S{221.1, 0.0};
    CopyAndMoveTest(std::move(S));
}

BOOST_AUTO_TEST_CASE(BasicStreamingTest)
{
    MXUtil::LorentzVector E{11.0, 221.1, 221.1, 0.0};
    DoBasicStreamingTest(E);

    MXUtil::ThreeVec P{221.1, 221.1, 0.0};
    DoBasicStreamingTest(P);

    MXUtil::TwoVec S{221.1, 0.0};
    DoBasicStreamingTest(S);
}

BOOST_AUTO_TEST_CASE(ConfTreeStreamingTest)
{
    MXUtil::LorentzVector E{11.0, 221.1, 221.1, 0.0};
    DoConfigurationCycleTest(E);

    MXUtil::ThreeVec P{221.1, 221.1, 0.0};
    DoConfigurationCycleTest(P);

    MXUtil::TwoVec S{221.1, 0.0};
    DoConfigurationCycleTest(S);

}

