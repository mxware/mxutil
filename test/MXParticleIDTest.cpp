/*
 * MXParticleIDTest.cpp
 *
 *  Created on: May 28, 2015
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE MXParticleIDTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"

#include <MXWare/Core/MXConfTree.hh>

#include "MXWare/Util/MXParticleID.hh"
using MXWare::Util::MXParticleID;
#include "MXWare/Util/MXUtil.hh"

BOOST_AUTO_TEST_CASE(Construction)
{
    cout << "Testing the constructor of the ParticleID object" << endl;
    MXParticleID Id{11};
    BOOST_CHECK(Id.Charge() < 0.0);
    BOOST_CHECK(Id.IsLepton());
    BOOST_CHECK(!Id.IsBaryon());

    Id = -11;
    BOOST_CHECK(Id.Charge() > 0.0);
    BOOST_CHECK(Id.IsLepton());
    BOOST_CHECK(!Id.IsBaryon());

    Id = MXParticleID{2212};
    BOOST_CHECK(Id.Charge()> 0.0);
    BOOST_CHECK(Id.IsBaryon());

    int32_t id_int = static_cast<int32_t>(Id);
    BOOST_CHECK(id_int == 2212);

    Id = MXParticleID{1000922350};
    BOOST_CHECK(Id.IsNucleus());
    BOOST_CHECK(Id.A() == 235);
    BOOST_CHECK(Id.Z() == 92);
}

BOOST_AUTO_TEST_CASE(CopyAndMove)
{
    cout << "Testing the copy an move" << endl;
    MXWare::Test::CopyAndMoveTest(MXParticleID{11});
}


BOOST_AUTO_TEST_CASE(SimpleStreaming)
{
    int32_t InputPID = 11;
    cout << "Testing the out-streaming of the MXParticleID class." <<
        "\nThe input PID is: " << InputPID << std::endl;

    MXParticleID Id{0};
    cout << "Initial PID: " << Id << std::endl;

    std::stringstream msg;
    msg << InputPID;

    msg >> Id;
    cout << "Streamed in PID: " << Id << std::endl;

    BOOST_CHECK(InputPID == Id.Pid());
}

BOOST_AUTO_TEST_CASE(ConfTreeStreaming)
{
    std::cout << "Testing the property tree input and output" << std::endl;

    MXParticleID Id{11};
    MXWare::Core::MXConfTree Tree;

    Tree.put("PID", Id);

    BOOST_CHECK(!Tree.empty());

    MXParticleID IDRead = Tree.get<MXParticleID>("PID");
    std::cout << "Id read from the tree: " << IDRead << std::endl;
    BOOST_CHECK(Id == IDRead);



}
