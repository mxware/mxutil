/*
 * CGAL_BasicGeometryTest.cpp
 *
 *  Created on: 8 Jul 2018
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE GeometryPointTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/Geometry/CGALBased_Cartesian.hh"
namespace MXGeo = MXWare::Geometry;

typedef MXGeo::CGALBased_Cartesian<double>              Primitives;
typedef Primitives::Point_2::Rep                           CartesianPoint_R2D;
typedef Primitives::Point_3::Rep                             CartesianPoint_R3D;

/// Checks that the class respects the boost point concept
//BOOST_CONCEPT_ASSERT((MXGeo::Concepts::Point<CartesianPoint_R2D>));
//BOOST_CONCEPT_ASSERT((MXGeo::Concepts::Point<CartesianPoint_R3D>));

BOOST_AUTO_TEST_CASE(PointAlgorithmsTests)
{
    CartesianPoint_R2D a{2, 5};
    CartesianPoint_R2D b{1, 1};

    std::cout << typeid(CartesianPoint_R2D).name() << std::endl;


    Primitives::Compute_squared_distance_2 squared_distance;
    std::cout << "squared_distance(a, b) == "
              << squared_distance(a, b) << std::endl;




}

