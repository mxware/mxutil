/*
 * RangeSetTest.cpp
 *
 *  Created on: Jun 6, 2017
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE RangeTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>

#include "MXWare/Util/RangeSet.hh"

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt =  MXWare::Test;
using namespace MXWare;

BOOST_AUTO_TEST_CASE(ConstructionTest)
{
    std::cout << "Testing the construction of the range set"
        " template for different arithmetic types" << std::endl;

    auto RSD = mt::GeneralClassConstructionTest<Util::RangeSet<double>>();
    BOOST_CHECK(RSD.empty());
    BOOST_CHECK(RSD.size() == 0);
    BOOST_CHECK(RSD.Length() == 0.0);

    auto RSI = mt::GeneralClassConstructionTest<Util::RangeSet<int>>();

    auto RSS = mt::GeneralClassConstructionTest<Util::RangeSet<short>>(Util::Range<short>{-2, 5});
    BOOST_CHECK(!RSS.empty());
    BOOST_CHECK(RSS.size() == 1);
    BOOST_CHECK(RSS.Length() == 7);

    Util::RangeSet<double> RS_InitList{
        Util::RangeD{-1, 2.5}, Util::RangeD{2, 5.0}, Util::RangeD{-10,-4.2}};
    BOOST_CHECK(RS_InitList.size() == 2);
    for(auto& Val: RS_InitList)
    {
        std::cout << Val << std::endl;
    }
    std::cout << "Total length: " << RS_InitList.Length()<< std::endl;
}

BOOST_AUTO_TEST_CASE(InsertionTest)
{
    std::cout << "Testing the element insertions" << std::endl;
    Util::RangeSet<int> RSI{Util::Range<int>{-2, 5}};
    std::cout << "First element: " << *(RSI.begin())<< std::endl;

    // Adding a range on the right
    RSI.insert(Util::Range<int>{8, 12});
    BOOST_CHECK(RSI.size() == 2);

    // Insertion of an already included range
    RSI.insert(-2, 1);
    std::cout << "Number of elements: " << RSI.size() <<
        "\tFirst element: " << *(RSI.begin())<< std::endl;
    BOOST_CHECK(RSI.size() == 2);

    // Extending a matching range
    RSI.insert(-2, 6);
    std::cout << "Number of elements: " << RSI.size() <<
        "\tFirst element: " << *(RSI.begin())<< std::endl;
    BOOST_CHECK(RSI.size() == 2);

    // Extending a range on the left
    RSI.insert(-3, 6);
    std::cout << "Number of elements: " << RSI.size() <<
            "\tFirst element: " << *(RSI.begin())<< std::endl;
    BOOST_CHECK(RSI.size() == 2);

    // Extending a range on the right
    RSI.insert(3, 7);
    std::cout << "Number of elements: " << RSI.size() <<
            "\tFirst element: " << *(RSI.begin())<< std::endl;
    BOOST_CHECK(RSI.size() == 2);

    // Adding a range on the left
    RSI.insert(-10, -7);
    std::cout << "Number of elements: " << RSI.size() <<
            "\tFirst element: " << *(RSI.begin())<< std::endl;
    BOOST_CHECK(RSI.size() == 3);

    // Bridging the gap
    RSI.insert(-8, 0);
    std::cout << "Number of elements: " << RSI.size() <<
            "\tFirst element: " << *(RSI.begin())<< std::endl;
    BOOST_CHECK(RSI.size() == 2);

    BOOST_CHECK(RSI.Length() == 21);

    // Iteration
    for(auto& Range: RSI)
    {
        std::cout << Range << std::endl;
    }

}

BOOST_AUTO_TEST_CASE(SetOperationsTest)
{
    std::cout << "Testing different operations on RangeSet" << std::endl;

    Util::RangeSet<double> RSD{
        Util::RangeD{-1, 5.0}, Util::RangeD{-10,-4.2}};
    BOOST_CHECK(RSD.size() == 2);
    BOOST_CHECK(RSD.Min() == -10.0);
    BOOST_CHECK(RSD.Max() == 5.0);
    BOOST_CHECK(RSD.Length() == 11.8);

    Util::RangeD NotOverlappingRange{-2.0, -3.1};
    Util::RangeD OverlappingRange{0.0, 0.0};

    BOOST_CHECK(RSD.Overlap(OverlappingRange));
    BOOST_CHECK(!RSD.Overlap(NotOverlappingRange));


    Util::RangeSetD EmptySet{};
    Util::RangeSetD NotOverlappingSet{Util::RangeD{-15, -13.0},
                                        Util::RangeD{-2.0, -1.5}};
    Util::RangeSetD OverlappingSet{Util::RangeD{-15, -13.0},
                                     Util::RangeD{-2.0, 1.5}};

    BOOST_CHECK(!RSD.Overlap(EmptySet));
    BOOST_CHECK(!RSD.Overlap(NotOverlappingSet));
    BOOST_CHECK(RSD.Overlap(OverlappingSet));



    RSD |= NotOverlappingRange;
    BOOST_CHECK(RSD.size() == 3);
    BOOST_CHECK(RSD.Overlap(NotOverlappingRange));

    RSD |= NotOverlappingSet;
    BOOST_CHECK(RSD.size() == 4);
    BOOST_CHECK(RSD.Overlap(NotOverlappingSet));

    auto IntersectingSet = OverlappingSet && NotOverlappingSet;
    BOOST_CHECK(IntersectingSet == NotOverlappingSet);

    std::cout << "Intersecting Set:\n" << IntersectingSet << std::endl;
    std::cout << "Not Overlapping Set:\n" << NotOverlappingSet << std::endl;
    std::cout << "Final Range set:\n" << RSD << std::endl;

    std::cout << "Value at 75% of the range " << Util::ProportionalExtract(RSD, 0.75) << std::endl;

}

BOOST_AUTO_TEST_CASE(ConfTreeTest)
{
    std::cout << "Testing the input and output from a configuration tree" << std::endl;

    Util::RangeSet<double> RSD{
        Util::RangeD{-1, 5.3}, Util::RangeD{-10.0,-4.2}};
    BOOST_CHECK(RSD.GetParameterList().size() == 1);

    auto Tree = RSD.PopulateTree();
    std::cout << Tree << std::endl;
    BOOST_CHECK(Tree.size() == 2);

    Util::RangeSet<double> CopyRS{Util::RangeD{33, 3.2}};
    std::cout << "Single range tree output\n" << CopyRS.PopulateTree() << std::endl;

    CopyRS.Configure(Tree);
    BOOST_CHECK(RSD == CopyRS);





}

