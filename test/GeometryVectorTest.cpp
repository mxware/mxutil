/*
 * GeometryVectorTest.cpp
 *
 *  Created on: Jan 29, 2018
 *      Author: caiazza
 */


#define BOOST_TEST_MODULE GeometryVectorTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

typedef MXGeo::CartesianPrimitives<double>                    Primitives;
typedef Primitives::Vector_2                                  CartesianVector_R2D;
typedef Primitives::Vector_3                                  CartesianVector_R3D;

BOOST_AUTO_TEST_CASE(VectorConstruction)
{
    typedef Primitives::CoordinateIndex CI;

    std::cout << "testing the construction and access of the point primitive" << std::endl;
    auto x = 12.2;
    auto y = 156.1;
    auto z = 44.3;
    auto Vector1D = mt::GeneralClassConstructionTest<
        MXGeo::Vector<Primitives, 1>>(x);
    std::cout << "Point 1D:\tx(" << Vector1D.get<0>() << ")" << std::endl;
    BOOST_CHECK(Vector1D.get<0>() == x);
    BOOST_CHECK(!MXGeo::Is_Empty(Vector1D));
    BOOST_CHECK(MXGeo::Is_Simple(Vector1D));
    BOOST_CHECK(MXGeo::Is_Valid(Vector1D));

    auto Point2D = mt::GeneralClassConstructionTest<CartesianVector_R2D>(x, y);
    std::cout << "Point2D:\tx(" << Point2D.get<0>() << ")\ty(" << Point2D.get<1>() << ")" << std::endl;
    BOOST_CHECK(Point2D.get<0>() == x);
    BOOST_CHECK(Point2D.get<1>() == y);

    auto Point3D = mt::GeneralClassConstructionTest<CartesianVector_R3D>(x, y, z);
    std::cout << "Point3D:\tx(" << Point3D.get<0>() <<
        ")\ty(" << Point3D.get<1>() <<
        ")\tz(" << Point3D.get<2>() << ")" << std::endl;
    BOOST_CHECK(Point3D.get<0>() == x);
    BOOST_CHECK(Point3D.get<1>() == y);
    BOOST_CHECK(Point3D.get<2>() == z);
    BOOST_CHECK(Point3D.get<CI::X>() == x);
    BOOST_CHECK(Point3D.get<CI::Y>() == y);
    BOOST_CHECK(Point3D.get<CI::Z>() == z);

    std::array<double, 3> Coord{{x, y, z}};
    auto Point3D_A = mt::GeneralClassConstructionTest<CartesianVector_R3D>(Coord);
    std::cout << "Point3D_A:\tx(" << Point3D_A.get<0>() <<
        ")\ty(" << Point3D_A.get<1>() <<
        ")\tz(" << Point3D_A.get<2>() << ")" << std::endl;
    BOOST_CHECK(Point3D_A.get<0>() == x);
    BOOST_CHECK(Point3D_A.get<1>() == y);
    BOOST_CHECK(Point3D_A.get<2>() == z);

}

BOOST_AUTO_TEST_CASE(BaseAlgorithmsTest)
{
    std::cout << "Testing the area algorithm on the point primitive" << std::endl;
    CartesianVector_R3D Vector1{1,1,1};
    auto area = MXGeo::Area(Vector1);
    BOOST_CHECK(area == 0);
    std::cout << "Point 3D area: " << area << std::endl;
    std::cout << "Return type: " << typeid(area).name() << std::endl;

    CartesianVector_R2D Vector2{1, 2};
    area = MXGeo::Area(Vector2);
    BOOST_CHECK(area == 0);
    std::cout << "Point 2D area: " << area << std::endl;

}

BOOST_AUTO_TEST_CASE(VectorTransformationTest)
{
    std::cout << "Testing the vector transformation functions" << std::endl;
    CartesianVector_R3D V1{1,1,1};
    CartesianVector_R3D V2{};
    CartesianVector_R3D V3{};

//    bgt::translate_transformer<double, 3, 3> PointTranslate{2, -1, 0};
//    PointTranslate.apply(Point1, Point2);
//    std::cout << Point1 << "\t->\t" << Point2 << std::endl;
//
//    MXGeo::Transform(Point1, Point3, PointTranslate);
//    std::cout << Point1 << "\t->\t" << Point3 << std::endl;

    MXGeo::Rotation<Primitives, 3> R1{Eigen::AngleAxis<double>{3.1415, MXGeo::Vector<Primitives, 3>{0,0,1}}};
    MXGeo::Transform(V1, V2, R1);
    std::cout << V1 << "\t->\t" << V2 << std::endl;
}


BOOST_AUTO_TEST_CASE(VectorAlgebraTest)
{
    CartesianVector_R3D V1{1,1,1};
    CartesianVector_R3D V2{1,1,1};

    auto V3 = V1 + V2;
    std::cout << "Type of V3: " << typeid(V3).name() << std::endl;
    std::cout << "V3: " << V3 << std::endl;
    BOOST_CHECK(V3 == CartesianVector_R3D(2, 2, 2));
    BOOST_CHECK(V3 - V1 == V2);

    auto V4 = V1 * 2;
    BOOST_CHECK((std::is_same<decltype(V4), CartesianVector_R3D>::value));
    std::cout << "Forward scalar product: " << V4 << std::endl;

    auto V5 = 2 * V2;
    BOOST_CHECK((std::is_same<decltype(V5), CartesianVector_R3D>::value));
    std::cout << "Reverse scalar product: " << V5 << std::endl;
    BOOST_CHECK(V4 == V5);



}


