/*
 * GeometrySegmentTest.cpp
 *
 *  Created on: Jan 30, 2018
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE GeometrySegmentTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <iostream>
using std::cout;
using std::endl;

#include "MXWare/Test/CoreInterfaceTestHelper.hh"
namespace mt = MXWare::Test;

#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"
namespace MXGeo = MXWare::Geometry;

typedef MXGeo::CartesianPrimitives<double>              Primitives;
typedef Primitives::Segment_2                           Segment_2;
typedef Primitives::Segment_3                           Segment_3;
typedef Segment_2::Point                                Point_2;
typedef Segment_3::Point                                Point_3;

BOOST_AUTO_TEST_CASE(SegmentConstruction)
{
    std::cout << "Testing the construction and access of the segment primitive" << std::endl;

    auto S_2d = mt::GeneralClassConstructionTest<Segment_2>();
    BOOST_CHECK(!MXGeo::Is_Empty(S_2d));
    BOOST_CHECK(MXGeo::Is_Simple(S_2d));
    BOOST_CHECK(!MXGeo::Is_Valid(S_2d));

    auto S_3d = mt::GeneralClassConstructionTest<Segment_3>();
    BOOST_CHECK(!MXGeo::Is_Empty(S_3d));
    BOOST_CHECK(MXGeo::Is_Simple(S_3d));
    BOOST_CHECK(!MXGeo::Is_Valid(S_3d));

    Point_2 p1_2d{11.5, 55.0};
    Point_2 p2_2d{11.5, 47.0};
    auto FullSegment_2 = mt::GeneralClassConstructionTest<Segment_2>(p1_2d, p2_2d);
    std::cout << "Segment 2D:\t" << FullSegment_2.source() << "\t" << FullSegment_2.target() << std::endl;
    BOOST_CHECK(p1_2d == FullSegment_2.source());
    BOOST_CHECK(p2_2d == FullSegment_2.target());

    Point_3 p1_3d{22.1, 55.7, 0.22};
    Point_3 p2_3d{22.1, 31.5, 1.99};
    auto FullSegment_3 = mt::GeneralClassConstructionTest<Segment_3>(p1_3d, p2_3d);
    std::cout << "Segment 3D:\t" << FullSegment_3.source() << "\t" << FullSegment_3.target() << std::endl;
    BOOST_CHECK(p1_3d == FullSegment_3.source());
    BOOST_CHECK(p2_3d == FullSegment_3.target());

}


BOOST_AUTO_TEST_CASE(BaseAlgorithmsTest)
{
    std::cout << "Testing the area algorithm on the segment primitive" << std::endl;

    Point_3 p1_3d{22.1, 55.7, 0.22};
    Point_3 p2_3d{22.1, 31.5, 1.99};
    Segment_3 S{p1_3d, p2_3d};
    auto area = MXGeo::Area(S);
    BOOST_CHECK(area == 0);
    std::cout << "Segment 3D area: " << area << std::endl;
    std::cout << "Return type: " << typeid(area).name() << std::endl;



}

BOOST_AUTO_TEST_CASE(SegmentTransformationtest)
{
    std::cout << "Testing the segment spatial transformation" << std::endl;
    Point_3 p1_3d{22, 55.7, 0.22};
    Point_3 p2_3d{22, 31.5, 1.99};
    Segment_3 S{p1_3d, p2_3d};
    Segment_3 S_T;
    Segment_3 S_I;

    MXGeo::Translation<Primitives, 3> Translate{4, 2, -1};
    MXGeo::Translation<Primitives, 3> InverseTranslate{Translate.inverse()};
    std::cout << "Translation vector:\n" << Translate.vector() << std::endl;
    std::cout << "Original segment:\t" << S << std::endl;
    MXGeo::Transform(S, S_T, Translate);
    std::cout << "Translated segment:\t" << S_T << std::endl;
    std::cout << "Inverse Translation:\n" << InverseTranslate.vector() << std::endl;
    MXGeo::Transform(S_T, S_I, InverseTranslate);
    std::cout << "Translation inverted segment:\t" << S_I << std::endl;
    //BOOST_CHECK(S == S_I);

    std::cout << "Segment rotation along the z axis" << std::endl;
    MXGeo::Rotation<Primitives, 3> R1{MXGeo::AxialRotation_3D<Primitives>{0.15, MXGeo::Vector<Primitives, 3>{0,0,1}}};
    MXGeo::Transform(S, S_T, R1);
    std::cout << S << "\t->\t" << S_T << std::endl;

}


