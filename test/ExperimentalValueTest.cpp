/*
 * ExperimentalValueTest.cpp
 *
 *  Created on: Mar 2, 2017
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE ExperimentalValueTest test

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include <MXWare/Core/MXConfTree.hh>

#include <iostream>

#include "MXWare/Util/ExperimentalValue_T.hh"
#include "MXWare/Util/MXUtil.hh"

BOOST_AUTO_TEST_CASE(ConstructionTest)
{
    std::cout << "Testing the constructors" << std::endl;
    MXWare::Util::ExperimentalValue LabVal{};
    BOOST_CHECK(LabVal.GetValue() == 0.0);
    BOOST_CHECK(LabVal.GetError() == 0.0);
    BOOST_CHECK(LabVal.HasSymmetricErrors());

    MXWare::Util::ExperimentalValue LabValSym(1.0, 2.0);
    BOOST_CHECK(LabValSym.GetValue() == 1.0);
    BOOST_CHECK(LabValSym.GetPositiveError() == 2.0);
    BOOST_CHECK(LabValSym.GetNegativeError() == 2.0);
    BOOST_CHECK(LabValSym.HasSymmetricErrors());

    MXWare::Util::ExperimentalValue LabValAsym(1.0, 2.0, 3.0);
    BOOST_CHECK(LabValAsym.GetValue() == 1.0);
    BOOST_CHECK(LabValAsym.GetPositiveError() == 3.0);
    BOOST_CHECK(LabValAsym.GetNegativeError() == 2.0);
    BOOST_CHECK(!LabValAsym.HasSymmetricErrors());


}

BOOST_AUTO_TEST_CASE(StreamingTest)
{
    MXWare::Util::ExperimentalValue LabValSym{1.11, 85.05};
    MXWare::Util::ExperimentalValue LabValAsym{114.42, 1.11, 22.12};

    std::cout << "Streaming the input and output stream" << std::endl;

    std::stringstream SymStream;
    SymStream << LabValSym;
    std::cout << "Symmetrical errors: " << SymStream.str() << std::endl;

    MXWare::Util::ExperimentalValue LabValSymRefill{2244, 45454};
    SymStream.str("1.11[85.05]");
    SymStream >> LabValSymRefill;
    std::cout << "Symmetrical errors refilled: " << LabValSymRefill << std::endl;
    BOOST_CHECK(LabValSym == LabValSymRefill);

    std::stringstream AsymStream;
    AsymStream << LabValAsym;
    std::cout << "Asymmetrical errors: " << AsymStream.str() << std::endl;

    AsymStream.str("114.42[1.11:22.12]");
    AsymStream >>LabValAsym;
    std::cout << "Asymmetrical errors refilled: " << LabValAsym << std::endl;

}


BOOST_AUTO_TEST_CASE(ConfTreeStreaming)
{
    std::cout << "Testing the property tree input and output" << std::endl;

    MXWare::Util::ExperimentalValue LabValSym{1.11, 85.05};
    MXWare::Util::ExperimentalValue LabValAsym{114.42, 1.11, 22.12};
    MXWare::Core::MXConfTree Tree;

    Tree.put("ExpSym", LabValSym);
    Tree.put("ExpAsym", LabValAsym);

    BOOST_CHECK(!Tree.empty());

    MXWare::Util::ExperimentalValue LabValSymRead = Tree.get<MXWare::Util::ExperimentalValue>("ExpSym");
    std::cout << "Value read from the tree: " << LabValSymRead << std::endl;
    BOOST_CHECK(LabValSym == LabValSymRead);

    MXWare::Util::ExperimentalValue LabValAsymRead = Tree.get<MXWare::Util::ExperimentalValue>("ExpAsym");
    std::cout << "Value read from the tree: " << LabValAsymRead << std::endl;
    BOOST_CHECK(LabValAsym == LabValAsymRead);



}

