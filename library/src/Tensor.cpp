/*
 * Tensor.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: caiazza
 */

#include "MXWare/Util/Tensor.hh"

#include "MXWare/Core/MXExceptions.hh"

#include "MXWare/Util/Spinor.hh"

static const  Complex i(0,1);

namespace MXWare
{
namespace Util
{

/* ******************************************************
 * Row operations
 ******************************************************** */

/// Increment
Tensor::Row&
operator+=(Tensor::Row&L, const Tensor::Row& R)
{
    L[0] += R[0];
    L[1] += R[1];
    L[2] += R[2];
    L[3] += R[3];
    return L;
}

/// Addition
Tensor::Row
operator+(Tensor::Row L, const Tensor::Row& R)
{
    return L+=R;
}

/// Decrement
Tensor::Row&
operator-=(Tensor::Row&L, const Tensor::Row& R)
{
    L[0] -= R[0];
    L[1] -= R[1];
    L[2] -= R[2];
    L[3] -= R[3];
    return L;
}

/// Subtraction
Tensor::Row
operator-(Tensor::Row L, const Tensor::Row& R)
{
    return L-=R;
}

/// Constant self-multiplication
Tensor::Row&
operator*=(Tensor::Row&L, const Complex& T)
{
    L[0] *= T;
    L[1] *= T;
    L[2] *= T;
    L[3] *= T;
    return L;
}

/// Constant multiplication
Tensor::Row
operator*(Tensor::Row L, const Complex& T)
{
    return L *= T;
}

Tensor::Row
operator*(const Complex& T, Tensor::Row R)
{
    return R *= T;
}

/// Constant self-multiplication
Tensor::Row&
operator/=(Tensor::Row&L, const Complex& T)
{
    L[0] /= T;
    L[1] /= T;
    L[2] /= T;
    L[3] /= T;
    return L;
}

/// Constant multiplication
Tensor::Row
operator/(Tensor::Row L, const Complex& T)
{
    return L /= T;
}

/// Row spinor multiplication
Complex
operator*(const Tensor::Row& R, const Spinor& S)
{
    return R[0] * S[0] + R[1]*S[1] + R[2]*S[2] + R[3]*S[3];

}

/// Row Tensor multiplication
Tensor::Row&
operator*=(Tensor::Row& R, const Tensor& T)
{
    Complex R0 = R[0] * T[0][0] + R[1]*T[1][0] + R[2]*T[2][0] + R[3]*T[3][0];
    Complex R1 = R[0] * T[0][1] + R[1]*T[1][1] + R[2]*T[2][1] + R[3]*T[3][1];
    Complex R2 = R[0] * T[0][2] + R[1]*T[1][2] + R[2]*T[2][2] + R[3]*T[3][2];
    Complex R3 = R[0] * T[0][3] + R[1]*T[1][3] + R[2]*T[2][3] + R[3]*T[3][3];
    R = Tensor::Row{{R0, R1, R2, R3}};
    return R;
}

Tensor::Row
operator*(Tensor::Row R, const Tensor& T)
{
    return R *= T;
}



/// Equality
bool
operator==(const Tensor::Row& L, const Tensor::Row& R)
{
    return (L[0] == R[0] &&
            L[1] == R[1] &&
            L[2] == R[2] &&
            L[3] == R[3]);
}

/// Inequality
bool
operator!=(const Tensor::Row& L, const Tensor::Row&R)
{
    return!(L==R);
}

/// Output to stream
std::ostream&
operator<<(std::ostream & os, const Tensor::Row & R)
{
    return os << "(" << R[0] << "," << R[1] << "," << R[2] << "," << R[3] << ")";
}

/* **************************************************************
 * Tensor member definitions
 ****************************************************************/

Tensor::Tensor(const Complex& n00, const Complex& n01,
               const Complex& n02, const Complex& n03,
               const Complex& n10, const Complex& n11,
               const Complex& n12, const Complex& n13,
               const Complex& n20, const Complex& n21,
               const Complex& n22, const Complex& n23,
               const Complex& n30, const Complex& n31,
               const Complex& n32, const Complex& n33):
                   m_Comp{{ Row{{n00, n01, n02, n03}},
                            Row{{n10, n11, n12, n13}},
                            Row{{n20, n21, n22, n23}},
                            Row{{n30, n31, n32, n33}} }}
{

}

Tensor::Tensor(Row&& R0, Row&& R1,
               Row&& R2, Row&& R3):
                   m_Comp{{R0, R1, R2, R3 }}
{

}

Tensor::Tensor(const Row& R0, const Row& R1,
               const Row& R2, const Row& R3):
                   m_Comp{{ R0, R1, R2, R3 }}
{

}

Tensor::Tensor(const LorentzVector& P):
    Tensor{P.e()        , 0             , -P.z()            , i*P.y() - P.x()   ,
           0            , P.e()         , -i*P.y() -P.x()   , P.z()             ,
           P.z()        , -i*P.y()+P.x(), -P.e()            , 0                 ,
           i*P.y()+P.x(), -P.z()        , 0                 ,   -P.e()          }
{

}

const Tensor
Tensor::Null()
{
    return Tensor{};
}

const Tensor
Tensor::Id()
{
    return Tensor{ Row{{1, 0, 0, 0}},
                   Row{{0, 1, 0, 0}},
                   Row{{0, 0, 1, 0}},
                   Row{{0, 0, 0, 1}} };
}

const Tensor
Tensor::G_mn()
{
    return Tensor{ Row{{1, 0, 0, 0}},
                   Row{{0, -1, 0, 0}},
                   Row{{0, 0, -1, 0}},
                   Row{{0, 0, 0, -1}} };
}

const Tensor
Tensor::Gamma(size_t id)
{
    switch(id)
    {
        case 0:
        {
            return Tensor{ Row{{1, 0, 0, 0}},
                           Row{{0, 1, 0, 0}},
                           Row{{0, 0,-1, 0}},
                           Row{{0, 0, 0,-1}} };
        }
        case 1:
        {
            return Tensor{ Row{{0, 0, 0, 1}},
                           Row{{0, 0, 1, 0}},
                           Row{{0,-1, 0, 0}},
                           Row{{-1, 0, 0, 0}} };
        }
        case 2:
        {
            return Tensor{ Row{{0, 0, 0,-i}},
                           Row{{0, 0, i, 0}},
                           Row{{0, i, 0, 0}},
                           Row{{-i, 0, 0, 0}} };
        }
        case 3:
        {
            return Tensor{ Row{{0, 0, 1, 0}},
                           Row{{0, 0, 0, -1}},
                           Row{{-1, 0, 0, 0}},
                           Row{{0, 1, 0, 0}} };
        }
        case 4:
        {
            return Tensor{ Row{{0, 0, 1, 0}},
                           Row{{0, 0, 0, 1}},
                           Row{{1, 0, 0, 0}},
                           Row{{0, 1, 0, 0}} };
        }
        default:
        {
            throw Core::OutOfRange{"Tensor", "Gamma", "Gamma matrix required with index not in the range 0-4"};
        }
    }

}

/*
const std::array< std::array<const Tensor, 4>, 4>
Tensor::SigmaSpin()
{
    auto& Gamma =  Tensor::Gamma;
    std::array<const Tensor, 4> R0{{
                Tensor::Null(), i*Gamma(0)*Gamma(1), i*Gamma(0)*Gamma(2), i*Gamma(0)*Gamma(3)
    }};

    std::array<const Tensor, 4> R1{{
                    i*Gamma(1)*Gamma(0), Tensor::Null(), i*Gamma(1)*Gamma(2), i*Gamma(1)*Gamma(3)
    }};

    std::array<const Tensor, 4> R2{{
                    i*Gamma(2)*Gamma(0), i*Gamma(2)*Gamma(1), Tensor::Null(), i*Gamma(2)*Gamma(3)
    }};

    std::array<const Tensor, 4> R3{{
                    i*Gamma(3)*Gamma(0), i*Gamma(3)*Gamma(1), i*Gamma(3)*Gamma(2), Tensor::Null()
    }};

    return std::array<std::array<const Tensor, 4>, 4>{ {std::move(R0),
                                                        std::move(R1),
                                                        std::move(R2),
                                                        std::move(R3) } };
}
*/

Tensor&
Tensor::operator+=(const Tensor& T)
{
    auto itThis = m_Comp.begin();
    auto itOther = T.m_Comp.cbegin();

    for(size_t i = 0; i < m_Comp.size() ; ++i, ++itThis, ++itOther)
    {
        Row& Left = *itThis;
        const Row& Right = *itOther;

        Left += Right;
    }
    return *this;
}

Tensor&
Tensor::operator-=(const Tensor& T)
{
    auto itThis = m_Comp.begin();
    auto itOther = T.m_Comp.cbegin();

    for(size_t i = 0; i < m_Comp.size() ; ++i, ++itThis, ++itOther)
    {
        Row& Left = *itThis;
        const Row& Right = *itOther;

        Left -= Right;
    }
    return *this;
}

Tensor&
Tensor::operator*=(const Tensor& T)
{
    auto R0 = m_Comp[0]*T;
    auto R1 = m_Comp[1]*T;
    auto R2 = m_Comp[2]*T;
    auto R3 = m_Comp[3]*T;
    *this = Tensor{R0, R1, R2, R3};
    return *this;
}

Tensor&
Tensor::operator*=(const Complex& K)
{
    for(auto& theRow : m_Comp)
    {
        theRow *= K;
    }
    return *this;
}

Tensor&
Tensor::operator/=(const Complex& K)
{
    for(auto& theRow : m_Comp)
    {
        theRow /= K;
    }
    return *this;
}

Spinor
operator*(const Tensor& T, const Spinor& S)
{
    Spinor ans;
    for(auto i = 0; i < 4; ++i)
    {
        ans[i] = T[i] * S;
    }
    return ans;
}


std::ostream&
operator<<(std::ostream & os, const Tensor & T)
{
    return os << "| " << T[0] << " |\n| " << T[1] << " |\n| " << T[2] << " |\n| " << T[3] << " |";
}

}  // namespace Util

}  // namespace MXWare

