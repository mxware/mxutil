/*
 * MXParticleDataTable.cpp
 *
 *  Created on: May 20, 2015
 *      Author: caiazza
 */

#include "MXWare/Util/MXParticleDataTable.hh"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>

#include "MXWare/Core/MXExceptions.hh"
#include "MXWare/Core/MXCore.hh"

#include "MXWare/Util/SystemOfUnits.hh"
using MXWare::Units::GeV;

#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;
#include <boost/version.hpp>

namespace MXWare
{
namespace Util
{

const std::string MXParticleDataTable::s_NN_PData{"pdata"};

MXParticleDataTable::MXParticleDataTable():
    m_Data{ }
{

}
MXParticleDataTable::MXParticleDataTable(std::initializer_list<MXParticleData> PDataList):
    m_Data{PDataList}
{

}

bool
MXParticleDataTable::empty ( ) const
{
    return m_Data.empty();
}

size_t
MXParticleDataTable::size() const
{
    return m_Data.size();
}

MXParticleDataTable::iterator
MXParticleDataTable::begin ( )
{
    return m_Data.begin();
}

MXParticleDataTable::const_iterator
MXParticleDataTable::begin ( ) const
{
    return m_Data.begin();
}

MXParticleDataTable::const_iterator
MXParticleDataTable::cbegin ( ) const
{
    return m_Data.cbegin();
}

MXParticleDataTable::iterator
MXParticleDataTable::end ( )
{
    return m_Data.end();
}

MXParticleDataTable::const_iterator
MXParticleDataTable::end ( ) const
{
    return m_Data.end();
}

MXParticleDataTable::const_iterator
MXParticleDataTable::cend ( ) const
{
    return m_Data.cend();
}

MXParticleDataTable::reverse_iterator
MXParticleDataTable::rbegin ( )
{
    return m_Data.rbegin();
}

MXParticleDataTable::const_reverse_iterator
MXParticleDataTable::rbegin ( ) const
{
    return m_Data.rbegin();
}

MXParticleDataTable::const_reverse_iterator
MXParticleDataTable::crbegin ( ) const
{
    return m_Data.crbegin();
}

MXParticleDataTable::reverse_iterator
MXParticleDataTable::rend ( )
{
    return m_Data.rend();
}

MXParticleDataTable::const_reverse_iterator
MXParticleDataTable::rend ( ) const
{
    return m_Data.rend();
}

MXParticleDataTable::const_reverse_iterator
MXParticleDataTable::crend ( ) const
{
    return m_Data.crend();
}

std::pair<MXParticleDataTable::iterator, bool>
MXParticleDataTable::insert (const value_type& PData)
{
    return m_Data.insert(PData);
}

std::pair<MXParticleDataTable::iterator, bool>
MXParticleDataTable::insert (value_type&& PData)
{
    return m_Data.insert(std::move(PData));
}

MXParticleDataTable::iterator
MXParticleDataTable::insert (const_iterator position, const value_type& PData)
{
    return m_Data.insert(position, PData);
}

MXParticleDataTable::iterator
MXParticleDataTable::insert (const_iterator position, value_type&& PData)
{
    return m_Data.insert(position, std::move(PData));
}

void
MXParticleDataTable::insert (std::initializer_list<value_type> PDataList)
{
    m_Data.insert(PDataList);
}

MXParticleDataTable::iterator
MXParticleDataTable::erase (const_iterator position)
{
    return m_Data.erase(position);
}

MXParticleDataTable::size_type
MXParticleDataTable::erase (const value_type& PData)
{
    return m_Data.erase(PData);
}

MXParticleDataTable::iterator
MXParticleDataTable::erase (const_iterator first, const_iterator last)
{
    return m_Data.erase(first, last);
}

void
MXParticleDataTable::swap(MXParticleDataTable& other)
{
    m_Data.swap(other.m_Data);
}

void
MXParticleDataTable::clear() noexcept
{
    m_Data.clear();
}

MXParticleDataTable::const_iterator
MXParticleDataTable::find (const value_type& PData) const
{
    return m_Data.find(PData);
}

MXParticleDataTable::const_iterator
MXParticleDataTable::find (const MXParticleID& Id) const
{
    return std::find_if(m_Data.begin(), m_Data.end(),[&](const value_type& PData)
                        {
        return PData.PID() == Id;
                        });
}

MXParticleDataTable::iterator
MXParticleDataTable::find (const value_type& PData)
{
    return m_Data.find(PData);
}

MXParticleDataTable::iterator
MXParticleDataTable::find (const MXParticleID& Id)
{
    return std::find_if(m_Data.begin(), m_Data.end(),[&](const value_type& PData)
                        {
        return PData.PID() == Id;
                        });
}

MXParticleDataTable::size_type
MXParticleDataTable::count(const value_type& PData)
{
    return m_Data.count(PData);
}

MXParticleDataTable::const_iterator
MXParticleDataTable::lower_bound (const value_type& PData) const
{
    return m_Data.lower_bound(PData);
}

MXParticleDataTable::const_iterator
MXParticleDataTable::lower_bound (const MXParticleID& Id) const
{
    return std::lower_bound(m_Data.begin(),
                            m_Data.end(),
                            Id,
                            [](const value_type& PData, const MXParticleID& x)
                        {
        return PData.PID() < x;
                        });
}

MXParticleDataTable::iterator
MXParticleDataTable::lower_bound (const value_type& PData)
{
    return m_Data.lower_bound(PData);
}

MXParticleDataTable::iterator
MXParticleDataTable::lower_bound (const MXParticleID& Id)
{
    return std::lower_bound(m_Data.begin(),
                            m_Data.end(),
                            Id,
                            [](const value_type& PData, const MXParticleID& x)
                        {
        return PData.PID() < x;
                        });
}

MXParticleDataTable::const_iterator
MXParticleDataTable::upper_bound (const value_type& PData) const
{
    return m_Data.upper_bound(PData);
}

MXParticleDataTable::const_iterator
MXParticleDataTable::upper_bound (const MXParticleID& Id) const
{
    return std::upper_bound(m_Data.begin(),
                            m_Data.end(),
                            Id,
                            [](const MXParticleID& x, const value_type& PData)
                        {
        return x < PData.PID();
                        });
}

MXParticleDataTable::iterator
MXParticleDataTable::upper_bound (const value_type& PData)
{
    return m_Data.upper_bound(PData);
}

MXParticleDataTable::iterator
MXParticleDataTable::upper_bound (const MXParticleID& Id)
{
    return std::upper_bound(m_Data.begin(),
                            m_Data.end(),
                            Id,
                            [](const MXParticleID& x, const value_type& PData)
                        {
        return x < PData.PID();
                        });
}

void
MXParticleDataTable::Configure (const Core::MXConfTree& ConfTree)
{
    m_Data.clear();
    ParseConfigurationTree(ConfTree);
}

Core::MXConfTree
MXParticleDataTable::PopulateTree ( ) const
{
    Core::MXConfTree ans;

    for(auto& P: m_Data)
    {
        ans.add_child(s_NN_PData, P.PopulateTree());
    }
    return ans;
}

Core::ParameterList
MXParticleDataTable::GetParameterList ( ) const
{
    Core::ParameterList ans;
    ans.AddParameter<MXParticleData>(s_NN_PData, "Partcile data information");
    return ans;
}

void
MXParticleDataTable::LoadData(const bfs::path& Filename,
                              SupportedFormats Format,
                              bool Overwrite)
{
    if(!bfs::is_regular_file(Filename))
    {
        std::stringstream msg;
        msg << "The input path to load the particle data: " << Filename.native() << " is not valid";
        Core::FileNotFound e {msg.str()};
        e.raise();
    }

    std::ifstream pdfile(Filename.native());
    if( !pdfile )
    {
        Core::FileError e{"MXParticleDataTable",
            "LoadData",
            std::string{"Unable to open the file "} + Filename.native() };
        e.raise();
    }

    switch(Format)
    {
        case SupportedFormats::PDG2014:
        case SupportedFormats::AUTO:
        {
            ParsePDG2014(pdfile, Overwrite);
            break;
        }
        case SupportedFormats::XML:
        {
            Core::MXConfTree Tree;
            pt::read_xml(Filename.native(), Tree, pt::xml_parser::trim_whitespace);
            ParseConfigurationTree(Tree, Overwrite);
        }
    }
}

void
MXParticleDataTable::SaveData(const bfs::path& Filename)
{
    auto Tree = PopulateTree();

#if BOOST_VERSION >= 105600
    pt::xml_writer_settings<std::string> settings('\t', 1);
#else
    pt::xml_writer_settings<char> settings('\t', 1);
#endif

    pt::write_xml(Filename.native(), Tree, std::locale(), settings);

}

const MXParticleData&
MXParticleDataTable::GetData(const MXParticleID& Id) const
{
    auto Particle = find(Id);
    if(Particle == m_Data.end())
    {
        std::stringstream msg;
        msg << "Particle with Id " << Id;
        throw Core::ObjectNotFound("MXParticleDataTable",
                                    "GetData",
                                    msg.str());
    }

    return *Particle;
}

void
MXParticleDataTable::ParsePDG2014(std::ifstream& File, bool Overwrite)
{
    for(std::string line;std::getline(File, line); )
    {
        // Ignoring the lines where the first character is a *
        if(line.compare(line.find_first_not_of(" "), 1, "*"))
        {
            //std::cout << line << std::endl;

            const size_t Lim1 = 32;
            const size_t Lim2 = 70;
            const size_t Lim3 = 107;
            // In the PDG14 format the first 32 char are for the ids of up
            // to 4 states with the same quantum numbers but the electric charge
            auto IdS = line.substr(0, Lim1);
            IdS = IdS.substr(line.find_first_not_of(" ") );
            // Removing trailing spaces
            IdS = IdS.substr(0, IdS.find_last_not_of(" ")+1);

            std::stringstream IdMore{IdS};
            std::vector<std::string> Tokens;
            while(IdMore)
            {
                std::string tmp;
                IdMore >> tmp;
                if(IdMore)
                    Tokens.push_back(tmp);
            }

            std::stringstream DataS{line.substr(Lim1, Lim2-Lim1)};
            // The rest of the string contains the physical data
            double Mass;
            double MassErrPlus;
            double MassErrMin;
            DataS>> Mass >> MassErrPlus >> MassErrMin;

            std::string Width_S = line.substr(Lim2, Lim3-Lim2);
            double Width;
            double WidthErrPlus;
            double WidthErrMin;

            if(Width_S.find_first_not_of(" ") != std::string::npos)
            {
                std::stringstream DataW{Width_S};
                DataW >> Width >> WidthErrPlus >> WidthErrMin;

            }
            else
            {
                Width = 0.0;
                WidthErrPlus = 0.0;
                WidthErrMin = 0.0;
            }

            std::stringstream DataC{line.substr(Lim3)};
            std::string Name;
            std::string Charges;

            // Transfer the tokens from the stream to the data elements
            DataC >> Name >> Charges;

            // Parsing the charge list
            std::vector<std::string> ChargeList;
            auto ChDelim = ',';
            size_t ChStart = Charges.find_first_not_of(ChDelim);
            size_t ChEnd = ChStart;
            while(ChStart != std::string::npos)
            {
                ChEnd = Charges.find(ChDelim, ChStart);
                ChargeList.push_back(Charges.substr(ChStart, ChEnd - ChStart));
                ChStart = Charges.find_first_not_of(ChDelim, ChEnd);
            }

            // Creating the Particle data
            auto ChargeIt = ChargeList.begin();
            for(auto tok : Tokens)
            {
                //Creating the particle and filling the physical data
                MXParticleData tmp{};

                std::stringstream PID_s{tok};
                MXParticleID PID_i;
                PID_s >> PID_i;
                tmp.SetPID(MXParticleID{PID_i});

                tmp.SetMass(ExperimentalValue{Mass * GeV, MassErrMin * GeV, MassErrPlus * GeV});
                tmp.SetWidth(ExperimentalValue{Width * GeV, WidthErrMin * GeV, WidthErrPlus * GeV});

                /*
                 * Setting the particle name.
                 * For particles with one charge state the name just corresponds to
                 * name column, otherwise the charge state is appended.
                 * The antiparticle have the same name of the particle but with a tilde
                 */
                if(Tokens.size() > 1)
                {
                    tmp.SetName(Name + (*ChargeIt));
                    ++ChargeIt;
                }
                else
                {
                    tmp.SetName(Name);
                }

                auto Res = m_Data.insert(tmp);
                if(Overwrite && !Res.second)
                {
                    m_Data.erase(Res.first);
                    m_Data.insert(tmp);
                }


                // Creating the antiparticle with opposite PID
                if(PID_i.HasAntiparticle())
                {
                    MXParticleID AntiParticle_Id{-1*PID_i.Pid()};
                    MXParticleData AntiP{tmp};
                    AntiP.SetPID(AntiParticle_Id);
                    AntiP.SetName(tmp.Name() + "~");

                    Res = m_Data.insert(AntiP);
                    if(!Res.second)
                    {
                        m_Data.erase(Res.first);
                        m_Data.insert(AntiP);
                    }
                }
            }
        }
    }
}

void
MXParticleDataTable::ParseConfigurationTree(const Core::MXConfTree& Tree, bool Overwrite)
{
    for(auto& Node: Tree)
    {
        if(Node.first == s_NN_PData)
        {
            MXParticleData tmp;
            tmp.Configure(Node.second);

            auto Res = m_Data.insert(tmp);
            if(Overwrite && !Res.second)
            {
                m_Data.erase(Res.first);
                m_Data.insert(tmp);
            }
        }
    }
}


MXParticleDataTable&
MXParticleDataTable::GlobalPDataTable ( )
{
    static std::unique_ptr<MXParticleDataTable> Table;
    if(!Table)
    {
        Table = std::make_unique<MXParticleDataTable>();
    }
    return *Table;
}

void
MXParticleDataTable::LoadDefaultData( MXParticleDataTable& Table, bool Overwrite)
{
    MXParticleData tmp{MXParticleID{22},
                         "gamma",
                         ExperimentalValue{0.0, 0.0},
                         ExperimentalValue{0.0, 0.0}};
    auto res = Table.insert(tmp);
    if(!res.second && Overwrite)
    {
        Table.erase(res.first);
        Table.insert(tmp);
    }

    /* ************************************************************ */

    tmp = MXParticleData{MXParticleID{11},
                         "electron",
                         ExperimentalValue{Units::electron_mass_c2, 1.1e-08},
                         ExperimentalValue{0.0, 0.0}};
    res = Table.insert(tmp);
    if(!res.second && Overwrite)
    {
        Table.erase(res.first);
        Table.insert(tmp);
    }

    /* ************************************************************ */

    tmp = MXParticleData{MXParticleID{-11},
                         "positron",
                         ExperimentalValue{Units::electron_mass_c2, 1.1e-08},
                         ExperimentalValue{0.0, 0.0}};
    res = Table.insert(tmp);
    if(!res.second && Overwrite)
    {
        Table.erase(res.first);
        Table.insert(tmp);
    }

    /* ************************************************************ */

    tmp = MXParticleData{MXParticleID{2112},
                         "n",
                         ExperimentalValue{Units::neutron_mass_c2, 2.1e-05},
                         ExperimentalValue{0.0, 0.0}};
    res = Table.insert(tmp);
    if(!res.second && Overwrite)
    {
        Table.erase(res.first);
        Table.insert(tmp);
    }

    /* ************************************************************ */

    tmp = MXParticleData{MXParticleID{2212},
                         "p",
                         ExperimentalValue{Units::proton_mass_c2, 2.1e-05},
                         ExperimentalValue{7.477e-25, 1e-27}};
    res = Table.insert(tmp);
    if(!res.second && Overwrite)
    {
        Table.erase(res.first);
        Table.insert(tmp);
    }
}

}  // namespace Util

}  // namespace MXWare

