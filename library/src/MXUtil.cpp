/**
 * \file MXUtil.cpp
 *
 *  Created on: Nov 13, 2015
 *      Author: caiazza
 */

#include "MXWare/Util/MXUtil.hh"
using std::endl;

#include "MXWare/Util/SystemOfUnits.hh"
#include <iostream>

#include <string>

namespace MXWare
{
namespace Util
{

void
PrintPhysicalConstants(std::ostream& out)
{
    using namespace Units;
    out << "Avogadro number (Avogadro): " << Avogadro*mole << " particles per mole." << endl;
    out << "Light speed (c_light): " << c_light / (m / s) << " m/s."<< endl;
    out << "Plank Constant (h_Planck): " <<  h_Planck / (joule * s) << " J/s" << endl;
    out << "Reduced Planck constant (hbar_Planck): " << hbar_Planck / (joule * s) << " J * s" << endl;
    out << "                                       " << hbar_Planck / (MeV * s) << " MeV * s" << endl;
    out << "hbar * c (hbarc): " << hbarc / (joule * m) << " J * m" << endl;
    out << "                  " << hbarc / (MeV * cm) << " MeV * cm" << endl;
    out << "(hbar * c)^2 (hbarc_squared): " << hbarc_squared / (MeV * MeV * microbarn) << " MeV^2 * mubarn" << endl;
    out << "Electron charge (electron_charge): " << electron_charge / (coulomb) << " C" << endl;
    out << "Electron mass (electron_mass_c2): " << electron_mass_c2 / (MeV) << " MeV" << endl;
    out << "Proton mass (proton_mass_c2): " << proton_mass_c2 / (MeV) << " MeV" << endl;
    out << "Neutron mass (neutron_mass_c2): " << neutron_mass_c2/ (MeV) << " MeV" << endl;
    out << "Atomic mass unit (amu_c2): " << amu_c2 / MeV << " MeV" << endl;
    out << "SI Atomic mass unit (amu): " << amu / gram << " gram" << endl;
    out << "Magnetic permeability of free space (mu0): " << mu0 / (henry / m) << " H/m"<< endl;
    out << "Electric permittivity of free space (epsilon0): " << epsilon0 / (farad / m) << " F/m" << endl;
    out << "QED coupling (elm_coupling): " << elm_coupling << endl;
    out << "Fine structure constant (fine_structure_const): " << fine_structure_const << endl;
    out << "Classic electron radius (classic_electr_radius): " << classic_electr_radius / (fermi) << " fm" << endl;
    out << "Electron Compton length (electron_Compton_length): " << electron_Compton_length / fermi << " fm" << endl;
    out << "Bohr radius (Bohr_radius): " << Bohr_radius /fermi << " fm" << endl;
    out << "Boltzmann Constant(k_Boltzmann): " << k_Boltzmann / (joule / kelvin) << " J/k" << endl;
    out << "                    " << k_Boltzmann / (MeV/kelvin) << " MeV/k" << endl;
    out << "STP Temperature (STP_Temperature): " << STP_Temperature / (kelvin) << " k" << endl;
    out << "STP Pressure (STP_Pressure): " << STP_Pressure / (bar) << " bar" << endl;
    out << "Threshold gas density (kGasThreshold): " << kGasThreshold / (g/ m3) << " g/m^3" << endl;
    out << "University mean density (universe_mean_density): " << universe_mean_density / (g/cm3) << " g/cm^3" << endl;
    out << "Nuclear magneton (nuclear_magneton): " << nuclear_magneton / (joule / tesla) << " J/T" << endl;
    out << "Proton magnetic moment (proton_mu): " << proton_mu / (nuclear_magneton) << " mu_N" << endl;
    out << "                                    " << proton_mu / (joule / tesla) << " J/T" << endl;
    out << "Neutron magnetic moment (neutron_mu): " << neutron_mu / (nuclear_magneton) << " mu_N" << endl;
    out << "                                      " << neutron_mu / (joule / tesla) << " J/T" << endl;

    out << endl << "SI Units internal conversions" << endl;
    out << "Joule: " << joule /MeV << " MeV" << endl;
}





///Typedef to the implementation of MCParticle to use

/*
typedef MCParticle_Impl MCParticle;

std::unique_ptr<IMCParticle>
CreateMCParticle(const IMCParticle::PID& Id)
{
    return std::unique_ptr<IMCParticle>{new MCParticle{Id}};
}

std::unique_ptr<IMCParticle>
CreateMCParticle(const IMCParticle::PID& Id,
                               const ThreeVec& P,
                               const Vertex& V,
                               const ThreeVec& S)
{
    return std::unique_ptr<IMCParticle>{new MCParticle{Id, P, V, S}};
}
*/

}  // namespace Util

}  // namespace MXWare


