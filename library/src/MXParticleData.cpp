/*
 * MXParticleData.cpp
 *
 *  Created on: May 31, 2015
 *      Author: caiazza
 */

#include "MXWare/Util/MXParticleData.hh"

#include "MXWare/Core/ParameterAutoConfiguration.hh"
#include <sstream>
#include <iomanip>

namespace MXWare
{

namespace Util
{



const std::string MXParticleData::s_NN_PID{"pid"};
const std::string MXParticleData::s_NN_Label{"label"};
const std::string MXParticleData::s_NN_Mass{"mass"};
const std::string MXParticleData::s_NN_Width{"width"};

MXParticleData::MXParticleData():
    m_PID{0},
    m_Name{""},
    m_Mass{},
    m_Width{}
{

}

MXParticleData::MXParticleData ( MXParticleID ID,
                                 const std::string& Name,
                                 const ExperimentalValue& Mass,
                                 const ExperimentalValue& Width):
                                     MXParticleData()
{
    // The initialization is done in the body because we have to check the validity of
    // the input parameters
    SetPID(ID);
    SetMass(Mass);
    SetWidth(Width);
    SetName(Name);
}

MXParticleData::MXParticleData (const Core::MXConfTree& Tree):
    MXParticleData()
{
    Configure(Tree);
}

void
MXParticleData::Configure (const Core::MXConfTree& ConfTree)
{
    Core::UpdateParameter(m_PID,ConfTree, s_NN_PID);
    Core::UpdateParameter(m_Name, ConfTree, s_NN_Label);
    Core::UpdateParameter(m_Mass, ConfTree, s_NN_Mass);
    Core::UpdateParameter(m_Width, ConfTree, s_NN_Width);
}

Core::MXConfTree
MXParticleData::PopulateTree ( ) const
{
    Core::MXConfTree ConfTree;

    Core::WriteToTree(m_PID, ConfTree, s_NN_PID);
    Core::WriteToTree(m_Name, ConfTree, s_NN_Label);
    Core::WriteToTree(m_Mass, ConfTree, s_NN_Mass);
    Core::WriteToTree(m_Width, ConfTree, s_NN_Width);

    return ConfTree;

}

Core::ParameterList
MXParticleData::GetParameterList ( ) const
{
    return MXParticleData::CreateParameterList();
}

Core::ParameterList
MXParticleData::CreateParameterList()
{
    Core::ParameterList ans;
    ans.AddParameter<decltype(m_PID)>(s_NN_PID, "Montecarlo PID");
    ans.AddParameter<decltype(m_Name)>(s_NN_Label, "Particle label");
    ans.AddParameter<decltype(m_Mass)>(s_NN_Mass, "Particle mass");
    ans.AddParameter<decltype(m_Width)>(s_NN_Width, "Particle width");
    return ans;
}

/*std::string
MXParticleData::GetTableHeader()
{
    std::stringstream out;
    out << "|" << std::setw(16) << " PID " <<
        "|" << std::setw(24) << "  Mass (MeV)   " <<
        "|" << std::setw(24) << "  Width (MeV) " <<
        "|" << std::setw(16) << "  Symbol  " <<  "|";
    return out.str();
}*/

std::ostream&
operator<< (std::ostream& Out, const MXParticleData& Data)
{
    Out << Data.PopulateTree();
    return Out;
}

/*
std::istream&
operator>> (std::istream& In, MXParticleData& Data)
{
    MXParticleID TempId;
    std::string TempName;
    ExperimentalValue TempMass;
    ExperimentalValue TempWidth;

    In >> TempId >> TempMass >> TempWidth >> TempName ;


    if(!TempId.IsValid())
    {
        std::stringstream msg;
        msg << "Invalid PID streamed from the particle data: " << TempId;
        MXUtil::InvalidParameter e{msg.str()};
        e.raise();
    }

    if(TempMass.GetValue() < 0.0)
    {
        std::stringstream msg;
        msg << "The mass streamed from the particle data is negative: " << TempMass;
        MXUtil::InvalidParameter e{msg.str()};
        e.raise();
    }

    if(TempWidth.GetValue() < 0.0)
    {
        std::stringstream msg;
        msg << "The mass streamed from the particle data is negative: " << TempWidth;
        MXUtil::InvalidParameter e{msg.str()};
        e.raise();
    }

    Data.m_PID = TempId;
    Data.m_Name = TempName;
    Data.m_Mass = TempMass;
    Data.m_Width = TempWidth;

    return In;
}
*/


}  // namespace Util

}  // namespace MXWare

