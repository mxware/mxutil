/*
 * MXParticleID.cpp
 *
 *  Created on: May 28, 2015
 *      Author: caiazza
 */

#include "MXWare/Util/MXParticleID.hh"

#include <array>
#include <cstdlib>
#include <cmath>

#include <boost/log/trivial.hpp>

namespace MXWare
{

namespace Util
{



MXParticleID::MXParticleID(int32_t Id):
    m_PID{Id}
{

}

bool
MXParticleID::IsValid( ) const
{
    if(IsFundamental()||
        IsMeson() ||
        IsBaryon() ||
        IsHadron() ||
        IsLepton() ||
        IsNucleus()
        )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool
MXParticleID::IsMeson( )   const
{
    if(ExtraBits() > 0 || IsFundamental())
    {
        return false;
    }

    auto AbsID = AbsPid();

    // K_Long and K_Short are special cases
    if(AbsID == 130 || AbsID == 310)
    {
        return true;
    }

    // Checking we respect the structure of the meson id
    if(Digit(nj) > 0 &&
        Digit(nq3) > 0 &&
        Digit(nq2) > 0 &&
        Digit(nq1) == 0
        )
    {
        // Check for illegal antiparticles
/*        if(Digit(nq3) == Digit(nq2) && m_PID < 0)
        {
            return false;
        }
        else
        {
            return true;
        }*/
        return true;
    }

    return false;
}

bool
MXParticleID::IsBaryon( )  const
{
    // Baryon are composite and we are not including extension
    if(ExtraBits() > 0 ||
        IsFundamental() )
        return false;

    // Baryons have 3 quarks and half-integer spin
    if((Digit(nj) % 2) == 0 && Digit(nq3) >0 && Digit(nq2) > 0 && Digit(nq1) > 0 )
        return true;

    return false;
}

bool
MXParticleID::IsHadron( ) const
{
    if(IsMeson() || IsBaryon())
        return true;

    return false;
}

bool
MXParticleID::IsLepton( )  const
{
    if(IsFundamental() &&
        AbsPid() >= 11 && AbsPid() <= 18)
        return true;

    return false;
}

bool
MXParticleID::IsNucleus( ) const
{
    // The proton/anti-proton is also an hydrogen nucleus
    if(AbsPid() == 2212)
    {
        return true;
    }

    // PDG standard form nuclei is +/- 10LZZZAAAI
    if(Digit(n10) == 1 && Digit(n9) == 0)
    {
        // Charge less than equal the baryon number
        if(A() >= Z())
            return true;
    }

    return false;
}

bool
MXParticleID::IsFundamental( ) const
{
    if(AbsPid() <= 100)
        return true;
    else
        return false;
}

bool
MXParticleID::HasUp( ) const
{
    if(AbsPid() == 2)
        return true;

    if(IsHadron() &&
        (Digit(nq1) == 2 || Digit(nq2) == 2 || Digit(nq3) == 2) )
        return true;
    else
        return false;
}

bool
MXParticleID::HasDown( ) const
{
    if(AbsPid() == 1)
        return true;

    if(IsHadron() &&
        (Digit(nq1) == 1 || Digit(nq2) == 1 || Digit(nq3) == 1) )
        return true;
    else
        return false;
}

bool
MXParticleID::HasStrange( ) const
{
    if(AbsPid() == 3)
        return true;

    if(IsHadron() &&
        (Digit(nq1) == 3 || Digit(nq2) == 3 || Digit(nq3) == 3) )
        return true;
    else
        return false;
}

bool
MXParticleID::HasCharm( )   const
{
    if(AbsPid() == 4)
        return true;

    if(IsHadron() &&
        (Digit(nq1) == 4 || Digit(nq2) == 4 || Digit(nq3) == 4) )
        return true;
    else
        return false;
}

bool
MXParticleID::HasBottom( )  const
{
    if(AbsPid() == 5)
        return true;

    if(IsHadron() &&
        (Digit(nq1) == 5 || Digit(nq2) == 5 || Digit(nq3) == 5) )
        return true;
    else
        return false;
}

bool
MXParticleID::HasTop( )     const
{
    if(AbsPid() == 6)
        return true;

    if(IsHadron() &&
        (Digit(nq1) == 6 || Digit(nq2) == 6 || Digit(nq3) == 6) )
        return true;
    else
        return false;
}

bool
MXParticleID::HasAntiparticle( ) const
{
    if(IsBaryon() || IsNucleus())
    {
        return true;
    }
    else if(IsMeson())
    {
        if(Digit(nq3) == Digit(nq2))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        // Static array defining whether a fundamental particle is its own antiparticle
        static std::array<bool, 101> FundAntiparticles = { {0,
                                                            1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
                                                            1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
                                                            0 , 0, 0, 1, 0, 0, 0, 0, 0, 0,
                                                            0 , 0, 0, 1, 0, 0, 1, 0, 0, 0,
                                                            0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0 , 0, 0, 0, 0, 0, 0, 0, 0, 0
                                                        } };

        return FundAntiparticles[AbsPid()];
    }


    return false;

}


double
MXParticleID::Charge() const
{
    // The fundamental charges of the fundamental particles.
    static std::array<double, 101> Fund3Ch = { {0,
        -1, 2,-1, 2,-1, 2,-1, 2, 0, 0,
        -3, 0,-3, 0,-3, 0,-3, 0, 0, 0,
        0 , 0, 0, 3, 0, 0, 0, 0, 0, 0,
        0 , 0, 0, 3, 0, 0, 3, 0, 0, 0,
        0 ,-1, 0, 0, 0, 0, 0, 0, 0, 0,
        0 , 6, 3, 6, 0, 0, 0, 0, 0, 0,
        0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0 , 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0 , 0, 0, 0, 0, 0, 0, 0, 0, 0
    } };

    if(IsFundamental())
    {
        return Fund3Ch[AbsPid()]/ 3.0 * Sign();
    }

    if (IsNucleus())
    {
        return Z() * Sign();
    }

    if(Digit(nj) == 0)
        return 0.0;

    if(IsBaryon())
    {
        double q1 = Fund3Ch[Digit(nq1)];
        double q2 = Fund3Ch[Digit(nq2)];
        double q3 = Fund3Ch[Digit(nq3)];
        return (q1+q2+q3)/3.0 *Sign();
    }

    if(IsMeson())
    {
        /*
         * In the case of the mesons the particle is always that of
         * positive charge but by definition the quarks are ordered with
         * nq2 >= nq3 which means that if q2 is of "down" type one has to
         * invert the calculation
         */
        double q2 = Fund3Ch[Digit(nq2)];
        double q3 = Fund3Ch[Digit(nq3)];
        if(Digit(nq2) == 3 || Digit(nq2) == 5)
        {
            return (q3 - q2)/3.0*Sign();
        }
        else
        {
            return (q2 - q3)/3.0*Sign();
        }

    }

    return 0;
}

int
MXParticleID::A( ) const
{

    if(m_PID == 2212)
    {
        return 1;
    }
    else
    {
        return (AbsPid()/10) % 1000;
    }

    return 0;
}

int
MXParticleID::Z( ) const
{

    if(m_PID == 2212)
    {
        return 1;
    }
    else
    {
        return (AbsPid()/10000) % 1000;
    }

    return 0;

}

int
MXParticleID::Lambda( ) const
{
    if(IsNucleus())
    {
        if(m_PID == 2212)
        {
            return 0;
        }
        else
        {
            return Digit(n8);
        }
    }
    else
    {
        return 0;
    }
}

MXParticleID
MXParticleID::Electron()
{
    return MXParticleID(11);
}

MXParticleID
MXParticleID::Positron()
{
    return MXParticleID(-11);
}

MXParticleID
MXParticleID::DarkPhoton()
{
    return MXParticleID(81);
}

MXParticleID
MXParticleID::Proton()
{
    return MXParticleID(2212);
}

MXParticleID
MXParticleID::Neutron()
{
    return MXParticleID(2112);
}

MXParticleID
MXParticleID::Photon()
{
    return MXParticleID(22);
}

int
MXParticleID::ExtraBits( ) const
{
    return AbsPid()/10000000;
}

uint16_t
MXParticleID::Digit(const DigitLabel Loc) const
{
    /*
     * PDG digits (base 10) are: n10 n9 n8 n nr nl nq1 nq2 nq3 nj
     */

    int32_t num = static_cast<int32_t>(std::pow(10.0, static_cast<const double>(Loc) ) );
    return (AbsPid()/num)%10;
}

uint16_t
MXParticleID::FundamentalBits() const
{
    return Digit(nq3)*10 + Digit(nj);
}

int32_t
MXParticleID::AbsPid() const
{
    return std::abs(m_PID);
}

int16_t
MXParticleID::Sign() const
{
    if(m_PID > 0)
    {
        return 1;
    }
    else if (m_PID == 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}


std::istream&
operator>> (std::istream& in, MXParticleID& Id)
{
    in >> Id.m_PID;
    if(!Id.IsValid())
    {
        BOOST_LOG_TRIVIAL(warning) << "MXParticleId initiated with an invalid PID: " << Id.Pid();
    }
    return in;
}

}  // namespace Util

}  // namespace MXWare
