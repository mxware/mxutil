/*
 * Spinor.cpp
 *
 *  Created on: Jun 8, 2015
 *      Author: caiazza
 */

#include "MXWare/Util/Spinor.hh"

namespace MXWare
{
namespace Util
{
Spinor::Spinor (const Complex& s0, const Complex& s1, const Complex& s2, const Complex& s3) :
    m_Comp { { s0, s1, s2, s3 } }
{

}

Spinor::Spinor(const std::array<Complex, 4>& A):
    m_Comp(A)
{

}

Spinor::Spinor(std::array<Complex, 4>&& A):
    m_Comp(std::move(A))
{

}


Spinor::Spinor(const LorentzVector& K, bool Up, bool Part):
    m_Comp {}
{
    double n = K.e() + K.m();
    Complex SqrtN{std::sqrt(n)};
    Momentum P = K.vect();

    if(Part)
    {
        if (Up)
        {
            *this = Spinor{ 1     , 0,
                            P[2]/n, Complex{ P[0]/n, P[1]/n} } * SqrtN;
        }
        else
        {
            *this = Spinor{ 0, 1,
                            Complex{ P[0]/n, -P[1]/n }, -P[2]/n } * SqrtN;
        }
    }
    else
    {
        if(Up)
        {
            *this = Spinor{Complex{ P[0]/n, -P[1]/n}, -P[2]/n,
                           0                        , 1 } * SqrtN;
        }
        else
        {
            *this = Spinor{P[2]/n, Complex{ P[0]/n, P[1]/n},
                           1     , 0 } * SqrtN;
        }
    }

}
}  // namespace Util

}  // namespace MXWare


