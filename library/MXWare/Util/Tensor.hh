/**
 * \file Tensor.hh
 *
 *  Created on: Jun 9, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_TENSOR_HH_
#define SOURCE_MXUTIL_TENSOR_HH_

#include <complex>
typedef std::complex<double> Complex;

#include <array>
#include <cmath>
#include <iostream>

#include "MXWare/Util/MXUtil.hh"

namespace MXWare
{
namespace Util
{
///Forward declaration of the Spinor class
class Spinor;

/**
 * The class represents a physical tensor.
 * The Tensor is represented as a 4*4 matrix of complex numbers
 * The tensor can be constructed by:
 *  * Providing the individual components
 *  * Providing the components organized in Row objects
 *  * From a 4-momentum
 *  * Operating on the predefined constant tensor (Identity, Gamma and Metric tensors) which
 *  can be obtained through the class static functions
 *
 *  TODO Verify the correctness of the algebra
 *  TODO Check the correctness of the standard tensors
 *  TODO Complete the streaming of the tensors (probably a full input streaming is not necessary)
 *
 */
class Tensor
{
public:
    /// A fix size array representing a row of the tensor
    typedef std::array<Complex, 4> Row;

    ///Constructor from single components
    Tensor(const Complex& n00=0, const Complex& n01=0,
           const Complex& n02=0, const Complex& n03=0,
           const Complex& n10=0, const Complex& n11=0,
           const Complex& n12=0, const Complex& n13=0,
           const Complex& n20=0, const Complex& n21=0,
           const Complex& n22=0, const Complex& n23=0,
           const Complex& n30=0, const Complex& n31=0,
           const Complex& n32=0, const Complex& n33=0);

    /// Emplace constructor from the rows
    Tensor(Row&& R0,
           Row&& R1,
           Row&& R2,
           Row&& R3);

    /// Construct from the rows
    Tensor(const Row& R0,
           const Row& R1,
           const Row& R2,
           const Row& R3);

    /// Construction with a 4-Momentum (Feynamn slash)
    Tensor (const LorentzVector& P);

    /// Destructor
    ~Tensor ( ) = default;

    /// Copy
    Tensor (const Tensor& other) = default;

    /// Assignment
    Tensor&
    operator= (const Tensor& other) = default;

    /// Move
    Tensor (Tensor&& other) = default;

    /// Move assignment
    Tensor&
    operator= (Tensor&& other) = default;

    /// Row Access (no bound-checking)
    const Row&
    operator[] (size_t i) const;

    /// Row Access (no bound-checking)
    Row&
    operator[] (size_t i);

    /// Tensor Increment
    Tensor&
    operator+= (const Tensor& T);

    /// Tensor decrement
    Tensor&
    operator-= (const Tensor& T);

    /// Tensor multiplication
    Tensor&
    operator*= (const Tensor& T);

    /// Multiplication with a constant
    Tensor&
    operator*= (const Complex& K);

    /// Division by a constant
    Tensor&
    operator/= (const Complex& K);

    /// Retrieves the 4 diagonal elements of the tensor
    std::array<Complex, 4>
    Diagonal ( );

    /// Retrieves the real part of the diagonal elements of the tensor
    std::array<double, 4>
    RDiagonal ( );

    /* ********************************************
     * Static functions to retrieve special tensors
     ********************************************* */

    /// Null Tensor
    static const Tensor
    Null ( );

    /// Identity Tensor
    static const Tensor
    Id ( );

    /// G_mu_nu (Metric) Tensor
    static const Tensor
    G_mn ( );

    /// Returns 1 of the five gamma tensors indexed by the id (0 - 4)
    static const Tensor
    Gamma (size_t id);

    //static const std::array< std::array<const Tensor, 4>, 4> SigmaSpin();

private:
    ///The components of the tensor as a 4x4 matrix
    std::array<Row, 4> m_Comp;
};
/*
 /// Spin Tensor Sigma
 extern std::array< std::array<Tensor, 4>, 4> SigmaSpin;

 /// Sigma Times G_munu
 extern std::array< std::array<Tensor, 4>, 4> Sigma_gmn;*/

/* ******************************************************
 * Inline member functions
 ******************************************************* */
/// Index operator (const) to access a single row
inline const Tensor::Row&
Tensor::operator[] (size_t i) const
{
    return m_Comp[i];
}

/// Index operator to access a row
inline Tensor::Row&
Tensor::operator[] (size_t i)
{
    return m_Comp[i];
}

/// Extracts the diagonal components of the tensor
inline std::array<Complex, 4>
Tensor::Diagonal ( )
{
    return std::array<Complex, 4> { { m_Comp[0][0], m_Comp[1][1], m_Comp[2][2], m_Comp[3][3] } };
}

/// Extracts the real part of the diagonal components of the tensor
inline std::array<double, 4>
Tensor::RDiagonal()
{
    return std::array<double, 4>{ {std::real(m_Comp[0][0]),
                                   std::real(m_Comp[1][1]),
                                   std::real(m_Comp[2][2]),
                                   std::real(m_Comp[3][3])} };
}


/* *********************************************************
 * Non-member functions and operators
 *********************************************************** */

/// Equality
inline bool
operator==(const Tensor& L, const Tensor& R)
{
    return (L[0] == R[0] &&
            L[1] == R[1] &&
            L[2] == R[2] &&
            L[3] == R[3]);
}

/// Inequality
inline bool
operator!= (const Tensor& L, const Tensor& R)
{
    return !( L == R );
}

/// Addition
inline Tensor
operator+ (Tensor L, const Tensor& R)
{
    return L += R;
}

/// Subtraction
inline Tensor
operator- (Tensor L, const Tensor& R)
{
    return L -= R;
}

/// Multiplication
inline Tensor
operator* (Tensor L, const Tensor& R)
{
    return L *= R;
}

inline Tensor
operator* (Tensor T, const Complex& K)
{
    return T *= K;
}

inline Tensor
operator* (const Complex& K, Tensor T)
{
    return T *= K;
}

/// Tensor-Spinor multiplication
Spinor
operator* (const Tensor& T, const Spinor& S);

/// Division
inline Tensor
operator/ (Tensor T, const Complex& K)
{
    return T /= K;
}

/// Output to stream
std::ostream&
operator<< (std::ostream & os, const Tensor & T);

}  // namespace Util

}  // namespace MXWare


#endif /* SOURCE_MXUTIL_TENSOR_HH_ */
