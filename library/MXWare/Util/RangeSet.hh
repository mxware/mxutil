/*
 * RangeSet.hh
 *
 *  Created on: Jun 5, 2017
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_RANGESET_HH_
#define SOURCE_MXUTIL_RANGESET_HH_

#include <set>
#include <vector>
#include <initializer_list>
#include "MXWare/Core/MXExceptions.hh"
#include "MXWare/Core/MXLogger.hh"
#include "MXWare/Core/Configurable.hh"
#include "MXWare/Core/ParameterAutoConfiguration.hh"
#include <boost/current_function.hpp>
#include <sstream>
#include <iostream>
#include "MXWare/Util/Range.hh"

namespace MXWare
{
namespace Util
{

/**
 * The class manages a set of potentially disjoint ranges of the same underling type
 * The template type T should have the equality operator defined and should allow strict
 * ordering through the Compare functional
 */
template<typename T, class Compare = std::less<T> >
class RangeSet: public Core::Configurable
{
    /**
     * Sort function defining the strict ordering of the range objects
     */
    struct RangeSort
    {
    bool
    operator()(const Range<T, Compare>& lhs,
              const Range<T,Compare>& rhs)
    {
        return Compare{}(lhs.Min(), rhs.Min());
    }
    };

public:
    /// Alias to the contained object
    typedef Range<T, Compare>                           key_type;

    /// Alias to the value type returned by the accessors (same as the key)
    typedef key_type                                    value_type;

    typedef value_type&                                 reference;

    typedef const value_type&                           const_reference;

    /// Alias to the function used for the comparison
    /// This function should define the strict ordering of the range objects
    typedef RangeSort                                   key_compare;

    /// Alias to the function used to compare the values (same as for the keys)
    typedef key_compare                                 value_compare;

private:
    typedef std::set<key_type, key_compare>             inner_container_type;

public:
    /// Alias to the size type of the container
    typedef typename inner_container_type::size_type    size_type;

    /// Alias to the difference type of the container
    typedef typename inner_container_type::difference_type  difference_type;

    typedef typename inner_container_type::pointer      pointer;

    typedef typename inner_container_type::const_pointer     const_pointer;

    typedef typename inner_container_type::iterator      iterator;

    typedef typename inner_container_type::const_iterator       const_iterator;

    typedef typename inner_container_type::reverse_iterator     reverse_iterator;

    typedef typename inner_container_type::const_reverse_iterator   const_reverse_iterator;

public:


    /// Default constructor
    /// Declares an empty range set
    RangeSet():
        m_Ranges{}
    {

    }

    /**
     * Creates a RangeSet with a single input range
     * @param FirstRange
     */
    RangeSet(const Range<T, Compare>& FirstRange):
        m_Ranges{}
    {
        m_Ranges.insert(FirstRange);
    }

    /**
     * Proxy construction to the constructor with a single range
     */
    RangeSet(const T& min, const T& max):
        RangeSet(Range<T, Compare>{min, max})
    {

    }

    RangeSet(std::initializer_list<value_type> init):
        m_Ranges{}
    {
        for(auto& Range: init)
        {
            insert(Range);
        }
    }

    /// Destructor
    ~RangeSet() = default;

    /// Copy
    RangeSet(const RangeSet<T, Compare>& other) = default;

    /// Assignment
    RangeSet&
    operator=(const RangeSet<T, Compare>& other) = default;

    /// Move
    RangeSet(RangeSet<T, Compare>&& other) = default;

    /// Move assignment
    RangeSet&
    operator=(RangeSet<T, Compare>&& other) = default;

    /* **********************************************************
     *      Capacity
     * ********************************************************** */
    /**
     * Returns true if the container is empty, that is if no ranges have been added
     * @return
     */
    bool
    empty() const
    {
        return m_Ranges.empty();
    }

    /**
     * The number of disjoint ranges stored in the container
     * @return
     */
    size_type
    size() const
    {
        return m_Ranges.size();
    }

    /* **********************************************************
     *      Modifiers
     * ********************************************************** */
    /**
     * Clears the content of the container.
     * All iterators will become invalid
     */
    void
    clear()
    {
        m_Ranges.clear();
    }

    /**
     * Inserts a new range in the container sorting it automatically and merging overlapping ranges
     * @param NewRange
     * @return
     */
    iterator
    insert(const value_type& NewRange)
    {
        if(empty())
        {
            return m_Ranges.insert(NewRange).first;
        }

        // Trying to insert in the range
        auto insertion_result = m_Ranges.insert(NewRange);
        if(!insertion_result.second)
        {
            auto insertion_point = insertion_result.first;
            // if the insertion failed it means there is already a range with the same minimum.
            // if the new range is smaller or equal than the existing one then we can just ignore
            // the insertion and return the iterator to the already existing element
            if(insertion_point->Length() >= NewRange.Length())
            {
                return insertion_point;
            }
            // otherwise we have to merge the ranges
            else
            {
                auto insertion_hint = m_Ranges.erase(insertion_point);
                insertion_point = m_Ranges.insert(insertion_hint, NewRange);
                return MergeOverlappingRanges(insertion_point);
            }
        }
        else
        {
            auto insertion_point = insertion_result.first;
            if(insertion_point != m_Ranges.begin())
            {
                auto prev_range_it = std::prev(insertion_point);
                if(prev_range_it->Overlap(*insertion_point) ||
                    prev_range_it->Max() == insertion_point->Min())
                {
                    insertion_point = prev_range_it;
                }
            }
            return MergeOverlappingRanges(insertion_point);
        }
    }

    /**
     * Insert a range object built from a pair of min and maxß
     * @param min
     * @param max
     * @return
     */
    iterator
    insert(const T& min, const T& max)
    {
        return insert(value_type{min, max});
    }



    /* **********************************************************
     *      Iterators
     * ********************************************************** */

    /// Begin of the set
    iterator
    begin()
    {
        return m_Ranges.begin();
    }

    const_iterator
    begin() const
    {
        return m_Ranges.begin();
    }

    const_iterator
    cbegin() const
    {
        return m_Ranges.cbegin();
    }

    /// End of the set
    iterator
    end()
    {
        return m_Ranges.end();
    }

    const_iterator
    end() const
    {
        return m_Ranges.end();
    }

    const_iterator
    cend() const
    {
        return m_Ranges.cend();
    }

    reverse_iterator
    rbegin()
    {
        return m_Ranges.rbegin();
    }

    const_reverse_iterator
    rbegin() const
    {
        return m_Ranges.rbegin();
    }

    const_reverse_iterator
    crbegin() const
    {
        return m_Ranges.crbegin();
    }

    reverse_iterator
    rend()
    {
        return m_Ranges.rend();
    }

    const_reverse_iterator
    rend() const
    {
        return m_Ranges.rend();
    }

    const_reverse_iterator
    crend() const
    {
        return m_Ranges.crend();
    }


    /* **********************************************************
     *      Range specific operations
     * ********************************************************** */

    /**
     * Returns the minimum value of the range set
     * WARNING: Throws an exception on empty sets
     * @return
     */
    const T&
    Min() const
    {
        if(empty())
        {
            std::stringstream err;
            err << "No minimum defined for an empty range";
            BOOST_LOG_TRIVIAL(error) << err.str();
            Core::InvalidParameter e{"RangeSet", BOOST_CURRENT_FUNCTION, err.str()};
            e.raise();
        }

        return m_Ranges.begin()->Min();
    }

    /**
     * Returns the maximum value of the range set
     * WARNING: Throws an exception on empty sets
     * @return
     */
    const T&
    Max() const
    {
        if(empty())
        {
            std::stringstream err;
            err << "No maximum defined for an empty range";
            BOOST_LOG_TRIVIAL(error) << err.str();
            Core::InvalidParameter e{"RangeSet", BOOST_CURRENT_FUNCTION, err.str()};
            e.raise();
        }

        return m_Ranges.rbegin()->Max();
    }

    /**
     * The total length of all the contained ranges (different from max - min)
     * @return
     */
    T
    Length( ) const
    {
        T ans = 0.0;
        for(auto& Range: m_Ranges)
        {
            ans += Range.Length();
        }
        return ans;
    }

    /**
     * Checks if an input range overlaps the contained set
     * @param other
     * @return
     */
    bool
    Overlap(const Range<T, Compare>& other) const
    {
        for(auto& R: m_Ranges)
        {
            if(R.Overlap(other))
                return true;
        }
        return false;
    }

    /**
     * Checks whether a value is in the range of any of the ranges of the set
     * @param Value
     * @return
     */
    bool
    InRange(const T& Value) const
    {
        constexpr Compare Less_than{};
        if(Less_than(Value, Min()) || Less_than(Max(), Value))
        {
            return false;
        }
        else
        {
            for(auto& R: m_Ranges)
            {
                if(R.InRange(Value))
                    return true;
            }

            return false;
        }
    }

    /**
     * Returns true if at least one of the ranges in the input set overlaps with one
     * of the ranges of the current one
     * @param other
     * @return
     */
    bool
    Overlap(const RangeSet<T, Compare>& other) const
    {
        for(auto& R: other)
        {
            if(this->Overlap(R))
                return true;
        }
        return false;
    }

    /// Configures the concrete class through a property tree
    void
    Configure (const Core::MXConfTree& ConfTree) override
    {
        m_Ranges.clear();

        if(!ConfTree.data().empty())
        {
            auto TheRange = ConfTree.get_value<Range<T, Compare>>();
            insert(TheRange);
        }

        for(auto& Node: ConfTree)
        {
            if(Node.first == s_NN_InnerRange)
            {
                auto TheRange = Node.second.get_value<Range<T, Compare>>();
                insert(TheRange);
            }
        }
    }

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters
     * @return
     */
    Core::MXConfTree
    PopulateTree ( ) const override
    {
        Core::MXConfTree ans;
        for(auto& Range: m_Ranges)
        {
            ans.add(s_NN_InnerRange, Range);
        }
        return ans;
    }

    /// Returns a list of the configuration parameter.
    Core::ParameterList
    GetParameterList ( ) const override
    {
        Core::ParameterList ans;

        ans.AddParameter<std::vector<Range<T, Compare>>>(s_NN_InnerRange, "One of the ranges composing the set");

        return ans;
    }

    /* ******************************************************
     *              Logical operators
     * ****************************************************** */
    /**
     * Logical union of the set with another range
     * Equivalent to the insertion of the new range in the existing set.
     * Overlaps are taken care of by the insertion algorithm
     * @param rhs
     * @return
     */
    RangeSet<T, Compare>&
    operator|=(const Range<T, Compare>& rhs)
    {
        insert(rhs);
        return *this;
    }

    /**
     * Logical union of the set with another set.
     * Equivalent with the merging of all ranges of the other set in the existing one
     * @param rhs
     * @return
     */
    RangeSet<T, Compare>&
    operator|=(const RangeSet<T, Compare>& rhs)
    {
        for(auto& R: rhs)
        {
            insert(R);
        }
        return *this;
    }

    /**
     * Logical intersection between a set and a range
     * Equivalent to the intersection of the range with all the ranges of this set
     * @param rhs
     * @return
     */
    RangeSet<T, Compare>&
    operator&=(const Range<T, Compare>& rhs)
    {
        *this = *this && rhs;

        return *this;
    }

    /**
     * Logical intersection between two range sets
     * @param rhs
     * @return
     */
    RangeSet<T, Compare>&
    operator&=(const RangeSet<T, Compare>& rhs)
    {
        *this = *this && rhs;

        return *this;
    }

    /* ******************************************************
     *              Arithmetic operators
     * ****************************************************** */
    /**
     * Addition (shift by a constant)
     * @param Constant
     * @return
     */
    RangeSet<T, Compare>&
    operator+=(const T& Constant)
    {
        inner_container_type NewRangeSet;
        for(auto& Range: m_Ranges)
        {
            NewRangeSet.insert(Range + Constant);
        }
        m_Ranges = NewRangeSet;
        return *this;
    }

    /**
     * Subtraction (shift by a constant)
     * @param Constant
     * @return
     */
    RangeSet<T, Compare>&
    operator-=(const T& Constant)
    {
        inner_container_type NewRangeSet;
        for(auto& Range: m_Ranges)
        {
            NewRangeSet.insert(Range - Constant);
        }
        m_Ranges = NewRangeSet;
        return *this;

    }

    /**
     * Multiplication (proportional dilation)
     * @param Constant
     * @return
     */
    RangeSet<T, Compare>&
    operator*=(const T& Constant)
    {
        inner_container_type NewRangeSet;
        for(auto& Range: m_Ranges)
        {
            NewRangeSet.insert(Range * Constant);
        }
        m_Ranges = NewRangeSet;
        return *this;
    }

    /**
     * Division (proportional contraction)
     * @param Constant
     * @return
     */
    RangeSet<T, Compare>&
    operator/=(const T& Constant)
    {
        inner_container_type NewRangeSet;
        for(auto& Range: m_Ranges)
        {
            NewRangeSet.insert(Range / Constant);
        }
        m_Ranges = NewRangeSet;
        return *this;
    }

private:
    /// Container of the disjoint ranges
    inner_container_type m_Ranges;

    /// configuration node name for the inner range to store
    static const std::string s_NN_InnerRange;

    /**
     * Internal function to merge all the overlapping ranges starting from the one
     * pointed to by the start iterator
     * @param start
     * @return
     */
    iterator
    MergeOverlappingRanges(iterator start)
    {
        auto next_element_it = std::next(start);
        if(next_element_it == m_Ranges.end() ||
            (!start->Overlap(*next_element_it) && start->Max() != next_element_it->Min()) )
        {
            return start;
        }

        auto NewElement = *start;
        while(next_element_it != m_Ranges.end() &&
            (NewElement.Overlap(*next_element_it) || NewElement.Max() == next_element_it->Min() ))
        {
            NewElement |= *next_element_it;
            next_element_it = m_Ranges.erase(next_element_it);
        }
        m_Ranges.erase(start);
        return m_Ranges.insert(next_element_it, NewElement);
    }

    /* ******************************************************
     *              Comparison operators
     * ****************************************************** */
    /**
     * Comparison operator
     * checks that all ranges in both sets are the same
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool
    operator==(const RangeSet<T, Compare>& lhs, const RangeSet<T, Compare>& rhs)
    {
        if(lhs.size() != rhs.size())
            return false;

        auto lhs_it = lhs.begin();
        auto rhs_it = rhs.begin();
        for(; lhs_it != lhs.end(); ++ lhs_it, ++ rhs_it)
        {
            if(!(*lhs_it == *rhs_it))
                return false;
        }
        return true;
    }

    /**
     * inequality operator
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool
    operator!=(const RangeSet<T, Compare>& lhs, const RangeSet<T, Compare>& rhs)
    {
        return !(lhs == rhs);
    }

    /* ******************************************************
     *              Logical operators
     * ****************************************************** */

    /**
     * Logical OR between two range sets (set union)
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator||(RangeSet<T, Compare> lhs, const RangeSet<T, Compare>& rhs)
    {
        return lhs |= rhs;
    }

    /**
     * Logical OR between a set and a range
     * NOTE the ordering SET || RANGE
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator||(RangeSet<T, Compare> lhs, const Range<T, Compare>& rhs)
    {
        return lhs |= rhs;
    }

    /**
     * Logical OR between a range and a set
     * NOTE the ordering RANGE || SET
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator||(const Range<T, Compare>& lhs, RangeSet<T, Compare> rhs)
    {
        return rhs |= lhs;
    }

    /**
     * Logical OR between two ranges which produces a set
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator||(const Range<T, Compare>& lhs, const Range<T, Compare>& rhs)
    {
        RangeSet<T, Compare> ans{lhs};
        return ans |= rhs;
    }

    /**
     * Logical AND between a range and a range set (set intersection)
     * NOTE the ordering SET && RANGE
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator&&(const RangeSet<T, Compare>& lhs, const Range<T, Compare>& rhs)
    {
        RangeSet<T, Compare> ans;

        for(auto& R: lhs)
        {
            if(R.Min() > rhs.Max())
                break;

            if(rhs.Overlap(R))
            {
                ans.insert( rhs && R);
            }
        }
        return ans;
    }

    /**
     * Logical AND between a range and a range set (set intersection)
     * NOTE the ordering RANGE && SET (commutation of the previous)
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator&&(const Range<T, Compare>& rhs, const RangeSet<T, Compare>& lhs)
    {
        return lhs && rhs;
    }

    /**
     * Intersection between two sets
     * @param lhs
     * @param rhs
     * @return
     */
    friend RangeSet<T, Compare>
    operator&&(const RangeSet<T, Compare>& lhs, const RangeSet<T, Compare>& rhs)
    {
        RangeSet<T, Compare> ans;

        for(auto& OtherRange: rhs)
        {
            if(OtherRange.Min() > lhs.Max())
                break;

            for(auto& R: lhs)
            {
                if(R.Min() > OtherRange.Max())
                    break;

                if(OtherRange.Overlap(R))
                {
                    ans.insert(OtherRange && R);
                }
            }
        }

        return ans;
    }

    /* ******************************************************
     *              Arithmetic operators
     * ****************************************************** */
    /**
     * Addition
     * @param lhs
     * @param Constant
     * @return
     */
    friend RangeSet<T, Compare>
    operator+(const RangeSet<T, Compare> lhs, const T& Constant)
    {
        RangeSet<T, Compare> ans;
        for(auto& Range: lhs)
        {
            ans.insert(Range + Constant);
        }
        return ans;
    }

    /**
     * Subtraction
     * @param lhs
     * @param Constant
     * @return
     */
    friend RangeSet<T, Compare>
    operator-(const RangeSet<T, Compare> lhs, const T& Constant)
    {
        RangeSet<T, Compare> ans;
        for(auto& Range: lhs)
        {
            ans.insert(Range - Constant);
        }
        return ans;
    }

    /**
     * Multiplication
     * @param lhs
     * @param Constant
     * @return
     */
    friend RangeSet<T, Compare>
    operator*(const RangeSet<T, Compare> lhs, const T& Constant)
    {
        RangeSet<T, Compare> ans;
        for(auto& Range: lhs)
        {
            ans.insert(Range * Constant);
        }
        return ans;
    }

    /**
     * Division
     * @param lhs
     * @param Constant
     * @return
     */
    friend RangeSet<T, Compare>
    operator/(const RangeSet<T, Compare> lhs, const T& Constant)
    {
        RangeSet<T, Compare> ans;
        for(auto& Range: lhs)
        {
            ans.insert(Range / Constant);
        }
        return ans;
    }


    /* ******************************************************
     *              Streaming operators
     * ****************************************************** */

    /**
     * Stream out operator
     * @param out
     * @param Set
     * @return
     */
    friend std::ostream&
    operator<<(std::ostream& out, const RangeSet<T, Compare>& Set)
    {
        for(auto& R: Set)
        {
            out << R << '\t';
        }
        return out;
    }



};

template<typename T, class Compare>
const std::string RangeSet<T, Compare>::s_NN_InnerRange{"range"};


/// Alias to the Range of doubles
typedef RangeSet<double>    RangeSetD;

/**
 * Extracts a value within the range set proportional to the fraction
 * Only defined for real types
 * @param Fraction
 * @return
 */
template<typename T, class Compare = std::less<T> >
typename std::enable_if<std::is_floating_point<T>::value, T>::type
ProportionalExtract(const RangeSet<T, Compare>& Set, const T& Fraction)
{
    if(Set.empty())
    {
        std::stringstream err;
        err << "Impossible to extract values from an empty range set";
        BOOST_LOG_TRIVIAL(error) << err.str();
        Core::InvalidParameter e{"", BOOST_CURRENT_FUNCTION, err.str()};
        e.raise();
    }

    if(Fraction < 0.0 || Fraction >1.0)
    {
        std::stringstream err;
        err << "The extraction fraction should be a number included between 0 and 1 "
            "and is " << Fraction;
        BOOST_LOG_TRIVIAL(error) << err.str();
        Core::InvalidParameter e{"", BOOST_CURRENT_FUNCTION, err.str()};
        e.raise();
    }

    auto RemainingLength = Fraction * Set.Length();
    for(auto& R: Set)
    {
        if(RemainingLength < R.Length())
        {
            return R.Min() + RemainingLength;
        }
        else
        {
            RemainingLength -= R.Length();
        }
    }
    if(RemainingLength == 0.0)
        return Set.Max();

    std::stringstream err;
    err << "Mismatch between the total length and the sum of the ranges. "
        "This should not be happening";
    BOOST_LOG_TRIVIAL(error) << err.str();
    Core::MXGeneralException e{"", BOOST_CURRENT_FUNCTION, err.str()};
    e.raise();

    return T{};
}

}  // namespace Util

}  // namespace MXWare




#endif /* SOURCE_MXUTIL_RANGESET_HH_ */
