/**
 * \file PhysicalConstants.hh
 *
 * File containing the definition of the most common physical constants
 *
 *  Created on: Mar 23, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_PHYSICALCONSTANTS_HH_
#define SOURCE_MXUTIL_PHYSICALCONSTANTS_HH_

#include "MXWare/Util/SystemOfUnits.hh"


#endif /* SOURCE_MXUTIL_PHYSICALCONSTANTS_HH_ */
