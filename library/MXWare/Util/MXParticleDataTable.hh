/**
 * \file MXParticleDataTable.hh
 *
 *  Created on: May 20, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_MXPARTICLEDATATABLE_HH_
#define SOURCE_MXUTIL_MXPARTICLEDATATABLE_HH_

#include "MXWare/Core/Configurable.hh"

#include "boost/filesystem.hpp"

#include "MXWare/Util/MXParticleData.hh"
#include "MXWare/Util/MXParticleID.hh"
namespace bfs = boost::filesystem;
#include "MXWare/Util/MXUtil.hh"

#include <string>
#include <memory>
#include <set>
#include <fstream>
#include <vector>

namespace MXWare
{

namespace Util
{
/**
 * Ordered container for the particle data.
 * The ordering of the particle data is defined in the @class MXParticleData class
 * and is ascending by PID.
 * The PIDs of the particles contained in the container are unique.
 * Because the data comparison is only based on the PID, one cannot add a particle with the
 * same PID and but different physical parameters
 *
 * TODO Iterators
 * TODO Input/output from ConfTree
 *
 */
class MXParticleDataTable: public Core::Configurable
{
private:
    /// The type of object storing the data
    typedef std::set<MXParticleData> DataTable;

public:
    /*
     * STL typedefs
     */
    /// The type to which an iterator to the object dereferences
    typedef DataTable::value_type               value_type;
    typedef DataTable::reference                reference;
    typedef DataTable::const_reference          const_reference;
    typedef DataTable::pointer                  pointer;
    typedef DataTable::const_pointer            const_pointer;

    /// The function used to compare the contained values
    typedef DataTable::value_compare            value_compare;

    /// Bidirectional iterator types through the table
    typedef DataTable::iterator                 iterator;
    typedef DataTable::const_iterator           const_iterator;
    typedef DataTable::reverse_iterator         reverse_iterator;
    typedef DataTable::const_reverse_iterator   const_reverse_iterator;

    /// Signed integral for the distance between iterators
    typedef DataTable::difference_type          difference_type;

    /// Unsigned integral for the size of the container
    typedef DataTable::size_type                size_type;




    /**
     * Default constructor.
     * It creates an empty table
     */
    MXParticleDataTable ( );

    /**
     * Range construction
     * @param first
     * @param last
     */
    template<class InputIterator>
    MXParticleDataTable(InputIterator first, InputIterator last):
        m_Data{first, last}
    {

    }

    /**
     * Initializer list construction
     * @param PDataList
     */
    MXParticleDataTable(std::initializer_list<MXParticleData> PDataList);

    /**
     * Default destructor
     * The class is final, should not be extended
     */
    ~MXParticleDataTable ( ) = default;

    /**
     * Copy
     * @param other
     */
    MXParticleDataTable (const MXParticleDataTable& other) = default;

    /**
     * Assignment
     */
    MXParticleDataTable&
    operator= (const MXParticleDataTable& other) = default;

    /**
     * Default move
     * @param other
     */
    MXParticleDataTable (MXParticleDataTable&& other) = default;

    /**
     * Move assign
     * @param other
     * @return
     */
    MXParticleDataTable&
    operator= (MXParticleDataTable&& other) = default;

    /*
     * CAPACITY
     */

    /**
     * Checks if the table is empty
     * @return true if the table is empty
     */
    bool
    empty() const;

    /**
     * Gets the number of stored particles
     * @return
     */
    size_t
    size ( ) const;

    /*
     * ITERATORS
     */
    /**
     * Iterator to the beginning of the table
     * @return
     */
    iterator
    begin();

    const_iterator
    begin() const;

    const_iterator
    cbegin() const;

    /**
     * Iterator to the end of the table
     * @return
     */
    iterator
    end();

    const_iterator
    end() const;

    const_iterator
    cend() const;

    /**
     * Reverse iterator to the reverse begin of the container
     * @return
     */
    reverse_iterator
    rbegin();

    const_reverse_iterator
    rbegin() const;

    const_reverse_iterator
    crbegin() const;

    /**
     * Reverse iterator to the reverse end of the container
     * @return
     */
    reverse_iterator
    rend();

    const_reverse_iterator
    rend() const;

    const_reverse_iterator
    crend() const;

    /*
     * MODIFIERS (compatible with the STL set syntax)
     */
    /**
     * Single element insertion
     * @param PData
     * @return a pair, with its member pair::first set to an iterator pointing to either
     * the newly inserted element or to the equivalent element already in the set.
     * The pair::second element in the pair is set to true if a new element was inserted
     * or false if an equivalent element already existed.
     */
    std::pair<iterator, bool>
    insert(const value_type& PData);

    /**
     * Single element insertion (move version)
     * @param PData
     * @return
     */
    std::pair<iterator, bool>
    insert(value_type&& PData);

    /**
     * Insertion with hint
     * @param position
     * @param PData
     * @return iterator to either the new element or that which had the same key in the container
     */
    iterator
    insert(const_iterator position, const value_type& PData);

    /**
     * Insertion with hint (move version)
     * @param position
     * @param PData
     * @return
     */
    iterator
    insert(const_iterator position, value_type&& PData);

    /**
     * Range insertion
     * Needs an iterator that can be derefenced to value_type
     * @param first
     * @param last
     */
    template<class InputIterator>
    void
    insert(InputIterator first, InputIterator last)
    {
        m_Data.insert(first, last);
    }

    /**
     * Initializer list insertion
     * @param PDataList
     */
    void
    insert(std::initializer_list<value_type> PDataList);

    /**
     * Erase an element at the given position
     * @param position
     * @return the position of the element after the one that was deleted
     */
    iterator
    erase(const_iterator position);

    /**
     * Erase an element that compares equal to the argument
     * @param PData
     * @return the number of elements that were deleted
     */
    size_type
    erase(const value_type& PData);

    /**
     * Deletes a range of elements
     * @param first
     * @param last
     * @return the iterator of the leement after the last one that was deleted
     */
    iterator
    erase(const_iterator first, const_iterator last);

    /**
     * Swaps the content of two tables
     * @param other
     */
    void
    swap(MXParticleDataTable& other);

    /**
     * Empties the table
     * @return
     */
    void
    clear ( ) noexcept;

    /**
     * Emplace insertion of anelement in the table
     * @param args
     * @return
     */
    template<class... Args>
    std::pair<iterator, bool>
    emplace(Args&&... args)
    {
        return m_Data.emplace(std::forward<Args>(args)...);
    }

    /**
     * Emplace insertion with a hint to the insertion position
     * @param position
     * @param args
     * @return
     */
    template<class... Args>
    std::pair<iterator, bool>
    emplace_hint(const_iterator position, Args&&... args)
    {
        return m_Data.emplace_hint(position, std::forward<Args>(args)...);
    }

    /*
     * OPERATIONS (Compatible with the STL naming)
     */
    /**
     * Finds a Pdata object with the same PID as the argument
     * @param PData
     * @return
     */
    const_iterator
    find(const value_type& PData) const;

    /**
     * Finds a Pdata object with the same PID as the argument
     * @param PData
     * @return
     */
    const_iterator
    find(const MXParticleID& Id) const;

    /**
     * Finds a Pdata object with the same PID as the argument
     * @param PData
     * @return
     */
    iterator
    find(const value_type& PData);

    /**
     * Finds a Pdata object with the same PID as the argument
     * @param PData
     * @return
     */
    iterator
    find(const MXParticleID& Id);

    /**
     * Counts the number of matches of PData in the container
     * @param PData
     * @return
     */
    size_type
    count(const value_type& PData);

    /**
     * Returns an iterator pointing to the first element in the container
     * which is not considered to go before the argument (i.e., either it is equivalent or goes after).
     * @param PData
     * @return
     */
    const_iterator
    lower_bound(const value_type& PData) const;

    /**
     * Returns an iterator pointing to the first element in the container
     * which is not considered to go before the argument (i.e., either it is equivalent or goes after).
     * @param PData
     * @return
     */
    const_iterator
    lower_bound(const MXParticleID& Id) const;

    /**
     * Returns an iterator pointing to the first element in the container
     * which is not considered to go before the argument (i.e., either it is equivalent or goes after).
     * @param PData
     * @return
     */
    iterator
    lower_bound(const value_type& PData);

    /**
     * Returns an iterator pointing to the first element in the container
     * which is not considered to go before the argument (i.e., either it is equivalent or goes after).
     * @param PData
     * @return
     */
    iterator
    lower_bound(const MXParticleID& Id);

    /**
     * Returns an iterator pointing to the first element in the container
     * which is considered to go after the argument
     * @param PData
     * @return
     */
    const_iterator
    upper_bound(const value_type& PData) const;

    /**
     * Returns an iterator pointing to the first element in the container
     * which is considered to go after the argument
     * @param PData
     * @return
     */
    const_iterator
    upper_bound(const MXParticleID& Id) const;

    /**
     * Returns an iterator pointing to the first element in the container
     * which is considered to go after the argument
     * @param PData
     * @return
     */
    iterator
    upper_bound(const value_type& PData);

    /**
     * Returns an iterator pointing to the first element in the container
     * which is considered to go after the argument
     * @param PData
     * @return
     */
    iterator
    upper_bound(const MXParticleID& Id);

    /*
     * Configurable interface
     */

    /// Configures the concrete class through a property tree
    void
    Configure (const Core::MXConfTree& ConfTree) override;

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters
     * @return
     */
    Core::MXConfTree
    PopulateTree ( ) const override;

    /// Returns a list of the configuration parameter.
    Core::ParameterList
    GetParameterList ( ) const override;

    /**
     * List of the supported file formats
     */
    enum class SupportedFormats
    {
        AUTO = 0, //!< Automatic identification from file extension
        PDG2014 = 1, //!< PDG2014
        XML = 2 //!< XML Configuration Tree
    };

    /**
     * Loads the particle data from a file formatted with the required format type
     *
     * @param Filename
     */
    void
    LoadData (const bfs::path& Filename,
              SupportedFormats Format = SupportedFormats::PDG2014,
              bool Overwrite = false);

    /**
     * Saves the data contained in the current table in a file.
     * Only the XML format is allowed
     * @param Filename
     */
    void
    SaveData(const bfs::path& Filename);

    /**
     * Creates a global static instance of a particle data table stored in a smart
     * pointer
     * @return
     */
    static MXParticleDataTable&
    GlobalPDataTable ( );

    /**
     * Loads a set of default particle data in the table
     * @param Table
     */
    static void
    LoadDefaultData( MXParticleDataTable& Table, bool Overwrite = false);


    /**
     * Retrieves the data using a Magix Particle ID (the particle code atm) as key
     * This is similar to find but returns a reference
     * @param Id
     * @return
     */
    [[deprecated("Replaced by find with standard STL interface. It will be removed in the next release.")]]
    const MXParticleData&
    GetData (const MXParticleID& Id) const;

private:

    /// The Particle Data table which actually stores the data
    DataTable m_Data;

    /// Node name for the particle data in the table
    static const std::string s_NN_PData;

    /**
     * Parses the input file assuming the PDG2014 format
     * The description of the format one should follow the formatting instruction of the input PDG
     * file. The units are typically in GeV and they have to be properly converted using the
     * system of units
     * * 1) ignore documentation lines that begin with an asterisk
     * * 2) in a FORTRAN program, process data lines with
     *    FORMAT (BN, 4I8, 2(1X,E18.0, 1X,E8.0, 1X,E8.0), 1X,A21)
     * * 3)    column
     *       1 -  8 \ Monte Carlo particle numbers as described in the "Review of
     *       9 - 16 | Particle Physics". Charge states appear, as appropriate,
     *      17 - 24 | from left-to-right in the order -, 0, +, ++.
     *      25 - 32 /
     *           33   blank
     *      34 - 51   central value of the mass (double precision)
     *           52   blank
     *      53 - 60   positive error
     *           61   blank
     *      62 - 69   negative error
     *           70   blank
     *      71 - 88   central value of the width (double precision)
     *           89   blank
     *      90 - 97   positive error
     *           98   blank
     *      99 -106   negative error
     *          107   blank
     *     108 -128   particle name left-justified in the field and
     *                charge states right-justified in the field.
     *                This field is for ease of visual examination of the file and
     *                should not be taken as a standardized presentation of
     *                particle names.
     * @param File
     */
    void
    ParsePDG2014 (std::ifstream& File, bool Overwrite = false);

    /**
     * Parse the data contained in an xml structure
     * @param File
     * @param Overwrite
     */
    void
    ParseConfigurationTree(const Core::MXConfTree& Tree, bool Overwrite = false);
};

}  // namespace Util

}  // namespace MXWare


#endif /* SOURCE_MXUTIL_MXPARTICLEDATATABLE_HH_ */
