/**
 * \file MXParticleData.hh
 *
 *  Created on: May 31, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_MXPARTICLEDATA_HH_
#define SOURCE_MXUTIL_MXPARTICLEDATA_HH_

#include <string>
#include <iostream>
#include "MXWare/Core/MXExceptions.hh"
#include "MXWare/Core/Configurable.hh"
#include "MXWare/Util/ExperimentalValue_T.hh"
#include "MXWare/Util/MXParticleID.hh"

namespace MXWare
{

namespace Util
{

/**
 * Container for the experimental values of the physical particles
 * Each particle is indexed by its PID and has a predefined "human readable" label
 * The label is composed of one or more letters and numbers and the charge status
 * of the particle if the same particle has more than one of them.
 * Antiparticles are labeled with a ~ in front of the name of the corresponding particle
 * The physical quantities stored in this class are:
 * * Mass
 * * Width
 * Both the mass and the width are expressed in energy units so it is important to remember
 * to multiply by the appropriate c and h factors when using them in derived quantities
 *
 * TODO Maybe a link to the list of branching ratios
 *
 * The charge status and other quantum numbers can be derived directly from the PID
 *
 * TODO Configuration through a configuration tree
 */
class MXParticleData: public Core::Configurable
{
public:
    /**
     * Constructor
     * Creates a particle with ID 0 which is considered invalid
     */
    MXParticleData ( );

    /**
     * Full constructor which feels all the particle data
     * @param ID
     * @param Name
     * @param Mass
     * @param Width
     */
    MXParticleData ( MXParticleID ID,
                     const std::string& Name,
                     const ExperimentalValue& Mass,
                     const ExperimentalValue& Width);

    /**
     * Constructs and configure a particle data object from a configuration tree
     * @param Tree
     */
    MXParticleData (const Core::MXConfTree& Tree);

    /**
     * Destructor
     */
    ~MXParticleData ( ) = default;

    /**
     * Copy
     * @param other
     */
    MXParticleData (const MXParticleData& other) = default;

    /**
     * Assignment
     * @param other
     * @return
     */
    MXParticleData&
    operator= (const MXParticleData& other) = default;

    /**
     * Move
     * @param other
     */
    MXParticleData (MXParticleData&& other) = default;

    /**
     * Move Assignment
     * @param other
     * @return
     */
    MXParticleData&
    operator= (MXParticleData&& other) = default;

    /**
     * Returns the PID
     * @return
     */
    const MXParticleID&
    PID ( ) const;

    /**
     * Sets the PID
     * @param Id
     */
    void
    SetPID (const MXParticleID& Id);

    /**
     * Returns the particle name
     * @return
     */
    const std::string&
    Name ( ) const;

    /**
     * Sets the particle name
     * @param Name
     */
    void
    SetName (const std::string& Name);

    /**
     * Retrieves the particle mass
     * @return
     */
    const ExperimentalValue&
    Mass ( ) const;

    /**
     * Sets the particle mass (must be positive)
     * @param Mass
     */
    void
    SetMass (const ExperimentalValue& Mass);

    /**
     * Retrieves the particle width
     * @return
     */
    const ExperimentalValue&
    Width ( ) const;

    /**
     * Sets the particle width (must be positive)
     * @param Width
     */
    void
    SetWidth (const ExperimentalValue& Width);

    /// Configures the concrete class through a property tree
    void
    Configure (const Core::MXConfTree& ConfTree) override;

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters
     * @return
     */
    Core::MXConfTree
    PopulateTree ( ) const override;

    /**
     * Returns a list of the configuration parameter metadata.
     * This allows the configuration structure to be queried by the configuration
     * system.
     * @return
     */
    Core::ParameterList
    GetParameterList ( ) const override;

    /**
     * Returns the header of the table in which the particle data are output by the
     * stream function
     * @return
     */
/*    static std::string
    GetTableHeader();*/

    /// Equality
    friend bool
    operator== (const MXParticleData& Left, const MXParticleData& Right);

    /// Not Equality
    friend bool
    operator!= (const MXParticleData& Left, const MXParticleData& Right);

    /// Less than (Particle Data are sorted according to Pid)
    friend bool
    operator< (const MXParticleData& Left, const MXParticleData& Right);

    /// Strict ordering of the particles according to their PID
    friend bool
    operator> (const MXParticleData& Left, const MXParticleData& Right);

    /// Weak ordering according to the PID
    friend bool
    operator<=(const MXParticleData& Left, const MXParticleData& Right);

    /// Weak ordering according to the PID
    friend bool
    operator>=(const MXParticleData& Left, const MXParticleData& Right);

    /// Streaming out
    friend std::ostream&
    operator<< (std::ostream& Out, const MXParticleData& Data);

/*    friend std::istream&
    operator>> (std::istream& In, MXParticleData& Data);*/

private:
    /// The particle ID
    MXParticleID m_PID;

    /// The particle name
    std::string m_Name;

    /// The physical mass and its error
    ExperimentalValue m_Mass;

    /// The physical width and its error
    ExperimentalValue m_Width;

    /// Node names of the configurable variables
    static const std::string s_NN_PID;
    static const std::string s_NN_Label;
    static const std::string s_NN_Mass;
    static const std::string s_NN_Width;

    /// Creates the parameter list
    static Core::ParameterList
    CreateParameterList();
};


std::ostream&
operator<< (std::ostream& Out, const MXParticleData& Data);

std::istream&
operator>> (std::istream& In, MXParticleData& Data);


/* **************************************************
 *
 *          Inline functions
 *
 ************************************************** */

inline const MXParticleID&
MXParticleData::PID ( ) const
{
    return m_PID;
}

inline void
MXParticleData::SetPID (const MXParticleID& Id)
{
    if(Id.IsValid())
        m_PID = Id;
    else
    {
        std::stringstream msg;
        msg <<"Invalid PID set in the Particle data: " << Id;
        Core::InvalidParameter e{"MXParticleData", "SetPID", msg.str()};
        e.raise();
    }
}

inline const std::string&
MXParticleData::Name ( ) const
{
    return m_Name;
}

inline void
MXParticleData::SetName (const std::string& Name)
{
    m_Name = Name;
}

inline const ExperimentalValue&
MXParticleData::Mass ( ) const
{
    return m_Mass;
}

inline void
MXParticleData::SetMass (const ExperimentalValue& Mass)
{
    if (Mass.GetValue() < 0.0)
    {
        Core::InvalidParameter e("MXParticleData", "SetMass", "The particle mass must be positive.");
        e.raise();
    }
    m_Mass = Mass;
}

inline const ExperimentalValue&
MXParticleData::Width ( ) const
{
    return m_Width;
}

inline void
MXParticleData::SetWidth (const ExperimentalValue& Width)
{
    if(Width.GetValue() < 0.0)
    {
        Core::InvalidParameter e("MXParticleData", "SetWidth", "The particle width must be positive.");
        e.raise();
    }
    m_Width = Width;
}

inline bool
operator== (const MXParticleData& Left, const MXParticleData& Right)
{
    return ( Left.PID() == Right.PID() &&
        Left.Name() == Right.Name() &&
        Left.Mass() == Right.Mass() &&
        Left.Width() == Right.Width() );
}

inline bool
operator!= (const MXParticleData& Left, const MXParticleData& Right)
{
    return !( Left == Right );
}

inline bool
operator< (const MXParticleData& Left, const MXParticleData& Right)
{
    return Left.PID() < Right.PID();
}

inline bool
operator> (const MXParticleData& Left, const MXParticleData& Right)
{
    return Left.PID() > Right.PID();
}

inline bool
operator<=(const MXParticleData& Left, const MXParticleData& Right)
{
    return !(Left > Right);
}

inline bool
operator>=(const MXParticleData& Left, const MXParticleData& Right)
{
    return !(Left < Right);
}


}  // namespace Util

}  // namespace MXWare


#endif /* SOURCE_MXUTIL_MXPARTICLEDATA_HH_ */
