/**
 * \file MXParticleID.hh
 *
 *  Created on: May 28, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_MXPARTICLEID_HH_
#define SOURCE_MXUTIL_MXPARTICLEID_HH_

#include <stdint.h>
#include <iostream>

namespace MXWare
{

namespace Util
{

/**
 * Class that represents a particle id and provide several functions to infer the
 * quantum numbers of the particle, assuming the PID respects the Monte Carlo
 * numbering scheme defined by the PDG.
 * This is mainly extracted from the HepPDT::ParticleID class and simplified, reduced
 * and adapted to the need of the Magix framework
 *
 * The Particle ID can be created from a stream and from a configuration tree
 *
 * The particle ID is a signed 32-bit integer. Particles and anti-particles are represented
 * by integers of opposite signs
 * From the value of the integer it is possible to derive:
 * * Fundamentalness
 * * Which type of composite particle
 * * Quark content
 * * Atomic and mass number for the nuclei
 * * TODO Calculate the J S and L from the Id
 * * TODO Calculate the C P numbers ffrom the Id
 *
 * What is not defined by the ParticleID directly are the measured values associated
 * to each particle which are defined in a specific particle data structure which is
 * indexed through the PID:
 * * Mass
 * * Width
 * * Magnetic or electric moments
 * * Decay modes and branching ratios
 */
class MXParticleID
{
public:
    /**
     * Constructor
     * The PID is represented by an integer in which are encoded most of the
     * particle information and should be encoded according to the Monte Carlo
     * numbering scheme defined and documented by the PDG
     * @param m_Pid
     */
    MXParticleID (int32_t Id = 0);

    /// Destructor
    ~MXParticleID ( ) = default;

    ///Copy
    MXParticleID (const MXParticleID& other) = default;

    /// Assignment
    MXParticleID&
    operator= (const MXParticleID& other) = default;

    /// Move
    MXParticleID (MXParticleID&& other) = default;

    /// Move Assignment
    MXParticleID&
    operator= (MXParticleID&& other) = default;

    /// Conversion to integer
    explicit
    operator int32_t ( ) const;

    /// Equality
    friend bool
    operator== (const MXParticleID& Left, const MXParticleID& Right);

    /// Not Equality
    friend bool
    operator!= (const MXParticleID& Left, const MXParticleID& Right);

    ///Less than
    friend bool
    operator< (const MXParticleID& Left, const MXParticleID& Right);

    /// Less or equal
    friend bool
    operator<= (const MXParticleID& Left, const MXParticleID& Right);

    /// More than
    friend bool
    operator> (const MXParticleID& Left, const MXParticleID& Right);

    ///More or equal
    friend bool
    operator>= (const MXParticleID& Left, const MXParticleID& Right);

    /// Stream out
    friend std::ostream&
    operator<< (std::ostream& out, const MXParticleID& Id);

    /// Stream in
    friend std::istream&
    operator>> (std::istream& in, MXParticleID& Id);

    int32_t
    Pid ( ) const
    {
        return m_PID;
    }

    /// is this a valid ID?
    bool
    IsValid ( ) const;

    /// is this a valid meson ID?
    bool
    IsMeson ( ) const;

    /// is this a valid baryon ID?
    bool
    IsBaryon ( ) const;

    /// is this a valid hadron ID?
    bool
    IsHadron ( ) const;

    /// is this a valid lepton ID?
    bool
    IsLepton ( ) const;

    /// is this a valid ion ID?
    bool
    IsNucleus ( ) const;

    /// is this a fundamental particle id<100?
    bool
    IsFundamental ( ) const;

    /// does this particle contain an up quark?
    bool
    HasUp ( ) const;

    /// does this particle contain a down quark?
    bool
    HasDown ( ) const;

    /// does this particle contain a strange quark?
    bool
    HasStrange ( ) const;

    /// does this particle contain a charm quark?
    bool
    HasCharm ( ) const;

    /// does this particle contain a bottom quark?
    bool
    HasBottom ( ) const;

    /// does this particle contain a top quark?
    bool
    HasTop ( ) const;

    /// does this particle have an anti-particle different from itself?
    bool HasAntiparticle( ) const;

    /// jSpin returns 2J+1, where J is the total spin
    //int  jSpin( )        const;
    /// sSpin returns 2S+1, where S is the spin
    //int  sSpin( )        const;
    /// lSpin returns 2L+1, where L is the orbital angular momentum
    //int  lSpin( )        const;

    /// get the actual charge, which might be fractional
    double
    Charge ( ) const;

    /// if this is a nucleus (ion), get A
    int32_t
    A ( ) const;

    /// if this is a nucleus (ion), get Z
    int32_t
    Z ( ) const;

    /// if this is a nucleus (ion), get nLambda
    int32_t
    Lambda ( ) const;

    /* *******************************************************************
     * *******************************************************************
     * Static functions to generate the particle IDs of the most common particles
     *********************************************************************/
    /// Returns the electron particle ID
    static MXParticleID
    Electron();

    /// Returns the positron particle ID
    static MXParticleID
    Positron();

    /// Returns the dark photon particle ID
    static MXParticleID
    DarkPhoton();

    /// Returns the proton particle ID
    static MXParticleID
    Proton();

    /// Returns the Neutron Particle ID
    static MXParticleID
    Neutron();

    /// Returns the Photon Particle ID
    static MXParticleID
    Photon();

private:
    // Private methods
    /**
     * User-friendly definition of the locations of the digits
     * inside the pid as defined by the PDG.
     * NOTE the integer correspondence starts from 0
     */
    enum DigitLabel
    {
        nj = 0, //!< nj
        nq3,  //!< nq3
        nq2,  //!< nq2
        nq1,  //!< nq1
        nl,   //!< nl
        nr,   //!< nr
        n,    //!< n
        n8,   //!< n8
        n9,   //!< n9
        n10   //!< n10
    };

    /// returns everything beyond the 7th digit
    /// (e.g. outside the standard numbering scheme)
    int
    ExtraBits ( ) const;

    /**
     * Gets the digit at the required location in the Pid
     * @param Loc
     * @param Pid
     * @return
     */
    uint16_t
    Digit (const DigitLabel Loc) const;

    /**
     * Gets the final two digits of the PID which represent the identifier
     * for fundamental particle
     * @return
     */
    uint16_t
    FundamentalBits ( ) const;

    /**
     * Returns the absolute value of the Pid
     * @return
     */
    int32_t
    AbsPid ( ) const;

    /**
     * Returns the sign of the PID which also defines if the one defined
     * is a particle or an anti-particle
     * @return
     */
    int16_t
    Sign ( ) const;

private:
    // Data Members

    /// The particle ID
    int32_t m_PID;
};

inline
MXParticleID::operator int32_t ( ) const
{
    return m_PID;
}

inline bool
operator== (const MXParticleID& Left, const MXParticleID& Right)
{
    return ( Left.m_PID == Right.m_PID );
}

inline bool
operator!= (const MXParticleID& Left, const MXParticleID& Right)
{
    return !( Left == Right );
}

inline bool
operator< (const MXParticleID& Left, const MXParticleID& Right)
{
    return Left.m_PID < Right.m_PID;
}

inline bool
operator<= (const MXParticleID& Left, const MXParticleID& Right)
{
    return !( Left > Right );
}

inline bool
operator>= (const MXParticleID& Left, const MXParticleID& Right)
{
    return !( Left < Right );
}

inline bool
operator> (const MXParticleID& Left, const MXParticleID& Right)
{
    return Left.m_PID > Right.m_PID;
}

inline std::ostream&
operator<< (std::ostream& out, const MXParticleID& Id)
{
    out << Id.m_PID;
    return out;
}


}  // namespace Util

}  // namespace MXWare


#endif /* SOURCE_MXUTIL_MXPARTICLEID_HH_ */
