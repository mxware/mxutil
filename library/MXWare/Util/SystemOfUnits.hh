/**
 * \file SystemOfUnits.hh
 *
 * File containing the system of units of the framework.
 *
 * The MAGIX software framework utilizes the structure of the system of units
 * used in CLHEP and Geant. For each basic measurement unit there is one magnitude,
 * for example mm, defined as 1. while the subunits and superunits are defined as a
 * function as a multiplier of that unit. In this way the up and down conversions
 * can be done automatically and quite naturally. It is important, though, to
 * uniformly use the basic unit of measure for the quantities stored in the classes
 * and eventually convert them when necessary. To store a unit from a user magnitude
 * to the common base unit defined by the system it is sufficient to multiply the
 * value by the unit of measure. Conversely to retrieve the measurement in a custom
 * unit of value it is sufficient to divide it by that unit.
 *
 * For example if we are working with lengths, the base unit of the system is mm and
 * this variable is set to 1.0.
 * All the lengths stored in the classes should be stored and interpreted as being mm.
 * When one wants to store a length of 1 meter, it can simply store the quantity
 * (val = 1.0 * m). because m=1000 * mm, what is stored is 1000.0
 * If one wants to retrieve the same length in meters, it is necessary to divide by the
 * same unit so that L = val / m, which computes to 1.
 *
 *  Created on: Mar 19, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_SYSTEMOFUNITS_HH_
#define SOURCE_MXUTIL_SYSTEMOFUNITS_HH_



namespace MXWare
{
namespace Units
{

//
//
//
constexpr double     pi  = 3.14159265358979323846; ///< \f$ \pi \f$
constexpr double  twopi  = 2*pi; ///< \f$ 2 \pi \f$
constexpr double halfpi  = pi/2; ///< \f$ \pi / 2 \f$
constexpr double     pi2 = pi*pi; ///< \f$ \pi ^ 2 \f$

//
// Angle
//
constexpr double radian      = 1.; ///< radians
constexpr double milliradian = 1.e-3*radian; ///< millrad
constexpr double degree = (pi/180.0)*radian; ///< degree

constexpr double   steradian = 1.; ///< steradians

// symbols
constexpr double rad  = radian; ///< radians
constexpr double mrad = milliradian; ///< millirad
constexpr double sr   = steradian; ///< steradians
constexpr double deg  = degree; ///< degree

//
// Length [L]
//
constexpr double millimeter  = 1.; ///< mm
constexpr double millimeter2 = millimeter*millimeter; ///< \f$ mm^2 \f$
constexpr double millimeter3 = millimeter*millimeter*millimeter; ///< \f$ mm^3 \f$

constexpr double centimeter  = 10.*millimeter; ///< \f$ cm \f$
constexpr double centimeter2 = centimeter*centimeter; ///< \f$ cm^2 \f$
constexpr double centimeter3 = centimeter*centimeter*centimeter; ///< \f$ cm^3 \f$

constexpr double meter  = 1000.*millimeter; ///< \f$ m \f$
constexpr double meter2 = meter*meter; ///< \f$ m^2 \f$
constexpr double meter3 = meter*meter*meter; ///< \f$ m^3 \f$

constexpr double kilometer = 1000.*meter; ///< \f$ km \f$
constexpr double kilometer2 = kilometer*kilometer; ///< \f$ km^2 \f$
constexpr double kilometer3 = kilometer*kilometer*kilometer; ///< \f$ km^3 \f$

constexpr double parsec = 3.0856775807e+16*meter; ///< parsec

constexpr double micrometer = 1.e-6 *meter; ///< \f$ \mu \f$ m
constexpr double  nanometer = 1.e-9 *meter; ///< nm
constexpr double  angstrom  = 1.e-10*meter; ///< angstrom
constexpr double  fermi     = 1.e-15*meter; ///< fm

/*
 * Cross section
 */
constexpr double      barn = 1.e-28*meter2; ///< b
constexpr double millibarn = 1.e-3 *barn; ///< mb
constexpr double microbarn = 1.e-6 *barn; ///< \f$ \mu \f$ b
constexpr double  nanobarn = 1.e-9 *barn; ///< nb
constexpr double  picobarn = 1.e-12*barn; ///< pb

// symbols
constexpr double nm  = nanometer; ///< nm
constexpr double um  = micrometer; ///< \f$ \mu \f$ m

constexpr double mm  = millimeter; ///< mm
constexpr double mm2 = millimeter2; ///< \f$ mm^2 \f$
constexpr double mm3 = millimeter3; ///< \f$ mm^3 \f$

constexpr double cm  = centimeter; ///< \f$ cm \f$
constexpr double cm2 = centimeter2; ///< \f$ cm^2 \f$
constexpr double cm3 = centimeter3; ///< \f$ cm^3 \f$

constexpr double m  = meter; ///< \f$ m \f$
constexpr double m2 = meter2; ///< \f$ m^2 \f$
constexpr double m3 = meter3; ///< \f$ m^3 \f$

constexpr double km  = kilometer; ///< \f$ km \f$
constexpr double km2 = kilometer2; ///< \f$ km^2 \f$
constexpr double km3 = kilometer3; ///< \f$ km^3 \f$

constexpr double pc = parsec; ///< parsec

//
// Time [T]
//
constexpr double nanosecond  = 1.; ///< ns
constexpr double second      = 1.e+9 *nanosecond; ///< s
constexpr double millisecond = 1.e-3 *second; ///< ms
constexpr double microsecond = 1.e-6 *second; ///< \f$ \mu \f$ s
constexpr double  picosecond = 1.e-12*second; ///< ps

constexpr double hertz = 1./second; ///< Hz
constexpr double kilohertz = 1.e+3*hertz; ///< KHz
constexpr double megahertz = 1.e+6*hertz; ///< MHz

// symbols
constexpr double ns = nanosecond; ///< ns
constexpr double  s = second; ///< s
constexpr double ms = millisecond; ///< ms

//
// c   = 299.792458 mm/ns
// c^2 = 898.7404 (mm/ns)^2
//
constexpr double c_light   = 2.99792458e+8 * m/s; ///< Speed of light
constexpr double c_squared = c_light * c_light; ///< \f$ c^2 \f$

//
// Electric charge [Q]
//
constexpr double eplus = 1. ; ///< positron charge
constexpr double e_SI  = 1.602176487e-19; ///< positron charge in coulomb
constexpr double coulomb = eplus/e_SI; ///<  coulomb = 6.24150 e+18 * eplus

constexpr double electron_charge = - eplus; ///< Electron charge
constexpr double e_squared = eplus * eplus; ///< \f$ e^2 \f$

//
// Energy [E]
//
constexpr double megaelectronvolt = 1. ; ///< MeV
constexpr double     electronvolt = 1.e-6*megaelectronvolt; ///< eV
constexpr double kiloelectronvolt = 1.e-3*megaelectronvolt; ///< keV
constexpr double gigaelectronvolt = 1.e+3*megaelectronvolt; ///< GeV
constexpr double teraelectronvolt = 1.e+6*megaelectronvolt; ///< TeV
constexpr double petaelectronvolt = 1.e+9*megaelectronvolt; ///< PeV

constexpr double joule = electronvolt/e_SI; ///< joule = 6.24150 e+12 * MeV

// symbols
constexpr double MeV = megaelectronvolt; ///< MeV
constexpr double  eV = electronvolt; ///< eV
constexpr double keV = kiloelectronvolt; ///< keV
constexpr double GeV = gigaelectronvolt; ///< GeV
constexpr double TeV = teraelectronvolt; ///< TeV
constexpr double PeV = petaelectronvolt; ///< PeV

//
// h     = 4.13566e-12 MeV*ns
// hbar  = 6.58212e-13 MeV*ns
// hbarc = 197.32705e-12 MeV*mm
//
constexpr double h_Planck      = 6.62606896e-34 * joule*s; ///< Planck Constant
constexpr double hbar_Planck   = h_Planck/twopi; ///< \f$ \hbar \f$
constexpr double hbarc         = hbar_Planck * c_light; ///< \f$ \hbar c \f$
constexpr double hbarc_squared = hbarc * hbarc; ///< \f$ (\hbar c)^2 \f$

//
// Mass [E][T^2][L^-2]
//
constexpr double  kilogram = joule*second*second/(meter*meter); ///< kg
constexpr double      gram = 1.e-3*kilogram; ///< g
constexpr double milligram = 1.e-3*gram; ///< mg

//
// amu_c2 - atomic equivalent mass unit
//        - AKA, unified atomic mass unit (u)
// amu    - atomic mass unit
//
constexpr double electron_mass_c2 = 0.510998910 * MeV; ///< Electron mass (in MeV)
constexpr double   proton_mass_c2 = 938.272013 * MeV; ///< Proton mass (in MeV)
constexpr double  neutron_mass_c2 = 939.56536 * MeV; ///< Neutron mass (in MeV)
constexpr double           amu_c2 = 931.494028 * MeV; ///< Atomic mass unit (in MeV)
constexpr double              amu = amu_c2/c_squared; ///< Atomic mass unit (in SI units)

// symbols
constexpr double  kg = kilogram; ///< kg
constexpr double   g = gram; ///< g
constexpr double  mg = milligram; ///< mg

constexpr double universe_mean_density = 1.e-25*g/cm3; ///< Universe Mean Density

//
// Power [E][T^-1]
//
constexpr double watt = joule/second; ///< watt = 6.24150 e+3 * MeV/ns

//
// Force [E][L^-1]
//
constexpr double newton = joule/meter; ///< newton = 6.24150 e+9 * MeV/mm

//
// Pressure [E][L^-3]
//
#define pascal hep_pascal                           ///<  a trick to avoid warnings
constexpr double hep_pascal = newton/m2; ///< pascal = 6.24150 e+3 * MeV/mm3
constexpr double bar        = 100000.0*pascal; ///< bar    = 6.24150 e+8 * MeV/mm3
constexpr double mbar   = 1.e-3 * bar; ///< mbar = 6.24150 e+5 * MeV/mm3
constexpr double atmosphere = 101325.0*pascal; ///< atm    = 6.32420 e+8 * MeV/mm3
constexpr double torr = atmosphere / 760.0; ///< Torr =  133.3224 Pa

//
// Electric current [Q][T^-1]
//
constexpr double      ampere = coulomb/second; ///< ampere = 6.24150 e+9 * eplus/ns
constexpr double milliampere = 1.e-3*ampere; ///< mA
constexpr double microampere = 1.e-6*ampere; ///< \f$ \mu \f$ A
constexpr double  nanoampere = 1.e-9*ampere; ///< nA

//
// Electric potential [E][Q^-1]
//
constexpr double megavolt = megaelectronvolt/eplus;  ///< MV
constexpr double kilovolt = 1.e-3*megavolt; ///< kV
constexpr double     volt = 1.e-6*megavolt; ///< V

//
// Electric resistance [E][T][Q^-2]
//
constexpr double ohm = volt/ampere; ///< ohm = 1.60217e-16*(MeV/eplus)/(eplus/ns)

//
// Electric capacitance [Q^2][E^-1]
//
constexpr double farad = coulomb/volt; ///< farad = 6.24150e+24 * eplus/Megavolt
constexpr double millifarad = 1.e-3*farad; ///< mF
constexpr double microfarad = 1.e-6*farad; ///< \f$ \mu \f$ F
constexpr double  nanofarad = 1.e-9*farad; ///< nF
constexpr double  picofarad = 1.e-12*farad; ///< pF

//
// Magnetic Flux [T][E][Q^-1]
//
constexpr double weber = volt*second; ///< weber = 1000*megavolt*ns

//
// Magnetic Field [T][E][Q^-1][L^-2]
//
constexpr double tesla     = volt*second/meter2; ///< tesla =0.001*megavolt*ns/mm2

constexpr double gauss     = 1.e-4*tesla; ///< G
constexpr double kilogauss = 1.e-1*tesla; ///< kG

//
// Inductance [T^2][E][Q^-2]
//
constexpr double henry = weber/ampere; ///< henry = 1.60217e-7*MeV*(ns/eplus)**2

//
// permeability of free space mu0    = 2.01334e-16 Mev*(ns*eplus)^2/mm
// permittivity of free space epsil0 = 5.52636e+10 eplus^2/(MeV*mm)
//
constexpr double mu0      = 4*pi*1.e-7 * henry/m; ///< Permeability of free space
constexpr double epsilon0 = 1./(c_squared*mu0); ///< Permittivity of free space

//
// electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
//
constexpr double elm_coupling           = e_squared/(4*pi*epsilon0); ///< EM coupling
constexpr double fine_structure_const   = elm_coupling/hbarc; ///< fine structure constant
constexpr double classic_electr_radius  = elm_coupling/electron_mass_c2; ///< Classic electron radius
constexpr double electron_Compton_length = hbarc/electron_mass_c2; ///< Electron compton length
constexpr double Bohr_radius = electron_Compton_length/fine_structure_const; ///< Bohr radius

constexpr double alpha_rcl2 = fine_structure_const
                                   *classic_electr_radius
                                   *classic_electr_radius; ///< \f$ \alpha R_e^2 \f$

constexpr double twopi_mc2_rcl2 = twopi*electron_mass_c2
                                             *classic_electr_radius
                                             *classic_electr_radius; ///< \f$ 2 \pi M_e c^2 *R_e^2 \f$

//
// Magnetic moments
//
constexpr double nuclear_magneton = eplus * hbar_Planck
                                    / 2.0 / proton_mass_c2 * c_squared; ///< Nuclear magneton
constexpr double proton_mu = 2.79284739 * nuclear_magneton; ///< proton magnetic moment
constexpr double neutron_mu = -1.91304272 * nuclear_magneton; ///< Neutron magnetic moment

//
// Temperature
//
constexpr double kelvin = 1.; ///< K
constexpr double k_Boltzmann = 8.617343e-11 * MeV/kelvin; ///< Boltzmann constant

constexpr double STP_Temperature = 273.15*kelvin; ///< STP temperature
constexpr double STP_Pressure    = 1.*atmosphere; ///< STP Pressure
constexpr double kGasThreshold   = 10.*mg/cm3; ///< Threshold Gas Density

//
// Amount of substance
//
constexpr double mole = 1.; ///< Mole
constexpr double Avogadro = 6.02214179e+23/mole; ///< Avogadro constant

//
// Activity [T^-1]
//
constexpr double becquerel = 1./second ;  ///< Bq
constexpr double curie = 3.7e+10 * becquerel; ///< Ci
constexpr double kilobecquerel = 1.e+3*becquerel; ///< kBq
constexpr double megabecquerel = 1.e+6*becquerel; ///< MBq
constexpr double gigabecquerel = 1.e+9*becquerel; ///< GBq
constexpr double millicurie = 1.e-3*curie; ///< mCi
constexpr double microcurie = 1.e-6*curie; ///< \f$ \mu \f$ Ci
constexpr double Bq = becquerel; ///< Bq
constexpr double kBq = kilobecquerel; ///< kBq
constexpr double MBq = megabecquerel; ///< MBq
constexpr double GBq = gigabecquerel; ///< GBq
constexpr double Ci = curie; ///< Ci
constexpr double mCi = millicurie; ///< mCi
constexpr double uCi = microcurie; ///< \f$ \mu \f$ Ci

//
// Absorbed dose [L^2][T^-2]
//
constexpr double      gray = joule/kilogram ; ///< Gr
constexpr double  kilogray = 1.e+3*gray; ///<  kGr
constexpr double milligray = 1.e-3*gray; ///< mGr
constexpr double microgray = 1.e-6*gray; ///< \f$ \mu \f$ Gr

//
// Luminous intensity [I]
//
constexpr double candela = 1.; ///< cand

//
// Luminous flux [I]
//
constexpr double lumen = candela*steradian; ///< lumen

//
// Illuminance [I][L^-2]
//
constexpr double lux = lumen/meter2; ///< lux

//
// Miscellaneous
//
constexpr double perCent     = 0.01 ; ///< perCent
constexpr double perThousand = 0.001; ///< perMille
constexpr double perMillion  = 0.000001; ///< perMillion





}  // namespace Units


}  // namespace MXWare


#endif /* SOURCE_MXUTIL_SYSTEMOFUNITS_HH_ */
