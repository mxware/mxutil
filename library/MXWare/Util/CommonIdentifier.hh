/*
 * CommonIdentifier.hh
 *
 *  Created on: Mar 1, 2019
 *      Author: caiazza
 */

#ifndef LIBRARY_MXWARE_UTIL_COMMONIDENTIFIER_HH_
#define LIBRARY_MXWARE_UTIL_COMMONIDENTIFIER_HH_

#include <utility>
#include <stdint.h>
#include <exception>

#include "MXWare/Core/MXLogger.hh"

namespace MXWare
{

namespace Util
{
/// This class represents the common integral identifier which should be used throughout MXWare to
/// provide a discrete identifier for an object. Typically this should be used to identify electronic
/// channels, detector or readout elements and so on. The necessity of a common identifier type arises
/// from the need to possibly remap objects from one representation to another. For example data from
/// an electronic channel should be often remapped to a readout element during a reconstruction or
/// vice versa during a detector simulation.
/// The identifiers satisfy the requirements of EqualityComparable and LessThanComparable. Two undefined
/// identifiers always compare equale to each other. Each undefined identifier is always considered larger
/// than any defined identifier
class CommonIdentifier
{
public:
    ///@{
    ///  Alias for the
    using id_0 = uint32_t;
    using group_t = id_0;
    using id_1 = uint32_t;
    using element_t = uint32_t;
    ///@}

    /// Creates an undefined identifier
    constexpr CommonIdentifier():
        m_Id{s_UndefinedId},
        m_Undefined{true}
    {    }

    ///@{
    /// Pair Creates an identifier from a pair of U32
    constexpr CommonIdentifier(std::pair<id_0, id_1> Pair):
        m_Id{Pair},
        m_Undefined{false}
    {    }

    constexpr CommonIdentifier(id_0 Group, id_1 Element):
        m_Id{Group, Element},
        m_Undefined{false}
    {    }

    constexpr CommonIdentifier(uint64_t ID):
        m_Id{ID >> 32, ID% (1ULL << 32)},
        m_Undefined{false}
    {    }

    /// Checks whether the identifier is undefined
    bool
    IsUndefined() const noexcept
    {
        return m_Undefined;
    }

    /// Returns true if the identifier is defined, false otherwise
    operator bool() const noexcept
    {
        return !m_Undefined;
    }

    /// Converts the identifier to a pair of U32
    explicit operator std::pair<id_0,id_1>() const noexcept
    {
        return m_Id;
    }

    explicit operator uint64_t() const noexcept
    {
        return (static_cast<uint64_t>(m_Id.first) << 32) + m_Id.second;
    }

    ///@{
    /// Retrieves the subidentifiers of the object in the STL compatible way that is used for pair and tuples
    /// Note that if this operation is applied to an undefined identifier the result is not usable
    template<std::size_t I>
    constexpr uint32_t&
    get() noexcept
    {
        static_assert((I < 2),
                      "The CommonIdentifier contains only 2 accessible subelements");
        return std::get<I>(m_Id);
    }

    template<std::size_t I>
    constexpr const uint32_t&
    get() const noexcept
    {
        static_assert((I < 2),
                      "The CommonIdentifier contains only 2 accessible subelements");
        return std::get<I>(m_Id);
    }
    ///@}

    /// Sets an identifier as undefined
    void
    SetUndefined() noexcept
    {
        m_Undefined = true;
        m_Id = s_UndefinedId;
    }

    ///@{
    /// Sets the identifier to a defined value
    void
    SetId(id_0 Group, id_1 Element) noexcept
    {
        m_Id = std::make_pair(Group, Element);
        m_Undefined = false;
    }

    void
    SetId(std::pair<id_0, id_1> Id) noexcept
    {
        m_Id = Id;
        m_Undefined = false;
    }

    void
    SetId(uint64_t Id) noexcept
    {
        m_Id.second = Id% (1ULL << 32);
        m_Id.first = Id >> 32;
        m_Undefined = false;
    }
    ///@}


private:
    /// The
    std::pair<id_0, id_1> m_Id;

    /// Signals that the DAQ id is actually undefined
    bool m_Undefined;

    /// The identifier which will be stored when the object is undefined
    /// Note that this identifier is not invalid, it's just an implementation detail.
    /// The only way to check if the identifier is undefined or not is to check the Undefined flag
    static constexpr decltype(m_Id) s_UndefinedId {std::numeric_limits<uint32_t>::max(),
                                                   std::numeric_limits<uint32_t>::max()};

    /// Comparison
    /// Two undefined ids are always equal to each other
    friend constexpr bool
    operator==(const CommonIdentifier& lhs, const CommonIdentifier& rhs)
    {
        if(lhs.m_Undefined)
        {
            return rhs.m_Undefined;
        }
        else
        {
            if(rhs.m_Undefined)
            {
                return false;
            }
            else
            {
                return lhs.m_Id == rhs.m_Id;
            }
        }
    }

    /// Inequality
    friend constexpr bool
    operator!=(const CommonIdentifier& lhs, const CommonIdentifier& rhs)
    {
        return !(lhs == rhs);
    }

    ///@{
    /// lexicographical strict ordering. Only the < operator is defined explicitly.
    /// Other relational operations are defined in terms of this one.
    /// This definition guarantees that an undefined identifier is ordered as less than any defined
    /// identifier and equivalent to any other undefined identifier.
    friend constexpr bool
    operator<(const CommonIdentifier& lhs, const CommonIdentifier& rhs)
    {
        return (rhs.m_Undefined) ? false : (lhs.m_Undefined) ? true : lhs.m_Id < rhs.m_Id ;
    }

    friend constexpr bool
    operator>(const CommonIdentifier& lhs, const CommonIdentifier& rhs)
    {
        return rhs < lhs;
    }

    friend constexpr bool
    operator>=(const CommonIdentifier& lhs, const CommonIdentifier& rhs)
    {
        return !(lhs < rhs);
    }

    friend constexpr bool
    operator<=(const CommonIdentifier& lhs, const CommonIdentifier& rhs)
    {
        return !(rhs < lhs);
    }
    ///@}


    /// The character separating the channel and the group elements in the input stream
    static const char s_GroupSeparator = '|';

    /// Out Streaming
    friend std::ostream&
    operator<<(std::ostream& out, const CommonIdentifier& Id)
    {
        if(Id.m_Undefined)
        {
            out << '?' << CommonIdentifier::s_GroupSeparator<< '?';
        }
        else
        {
            out << Id.get<0>() << CommonIdentifier::s_GroupSeparator << Id.get<1>();
        }
        return out;
    }

    /// Input streaming
    friend std::istream&
    operator>>(std::istream& in, CommonIdentifier& Id)
    {
        std::string tmp;
        in >> tmp;

        // If there is a question mark I take it as an undefined identifier
        if(tmp.find('?') != tmp.npos)
        {
            Id = CommonIdentifier{};
        }
        else
        {
            auto SeparatorPos = tmp.find_first_of(CommonIdentifier::s_GroupSeparator);
            if(SeparatorPos == tmp.npos)
            {
                // If there is no separator I attempt to decode it as a U64
                try
                {
                    Id.SetId(std::stoull(tmp));
                }
                catch(std::invalid_argument&)
                {
                    MAGIX_LOG_WARNING << "Invalid string use to represent an object of the CommonIdentifier class."
                        " Returning an undefined identifier. Offending string: " << tmp;
                    Id = CommonIdentifier{};
                }
                catch(std::out_of_range& )
                {
                    MAGIX_LOG_WARNING << "The value of the string used to represent an object of the CommonIdentifier "
                        "class does not represent a number representable by a unsigned long. "
                        "Returning an undefined identifier. Offending string: " << tmp;
                    Id = CommonIdentifier{};
                }
            }
            else
            {
                try
                {
                    auto id0 = std::stoul(tmp.substr(0, SeparatorPos));
                    auto id1 = std::stoul(tmp.substr(SeparatorPos+1));
                    Id.SetId(id0, id1);
                }
                catch(std::invalid_argument&)
                {
                    MAGIX_LOG_WARNING << "Invalid string use to represent an object of the CommonIdentifier class."
                        " Returning an undefined identifier. Offending string: " << tmp;
                    Id = CommonIdentifier{};
                }
                catch(std::out_of_range& )
                {
                    MAGIX_LOG_WARNING << "The value of the string used to represent an object of the CommonIdentifier "
                        "class does not represent a number representable by a unsigned long. "
                        "Returning an undefined identifier. Offending string: " << tmp;
                    Id = CommonIdentifier{};
                }
            }
        }
        return in;
    }



};

}  // namespace Util

}  // namespace MXWare


namespace std
{
template<>
struct hash<MXWare::Util::CommonIdentifier>
{
    std::size_t
    operator()(const MXWare::Util::CommonIdentifier& Id) const
    {
        std::hash<uint64_t> Hasher;
        return Hasher(static_cast<uint64_t>(Id));
    }

};


}  // namespace std


#endif /* LIBRARY_MXWARE_UTIL_COMMONIDENTIFIER_HH_ */
