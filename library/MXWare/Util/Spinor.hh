/**
 * \file Spinor.hh
 *
 *  Created on: Jun 8, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_SPINOR_HH_
#define SOURCE_MXUTIL_SPINOR_HH_

#include <complex>
typedef std::complex<double> Complex;

#include <array>
#include <cmath>
using std::conj;

#include <iostream>

#include "MXWare/Util/MXUtil.hh"

namespace MXWare
{
namespace Util
{
/**
 * Representation of a spinor
 * TODO Think about making two classes for Dirac and Weyl spinors
 * TODO Develop the serialization of the spinor to a ConfTree (add the istream input)
 * TODO Spinor/Tensor multiplication
 */
class Spinor
{
public:
    /// Constructor
    Spinor (const Complex& s0 = 0, const Complex& s1 = 0, const Complex& s2 = 0, const Complex& s3 = 0);

    /**
     * Creates a spinor from the 4momentum.
     * The spin of the particle along the third component of the momentum is
     * defined by the second parameter with true meaning that the particle has
     * positive helicity.
     * The third component allows to specify if one wants to create a particle
     * or an antiparticle
     * @param P
     * @param Up
     * @param Part
     */
    Spinor (const LorentzVector& P, bool Up, bool Part = true);

    /// Construct from an array
    explicit
    Spinor (const std::array<Complex, 4>& Array);

    /// Move constructor from an externally provided array
    explicit
    Spinor (std::array<Complex, 4>&& Array);

    /// Destructor
    ~Spinor ( ) = default;

    ///Copy
    Spinor (const Spinor& other) = default;

    ///Assignment
    Spinor&
    operator= (const Spinor& other) = default;

    /// Move
    Spinor (Spinor&& other) = default;

    /// Move assignment
    Spinor&
    operator= (Spinor&& other) = default;

    /// Subscript operator access to the spinor directly (no bound checking)
    Complex&
    operator[] (size_t i);

    /// Substrict operator access with const return (no bound checking)
    const Complex&
    operator[] (size_t i) const;

    /// Creates the adjoint spinor
    Spinor
    operator! ( ) const;

    /// Addition
    Spinor&
    operator+= (const Spinor& S);

    /// Subtraction
    Spinor&
    operator-= (const Spinor& S);

    /// Multiplication
    Spinor&
    operator*= (const Complex& C);

    /// Division
    Spinor&
    operator/= (const Complex & C);

private:
    /// The components of the spinor
    std::array<Complex, 4> m_Comp;
};

/* ******************************************************
 *      Inline member functions
 ******************************************************* */

inline Complex&
Spinor::operator[] (const size_t i)
{
    return m_Comp[i];
}

inline const Complex&
Spinor::operator[] (const size_t i) const
{
    return m_Comp[i];
}

inline Spinor
Spinor::operator! ( ) const
{
    return Spinor(conj(m_Comp[0]), conj(m_Comp[1]), -conj(m_Comp[2]), -conj(m_Comp[3]));
}

inline Spinor&
Spinor::operator+= (const Spinor& S)
{
    m_Comp[0] += S[0];
    m_Comp[1] += S[1];
    m_Comp[2] += S[2];
    m_Comp[3] += S[3];
    return *this;
}

inline Spinor&
Spinor::operator-= (const Spinor& S)
{
    m_Comp[0] -= S[0];
    m_Comp[1] -= S[1];
    m_Comp[2] -= S[2];
    m_Comp[3] -= S[3];
    return *this;
}

inline Spinor&
Spinor::operator*= (const Complex& C)
{
    m_Comp[0] *= C;
    m_Comp[1] *= C;
    m_Comp[2] *= C;
    m_Comp[3] *= C;
    return *this;
}

inline Spinor&
Spinor::operator/= (const Complex & C)
{
    m_Comp[0] /= C;
    m_Comp[1] /= C;
    m_Comp[2] /= C;
    m_Comp[3] /= C;
    return *this;
}

// TODO Tensor multiplication

/* ******************************************************
 *      Non member operators
 ******************************************************** */

/// Spinor moltiplication
inline Complex
operator* (const Spinor& L, const Spinor& R)
{
    return L[0] * R[0] + L[1] * R[1] + L[2] * R[2] + L[3] * R[3];
}

/// Spinor complex multiplication
inline Spinor
operator* (Spinor S, const Complex& K)
{
    return S *= K;
}

/// Complex spinor multiplication
inline Spinor
operator* (const Complex& K, Spinor S)
{
    return S *= K;
}

inline Spinor
operator/ (Spinor S, const Complex& K)
{
    return S /= K;
}

/// Addition of two spinors (NOTE The first one is by value but is also returned)
inline Spinor
operator+ (Spinor L, const Spinor&R)
{
    return L += R;
}

/// Subtraction of two spinors (NOTE The first one is by value but is also returned)
inline Spinor
operator- (Spinor L, const Spinor&R)
{
    return L -= R;
}

/// Output to stream
inline std::ostream&
operator<< (std::ostream & os, const Spinor & S)
{
    return os << "(" << S[0] << "," << S[1] << "," << S[2] << "," << S[3] << ")";
}

/// Comparison
inline bool
operator== (const Spinor& L, const Spinor&R)
{
    return ( L[0] == R[0] && L[1] == R[1] && L[2] == R[2] && L[3] == R[3] );
}

/// Inequality
inline bool
operator!= (const Spinor& L, const Spinor&R)
{
    return !( L == R );
}

}  // namespace Util

}  // namespace MXWare

#endif /* SOURCE_MXUTIL_SPINOR_HH_ */
