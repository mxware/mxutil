/**
 * \file MXUtil.hh
 *
 * File containing the global functions, typedefs and variables of the MXUtil package
 *
 *  Created on: Aug 11, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_MXUTIL_HH_
#define SOURCE_MXUTIL_MXUTIL_HH_

#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/TwoVector.h"

#include "MXWare/Util/MXParticleID.hh"

#include <memory>
#include <iostream>
#include <boost/property_tree/ptree.hpp>

#include "MXWare/Core/MXExceptions.hh"
namespace pt = boost::property_tree;


/**
 * \namespace MXUtil
 * Namespace for the common utilities of the Magix software framework.
 */
namespace MXWare
{
namespace Util
{

/**
 * Type definition of the Lorentz vector used in the software
 * When streamed in and out the format of this vector is:
 * (x,y,z;t)
 * Note the ; character before the t component and the parentheses
 */
//typedef CLHEP::HepLorentzVector FourVec;
typedef CLHEP::HepLorentzVector LorentzVector;
//typedef FourVec FourMomentum;

/**
 * Type definition of the 3-vectors, based on the CLHEP one
 * When streamed in and out the format of the vector is:
 * (x,y,z)
 * note the parentheses
 */
typedef CLHEP::Hep3Vector   ThreeVec;
typedef ThreeVec            Momentum;
typedef ThreeVec            PolarizationVector;
typedef ThreeVec            Position3D;
//typedef ThreeVec Vertex;

/**
 * Type definition of the 2-vectors, based on the CLHEP one
 * When streamed in and out the format of the vector is:
 * (x,y)
 * note the parentheses
 */
typedef CLHEP::Hep2Vector   TwoVec;
typedef TwoVec              Position2D;

//typedef MXParticleID ParticleID;

void
PrintPhysicalConstants(std::ostream& out);

}  // namespace Util

}  // namespace MXWare

#endif /* SOURCE_MXUTIL_MXUTIL_HH_ */
