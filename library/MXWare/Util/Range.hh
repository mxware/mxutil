/**
 * \file Range.hh
 *
 *  Created on: Apr 29, 2015
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_RANGE_HH_
#define SOURCE_MXUTIL_RANGE_HH_

#include <utility>
#include <iostream>
#include <sstream>
#include <string>
#include "MXWare/Core/MXExceptions.hh"
#include "MXWare/Core/MXLogger.hh"
#include <boost/current_function.hpp>
#include <algorithm>

namespace MXWare
{
namespace Util
{

/// Forward declaration of the RangeSet class for the friendship declaration
template<typename T, class Compare>
class RangeSet;

/**
 * A template class representing a continuous range  of values.
 * The fundamental requirement is for the template argument to have an ordering function
 * The range can be created from two values that are automatically ordered to represent
 * the minimum and the maximum and then extended, if necessary , with a new value that if
 * not in the range will extend it towards the minimum or the maximum
 * The range can be read from a stream with the following format [min:max]
 * Note that if the minimum value is larger than the maximum the class will automatically
 * rearrange them without warning.
 */
template<typename T, class Compare = std::less<T> >
    class Range
    {

    public:
        /**
         * Default constructor
         * Range of 0 centered at the default value of the parameter
         */
        Range ( ) :
            Range { T {}, T {} }
        {

        }

        /**
         * Constexpr version of the min max constructor
         * @param Min
         * @param Max
         */
        Range (const T& Min, const T& Max):
            min{Compare{}(Min, Max)? Min : Max},
            max{Compare{}(Min, Max)? Max : Min}
        {

        }

        /**
         * Constexpr version of the pair range constructor
         * @param Pair
         */
        Range(const std::pair<T, T>& Pair):
            Range(Pair.first, Pair.second)
        {

        }

        /**
         * Emplace conversion of a pair
         * @param Pair
         */
        Range (std::pair<T, T> && Pair) :
            Range { Pair }
        {

        }

        /// Destructor
        ~Range ( ) = default;

        /// Copy
        Range (const Range& other) = default;

        /// Assignment
        Range&
        operator= (const Range& other) = default;

        /// Move
        Range (Range&& other) = default;

        /// Move assignment
        Range&
        operator= (Range&& other) = default;

        /**
         * Access the minimum value
         * @return
         */
        const T&
        Min ( ) const
        {
            return min;
        }

        /**
         * Access the maximum value
         * @return
         */
        const T&
        Max ( ) const
        {
            return max;
        }

        /**
         * Sets the range being between the Min and the Max.
         * If Min > Max throws an exception
         */
        void
        Set (const T& Min, const T& Max)
        {
            min = std::min(Min, Max, Compare{});
            max = std::max(Min, Max, Compare{});
/*            constexpr Compare Less_than{};
            if (Less_than(Min, Max))
            {
                min = Min;
                max = Max;
            }
            else
            {
                min = Max;
                max = Min;
            }*/
        }

        /**
         * If the new value is already included in the range nothing happens otherwise the
         * range is extended to include the new value
         * @param NewValue
         */
        void
        Extend (const T& NewValue)
        {
            constexpr Compare Less_than{};

            if (Less_than(NewValue, min))
            {
                min = NewValue;
            }
            else if (Less_than(max, NewValue))
            {
                max = NewValue;
            }
        }

        /**
         * Checks if the required value is within the range specified by the class.
         * The max is considered out of the range
         */
        bool
        InRange (const T& Value) const
        {
            constexpr Compare Less_than{};
            if (min != max)
                return ( !Less_than(Value, min) && Less_than(Value, max));
            else
                return ( Value == min );
        }

        /**
         * Returns the distance between the minimum and the maximum.
         * It assumes that the subtraction operation is closed in the mathematical sense.
         * Because T can be something else than a standard numerical type it's the user
         * responsibility to check that the difference do not overflows the numerical
         * representation
         */
        auto
        Length ( ) const ->decltype(std::declval<T>()-std::declval<T>())
        {
            return ( max - min );
        }

        /// Checks whether to ranges overlap each other
        bool
        Overlap(const Range<T, Compare>& other) const
        {
            constexpr Compare Less_than{};
            return ((Less_than(min, other.max)) &&
                (Less_than(other.min, max)));
        }

        /**
         * Prints the range to an output stream
         */
        void
        print (std::ostream& os) const
        {
            os << "[" << min << ":" << max << "]";
        }

        /**
         * Logical union of two overlapping ranges
         * If the two ranges do not overlap they cannot be united
         * @param rhs
         * @return
         */
        Range<T, Compare>&
        operator|=(const Range<T, Compare>& rhs)
        {
            if(!Overlap(rhs) && rhs.min != max && rhs.max != min)
            {
                std::stringstream err;
                err << "The logical OR is only allowed on overlapping or touching ranges";
                BOOST_LOG_TRIVIAL(error) << err.str();
                Core::InvalidParameter e{"Range", BOOST_CURRENT_FUNCTION, err.str()};
                e.raise();
            }

            Extend(rhs.Max());
            Extend(rhs.Min());
            return *this;
        }

        /**
         * Logical intersection of two overlapping ranges.
         * The operation is only valid for two overlapping ranges and wll otherwise throw
         * an exception
         * @param rhs
         * @return
         */
        Range<T, Compare>&
        operator&=(const Range<T, Compare>& rhs)
        {
            if(!Overlap(rhs))
            {
                std::stringstream err;
                err << "The logical AND is only allowed on overlapping ranges";
                BOOST_LOG_TRIVIAL(error) << err.str();
                Core::InvalidParameter e{"Range", BOOST_CURRENT_FUNCTION, err.str()};
                e.raise();
            }
            min = std::max(min, rhs.min);
            max = std::min(max, rhs.max);
            return * this;
        }

        /**
         * Adding a constant
         * @param Second
         * @return
         */
        Range<T, Compare>&
        operator+= (const T& Second)
        {
            Set(min + Second, max + Second);
            return *this;
        }

        /**
         * Adding a range to another.
         * It will extend it including even the eventual gap between the ranges
         * @param Second
         * @return
         */
/*        Range&
        operator+= (const Range<T>& second)
        {
            Extend(second.Max());
            Extend(second.Min());
            return *this;
        }*/

        /**
         * Subtracting a constant
         * @param Second
         * @return
         */
        Range<T, Compare>&
        operator-= (const T& Second)
        {
            Set(min - Second, max - Second);
            return *this;
        }

        // The division and multiplication can be ambiguous. better not define them
        /**
         * Multiplying by a constant
         * @param Second
         * @return
         */
        Range<T, Compare>&
        operator*= (const T& Second)
        {
            Set(min * Second, max * Second);
            return *this;
        }

        /**
         * Dividing by a constant
         * @param Second
         * @return
         */
        Range<T, Compare>&
        operator/= (const T& Second)
        {
            Set(min / Second, max / Second);
            return *this;
        }

    private:

        T min; ///< The minimum of the range
        T max; ///< The maximum of the range


        /**
         * Comparison operators
         * @param Left
         * @param Right
         * @return
         */
        friend bool
        operator== (const Range<T, Compare>& Left, const Range<T, Compare>& Right)
        {
            return ( Left.min == Right.min && Left.max == Right.max );
        }

        /**
         * Inequality operator
         * @param Left
         * @param Right
         * @return
         */
        friend bool
        operator!= (const Range<T, Compare>& Left, const Range<T, Compare>& Right)
        {
            return !( Left == Right );
        }

        /**
         * Arithmetic operators
         * Addition
         * Notice the pass-by-value which creates the tmp to be added to
         * @param First
         * @param Second
         * @return
         */
         friend Range<T, Compare>
         operator+ (Range<T, Compare> First, const T& Second)
         {
             return First += Second;
         }

         /// Adding two ranges to each other. Any gap between them will be filled
//         friend Range<T, Compare>
//         operator+ (Range<T, Compare> First, const Range<T, Compare> Second)
//         {
//             return First += Second;
//         }

         /// Logic AND operator for ranges.
         /// It can only be applied to overlapping ranges as it will otherwise throw an exception
         /// If the ranges are overlapping the result is a new range extending the range to include
         /// both the operands
         /// Notice the pass-by-value which creates the tmp to be added to
         friend Range<T, Compare>
         operator&&(Range<T, Compare> lhs, const Range<T, Compare> rhs)
         {
             return lhs &= rhs;
         }

         /**
          * Logical OR operator for ranges
          * It can only be applied to overlapping ranges as it will otherwise throw an exception
          * If the range are overlapping it results in the logical intersection of the operands
          * Notice the pass-by-value which creates the tmp to be added to
          * @param lhs
          * @param rhs
          * @return
          */
         friend Range<T, Compare>
         operator||(Range<T, Compare> lhs, const Range<T, Compare> rhs)
         {
             return lhs |= rhs;
         }

         /**
          * Subtraction
          * Notice the pass-by-value which creates the tmp to be added to
          * @param First
          * @param Second
          * @return
          */
         friend Range<T, Compare>
         operator- (Range<T, Compare> First, const T& Second)
         {
             return First -= Second;
         }

         /**
          * Multiplication
          * Notice the pass-by-value which creates the tmp to be added to
          * @param First
          * @param Second
          * @return
          */
         friend Range<T, Compare>
         operator* (Range<T, Compare> First, const T& Second)
         {
             return First *= Second;
         }

         /**
          * Division
          * Notice the pass-by-value which creates the tmp to be added to
          * @param First
          * @param Second
          * @return
          */
         friend Range<T, Compare>
         operator/ (Range<T, Compare> First, const T& Second)
         {
             return First /= Second;
         }

         /**
          * Stream out operators
          * @param out
          * @param R
          * @return
          */
         friend std::ostream&
         operator<< (std::ostream& out, const Range<T, Compare>& R)
         {
             R.print(out);
             return out;
         }

         friend std::istream&
         operator>> (std::istream& in, Range<T, Compare>& R)
         {
             std::string tmp;
             in >> tmp;

             std::stringstream Tokenized;
             auto Begin = tmp.find_first_of('[');
             auto SeparatorPos = tmp.find_first_of(':', Begin);
             auto End = tmp.find_first_of(']', SeparatorPos);
             if(Begin == tmp.npos || End == tmp.npos ||
                 SeparatorPos == tmp.npos)
             {
                 Core::StreamingError e{"Invalid string encoding the range: " + tmp};
                 e.raise();
             }

             Tokenized << tmp.substr(Begin + 1, SeparatorPos - Begin - 1) << '\t' <<
                 tmp.substr(SeparatorPos + 1, End - SeparatorPos - 1);

             T TempMin;
             T TempMax;

             Tokenized >> TempMin >> TempMax;
             R.Set(TempMin, TempMax);

             return in;
         }

         friend class RangeSet<T, Compare>;

    };


/**
 * Redefinition of the most common usage of this template
 */
typedef Range<double> RangeD;

/**
 * Extracts a value within the range proportional to the fraction
 * Only defined for real types
 * @param Fraction
 * @return
 */
template<typename T, class Compare = std::less<T> >
typename std::enable_if<std::is_floating_point<T>::value, T>::type
ProportionalExtract(const Range<T, Compare>& Range, const T& Fraction)
{
    if(Fraction < 0.0 || Fraction >1.0)
    {
        std::stringstream err;
        err << "The extraction fraction should be a number included between 0 and 1 "
            "and is " << Fraction;
        BOOST_LOG_TRIVIAL(error) << err.str();
        Core::InvalidParameter e{"", BOOST_CURRENT_FUNCTION, err.str()};
        e.raise();
    }

    return Range.Min() + Fraction * Range.Length();
}

}  // namespace Util

}  // namespace MXWare

#endif /* SOURCE_MXUTIL_RANGE_HH_ */
