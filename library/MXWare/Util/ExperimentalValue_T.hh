/*
 * ExperimentalValue_T.hh
 *
 *  Created on: Mar 2, 2017
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_EXPERIMENTALVALUE_T_HH_
#define SOURCE_MXUTIL_EXPERIMENTALVALUE_T_HH_

#include <utility>
#include <iostream>
#include <string>
#include <sstream>

#include <MXWare/Core/MXExceptions.hh>

namespace MXWare
{

namespace Util
{

/**
 * Definition of the generic template with the second template arguments which allows
 * to define the class only for arithmentic types
 */
template<typename T, typename Enable = void>
class ExperimentalValue_T;

/**
 * Template class to package an experimental value with its error.
 * The errors can be asymmetric and are of the same type of the value itself.
 * The template is only defined for arithmetic types
 *
 * The class can be streamed out and in. For the input streaming the input
 * streaming has to comply with the following template: value[neg_err:pos_err]
 *
 * TODO Define the arithmetic operators for the class including the direct and quadrature
 * sum, and relative sum of the errors so that the basic error propagation can be done
 * directly with such a class
 */
template<typename T>
class ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>
{
public:

    /**
     * Default constructor.
     * The object is built with the default constructor of T
     */
    ExperimentalValue_T();

    /**
     * Full constructor which sets the value and errors independently
     * @param Value
     * @param NegError
     * @param PosError
     */
    ExperimentalValue_T(T Value, T NegError, T PosError);

    /**
     * Full constructor for symmetrical errors
     * @param Value
     * @param Error
     */
    ExperimentalValue_T(T Value, T Error);

    /**
     * Full constructor which sets the Value and the errors
     * @param Value The value of the measure
     * @param Errors A pair containing the negative and positive errors in this order
     */
    ExperimentalValue_T(T Value, std::pair<T, T> Errors);

    /// Gets the measurement value
    T
    GetValue ( ) const;

    /// Sets the measurement value
    void
    SetValue (T value);

    /// Gets the negative error
    T
    GetNegativeError ( ) const;

    /// Sets the negative error
    void
    SetNegativeError (T negativeError);

    /// Gets the positive error
    T
    GetPositiveError ( ) const;

    /// Sets the positive error
    void
    SetPositiveError (T positiveError);

    /// Gets a single error value
    /// If the errors are not symmetrical it will calculate an average
    T
    GetError() const;

    /// Sets the measurement error and defines that the errors are symmetrical
    void
    SetError(T Error);

    /// Gets the measurement error packaged as a pair of negative:positive error
    std::pair<T, T>
    GetErrors() const;

    /// Sets the errors one at the time.
    /// It implies non symmetrical errors even if the value are numerically the same
    void
    SetErrors(T NegError, T PosError);

    /// Sets the error as a pair.
    /// The first value is assumed to be the negative error
    /// It implies non symmetrical errors even if the value are numerically the same
    void
    SetErrors(std::pair<T, T> Errors);

    /// Direct conversion to the template type
    /// It will give back the measurement value, without errors
    operator T();

    /// Queries whether it has symmetric errors
    bool
    HasSymmetricErrors ( ) const;

    /// Forces the symmetric error switch
    void
    ForceSymmetricErrors (bool symmetricErrors);

    /// Stream out
    friend std::ostream&
    operator<< (std::ostream& out, const ExperimentalValue_T<T>& EV)
    {
        out << EV.m_Value << '[';
        if(EV.m_SymmetricErrors)
        {
            out << EV.m_PositiveError;
        }
        else
        {
            out << EV.m_NegativeError << ":" << EV.m_PositiveError;
        }

        out << ']';
        return out;
    }

    /// Stream in
    friend std::istream&
    operator>> (std::istream& in, ExperimentalValue_T<T>& EV)
    {
        std::string tmp;
        in >> tmp;

        std::stringstream Tokenized;
        auto ValEnd = tmp.find_first_of('[');

        auto ErrEnd = tmp.find_first_of(']', ValEnd);
        auto SeparatorPos = tmp.find_first_of(':', ValEnd);
        if(ValEnd == tmp.npos || ErrEnd == tmp.npos ||
            (SeparatorPos > ErrEnd && SeparatorPos != tmp.npos))
        {
            Core::StreamingError e{"Invalid string encoding the experimental value: " + tmp};
            e.raise();
        }

        Tokenized << tmp.substr(0, ValEnd) << '\t';

        if(SeparatorPos != tmp.npos)
        {
            // The separator was found so we have an asymmetric error
            EV.m_SymmetricErrors = false;

            Tokenized << tmp.substr(ValEnd + 1, SeparatorPos - ValEnd - 1) << '\t' <<
                tmp.substr(SeparatorPos + 1, ErrEnd - SeparatorPos - 1);
            Tokenized >> EV.m_Value >> EV.m_NegativeError >> EV.m_PositiveError;
        }
        else
        {
            // In this case we assume symmetric errors
            EV.m_SymmetricErrors = true;
            Tokenized << tmp.substr(ValEnd + 1, ErrEnd - ValEnd - 1);
            Tokenized >> EV.m_Value >> EV.m_NegativeError;
            EV.m_PositiveError = EV.m_NegativeError;
        }

        return in;
    }

    /// Equality
    friend bool
    operator== (const ExperimentalValue_T<T>& lhs, const ExperimentalValue_T<T>& rhs)
    {
        return(lhs.m_Value == rhs.m_Value &&
            lhs.m_NegativeError == rhs.m_NegativeError &&
            lhs.m_PositiveError == rhs.m_PositiveError &&
            lhs.m_SymmetricErrors == rhs.m_SymmetricErrors);
    }

    /// Inequality
    friend bool
    operator!= (const ExperimentalValue_T<T>& lhs, const ExperimentalValue_T<T>& rhs)
    {
        return !(lhs == rhs);
    }


private:
    /// Measurement value
    T m_Value;

    /// Error on the negative side
    T m_NegativeError;

    /// Error on the positive side
    T m_PositiveError;

    /// Defines if the errors are symmetric or not in which case only the positive error
    /// is used which can be more efficient and avoids checks which can fail due to numerical
    /// precision problems
    bool m_SymmetricErrors;

};

typedef ExperimentalValue_T<double> ExperimentalValue;



/* ********************************************************************
 *
 * Template declaration
 *
 ******************************************************************** */


template<typename T>
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::ExperimentalValue_T():
    ExperimentalValue_T<T>{T{}, T{}}
{
}

template<typename T>
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::ExperimentalValue_T(T Value, T NegError, T PosError):
    m_Value{Value},
    m_NegativeError{NegError},
    m_PositiveError{PosError},
    m_SymmetricErrors{false}
{
}

template<typename T>
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::ExperimentalValue_T(T Value, T Error):
    m_Value{Value},
    m_NegativeError{Error},
    m_PositiveError{Error},
    m_SymmetricErrors{true}
{
}

template<typename T>
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::ExperimentalValue_T(T Value, std::pair<T,T> Errors):
    ExperimentalValue_T<T>{Value, Errors.first, Errors.second}
{
    if(m_NegativeError == m_PositiveError)
        m_SymmetricErrors = true;
}

template<typename T>
T
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::GetValue() const
{
    return m_Value;
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::SetValue(T value)
{
    m_Value = value;
}

template<typename T>
T
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::GetNegativeError() const
{
    return m_NegativeError;
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::SetNegativeError(T negativeError)
{
    m_NegativeError = negativeError;
    m_SymmetricErrors = false;
}

template<typename T>
T
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::GetPositiveError() const
{
    return m_PositiveError;
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::SetPositiveError(T positiveError)
{
    m_PositiveError = positiveError;
    m_SymmetricErrors = false;
}

template<typename T>
T
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::GetError() const
{
    if(m_SymmetricErrors)
    {
        return m_PositiveError;
    }
    else
    {
        return (m_PositiveError + m_NegativeError) / 2.0;
    }
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::SetError(T Error)
{
    m_PositiveError = Error;
    m_NegativeError = Error;
    m_SymmetricErrors = true;
}

template<typename T>
std::pair<T,T>
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::GetErrors() const
{
    return std::pair<T, T>{m_NegativeError, m_PositiveError};
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::SetErrors(T NegError, T PosError)
{
    m_NegativeError = NegError;
    m_PositiveError = PosError;
    m_SymmetricErrors = false;
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::SetErrors(std::pair<T,T> Errors)
{
    m_NegativeError = Errors.first;
    m_PositiveError = Errors.second;
    m_SymmetricErrors = false;
}

template<typename T>
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::operator T()
{
    return m_Value;
}

template<typename T>
bool
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::HasSymmetricErrors() const
{
    return m_SymmetricErrors;
}

template<typename T>
void
ExperimentalValue_T<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>::ForceSymmetricErrors(bool symmetricErrors)
{
    m_SymmetricErrors = symmetricErrors;
}



}  // namespace Util

}  // namespace MXWare



#endif /* SOURCE_MXUTIL_EXPERIMENTALVALUE_T_HH_ */
