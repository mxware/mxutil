/*
 * MXUtil.hh
 *
 *  Created on: Jun 7, 2018
 *      Author: caiazza
 */

#ifndef SOURCE_MXUTIL_HH_
#define SOURCE_MXUTIL_HH_

// General functions and structures
#include "MXWare/Util/MXUtil.hh"
#include "MXWare/Util/MXUtilVersion.hh"
#include "MXWare/Util/ExperimentalValue_T.hh"
#include "MXWare/Util/Range.hh"
#include "MXWare/Util/RangeSet.hh"

#include "MXWare/Util/SystemOfUnits.hh"

// Geometry
#include "MXWare/Util/Geometry/Geometry.hh"
#include "MXWare/Util/Geometry/Algorithm.hh"

#include "MXWare/Util/Spinor.hh"
#include "MXWare/Util/Tensor.hh"

// ParticleData
#include "MXWare/Util/MXParticleID.hh"
#include "MXWare/Util/MXParticleData.hh"
#include "MXWare/Util/MXParticleDataTable.hh"

// Identifiers
#include "MXWare/Util/CommonIdentifier.hh"




#endif /* SOURCE_MXUTIL_HH_ */
