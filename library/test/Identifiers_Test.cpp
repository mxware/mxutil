/*
 * Identifiers_Test.cpp
 *
 *  Created on: Mar 1, 2019
 *      Author: caiazza
 */

#define BOOST_TEST_MODULE Identifiers_Test test

#define BOOST_TEST_DYN_LINK

#include <boost/core/lightweight_test.hpp>
#include <boost/test/unit_test.hpp>

#include <iostream>

#include "MXWare/Util/CommonIdentifier.hh"
#include "MXWare/Test/CoreInterfaceTestHelper.hh"

using MXWare::Util::CommonIdentifier;
namespace mt = MXWare::Test;

using pair_t = std::pair<uint32_t, uint32_t>;

BOOST_AUTO_TEST_CASE(BasicOperations)
{
    std::cout << "Testing the construction of the common identifier" << std::endl;

    CommonIdentifier Id0{};
    BOOST_TEST(Id0.IsUndefined());
    BOOST_TEST(!Id0);

    CommonIdentifier IdConstPair{pair_t{1U, 20U}};
    BOOST_TEST(IdConstPair);
    BOOST_TEST(!IdConstPair.IsUndefined());
    BOOST_TEST((static_cast<uint64_t>(IdConstPair) == (1ULL<<32) + 20));

    auto InPair = std::make_pair(22U, 20U);
    CommonIdentifier IdNormPair{InPair};
    BOOST_TEST(IdNormPair);
    BOOST_TEST(!IdNormPair.IsUndefined());
    BOOST_TEST((static_cast<pair_t>(IdNormPair) == InPair));

    CommonIdentifier IdDuplo{InPair.first, InPair.second};
    BOOST_TEST(IdDuplo);
    BOOST_TEST(!IdDuplo.IsUndefined());
    BOOST_TEST((static_cast<pair_t>(IdDuplo) == InPair));
    BOOST_TEST(IdDuplo == IdNormPair);
    BOOST_TEST(IdDuplo != Id0);
    BOOST_TEST(IdDuplo != IdConstPair);

    CommonIdentifier ID64{static_cast<uint64_t>(IdDuplo)};
    BOOST_TEST(ID64);
    BOOST_TEST(!ID64.IsUndefined());
    BOOST_TEST(IdDuplo == ID64);
    BOOST_TEST(ID64 != Id0);
    BOOST_TEST(ID64 != IdConstPair);

    mt::CopyAndMoveTest(std::move(IdConstPair));
}

BOOST_AUTO_TEST_CASE(GetAndSetTest)
{
    std::cout << "Testing getters and setters" << std::endl;

    auto GroupId = 12U;
    auto ElementId = 1000U;
    auto PairId = std::make_pair(GroupId, ElementId);
    auto U64Id = GroupId * std::pow(2, 32) + ElementId;

    CommonIdentifier IdTest{};
    IdTest.SetId(GroupId, ElementId);
    BOOST_TEST(IdTest);
    BOOST_TEST((IdTest.get<0>() == GroupId));
    BOOST_TEST((IdTest.get<1>() == ElementId));
    BOOST_TEST((static_cast<pair_t>(IdTest) == PairId));
    BOOST_TEST((static_cast<uint64_t>(IdTest) == U64Id));

    IdTest.SetUndefined();
    BOOST_TEST(!IdTest);

    IdTest.SetId(U64Id);
    BOOST_TEST(IdTest);
    BOOST_TEST((IdTest.get<0>() == GroupId));
    BOOST_TEST((IdTest.get<1>() == ElementId));
    BOOST_TEST((static_cast<pair_t>(IdTest) == PairId));
    BOOST_TEST((static_cast<uint64_t>(IdTest) == U64Id));

    IdTest.SetUndefined();
    BOOST_TEST(!IdTest);

    IdTest.SetId(PairId);
    BOOST_TEST(IdTest);
    BOOST_TEST((IdTest.get<0>() == GroupId));
    BOOST_TEST((IdTest.get<1>() == ElementId));
    BOOST_TEST((static_cast<pair_t>(IdTest) == PairId));
    BOOST_TEST((static_cast<uint64_t>(IdTest) == U64Id));
}

BOOST_AUTO_TEST_CASE(ConfTreeIOTest)
{
    std::cout << "Testing the streaming" << std::endl;

    auto GroupId = 12U;
    auto ElementId = 1000U;
    auto U64Id = GroupId * std::pow(2, 32) + ElementId;
    std::string GoodStr = "12|1000";
    std::string GoodU64 = std::to_string(U64Id);
    std::string UndefinedStr = "?";
    std::string WrongStr = "ddw";
    std::string NonRep = "-1";
    std::string NonRepPair = "-1|22";

    CommonIdentifier IdTest{GroupId, ElementId};
    std::cout << "Identifier string  " << IdTest << std::endl;
    std::stringstream ss{};
    ss << IdTest;
    BOOST_TEST((ss.str() == GoodStr));

    CommonIdentifier InputId{};
    ss.str(GoodStr);
    ss.seekg(0);
    ss >> InputId;
    BOOST_TEST((InputId == IdTest));

    ss.str(UndefinedStr);
    ss.seekg(0);
    ss >> InputId;
    BOOST_TEST(!InputId);

    std::cout << "U64: " << GoodU64 << std::endl;
    ss.str(GoodU64);
    ss.seekg(0);
    ss >> InputId;
    BOOST_TEST((InputId == IdTest));

    ss.str(WrongStr);
    ss >> InputId;
    BOOST_TEST(!InputId);

    InputId = IdTest;
    ss.str(NonRep);
    ss.seekg(0);
    ss >> InputId;
    BOOST_TEST(!InputId);

    InputId = IdTest;
    ss.str(NonRepPair);
    ss.seekg(0);
    ss >> InputId;
    BOOST_TEST(!InputId);

    auto Tree = mt::DoConfigurationCycleTest(IdTest);
    std::cout << Tree << std::endl;
}

